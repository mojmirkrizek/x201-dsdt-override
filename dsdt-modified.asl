// CreatorID=MSFT	CreatorRev=3.0.1
// FileLength=56794	FileChkSum=0xb4

DefinitionBlock("dsdt.dat", "DSDT", 0x01, "LENOVO", "TP-6Q   ", 0x00001320)
{
    Scope(\_PR_)
    {
        Processor(CPU0, 0x0, 0x1010, 0x6)
        {
        }
        Processor(CPU1, 0x1, 0x1010, 0x6)
        {
        }
        Processor(CPU2, 0x2, 0x1010, 0x6)
        {
        }
        Processor(CPU3, 0x3, 0x1010, 0x6)
        {
        }
        Processor(CPU4, 0x4, 0x1010, 0x6)
        {
        }
        Processor(CPU5, 0x5, 0x1010, 0x6)
        {
        }
        Processor(CPU6, 0x6, 0x1010, 0x6)
        {
        }
        Processor(CPU7, 0x7, 0x1010, 0x6)
        {
        }
    }
    Scope(\)
    {
        Method(PNTF, 0x1, NotSerialized)
        {
            If(And(\PPMF, 0x1000000, ))
            {
                If(LOr(LAnd(And(PDC0, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC0, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU0, Arg0)
                }
                If(LOr(LAnd(And(PDC1, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC1, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU1, Arg0)
                }
                If(LOr(LAnd(And(PDC2, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC2, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU2, Arg0)
                }
                If(LOr(LAnd(And(PDC3, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC3, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU3, Arg0)
                }
                If(LOr(LAnd(And(PDC4, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC4, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU4, Arg0)
                }
                If(LOr(LAnd(And(PDC5, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC5, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU5, Arg0)
                }
                If(LOr(LAnd(And(PDC6, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC6, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU6, Arg0)
                }
                If(LOr(LAnd(And(PDC7, 0x8, ), LOr(LEqual(Arg0, 0x80), LEqual(Arg0, 0x82))), LAnd(And(PDC7, 0x10, ), LEqual(Arg0, 0x81))))
                {
                    Notify(\_PR_.CPU7, Arg0)
                }
            }
            Else
            {
                If(LOr(LEqual(Arg0, 0x80), LOr(LEqual(Arg0, 0x81), LEqual(Arg0, 0x82))))
                {
                    Notify(\_PR_.CPU0, Arg0)
                }
            }
        }
    }
    OperationRegion(MNVS, SystemMemory, 0xbb6e7000, 0x1000)
    Field(MNVS, DWordAcc, NoLock, Preserve)
    {
        Offset(0xe00),
        GAPA, 32,
        GAPL, 32,
        DCKI, 32,
        DCKS, 32,
        VCDL, 1,
        VCDC, 1,
        VCDT, 1,
        VCDD, 1,
        VIGD, 1,
        VCSS, 1,
        VCDB, 1,
        VCIN, 1,
        VPDF, 1,
        , 7,
        VLID, 4,
        VVPO, 4,
        BRLV, 4,
        BRFQ, 2,
        BRHB, 1,
        BREV, 1,
        CDFL, 8,
        CDAH, 8,
        PMOD, 2,
        PDIR, 1,
        PDMA, 1,
        , 4,
        LFDC, 1,
        , 7,
        C2NA, 1,
        C3NA, 1,
        C4NA, 1,
        C6NA, 1,
        C7NA, 1,
        , 3,
        , 8,
        , 2,
        , 1,
        NHPS, 1,
        NPME, 1,
        , 3,
        UOPT, 8,
        BTID, 32,
        DPP0, 1,
        DPP1, 1,
        DPP2, 1,
        DPP3, 1,
        DPP4, 1,
        DPP5, 1,
        , 2,
        , 8,
        TCRT, 16,
        TPSV, 16,
        TTC1, 16,
        TTC2, 16,
        TTSP, 16,
        SRAH, 8,
        SRHE, 8,
        SRE1, 8,
        SRE2, 8,
        SRE3, 8,
        SRE4, 8,
        SRE5, 8,
        SRE6, 8,
        SRU1, 8,
        SRU2, 8,
        SRU3, 8,
        SRU7, 8,
        SRU4, 8,
        SRU5, 8,
        SRU8, 8,
        SRPB, 8,
        SRLP, 8,
        SRSA, 8,
        SRSM, 8,
        CWAC, 1,
        CWAS, 1,
        CWUE, 1,
        CWUS, 1,
        , 4,
        CWAP, 16,
        CWAT, 16,
        DBGC, 1,
        , 7,
        FS1L, 16,
        FS1M, 16,
        FS1H, 16,
        FS2L, 16,
        FS2M, 16,
        FS2H, 16,
        FS3L, 16,
        FS3M, 16,
        FS3H, 16,
        TATC, 1,
        , 6,
        TATL, 1,
        TATW, 8,
        TNFT, 4,
        TNTT, 4,
        TDFA, 4,
        TDTA, 4,
        TDFD, 4,
        TDTD, 4,
        TCFA, 4,
        TCTA, 4,
        TCFD, 4,
        TCTD, 4,
        TSFT, 4,
        TSTT, 4,
        TIT0, 8,
        TCR0, 16,
        TPS0, 16,
        TIT1, 8,
        TCR1, 16,
        TPS1, 16,
        TIT2, 8,
        TCR2, 16,
        TPS2, 16,
        TIF0, 8,
        TIF1, 8,
        TIF2, 8,
        , 32,
        TCZ1, 8,
        TCZ2, 8,
        TCZ3, 8,
        BTHI, 1,
        , 7,
        HDIR, 1,
        HDEH, 1,
        HDSP, 1,
        HDPP, 1,
        HDUB, 1,
        HDMC, 1,
        , 2,
        TPMP, 1,
        TPMS, 1,
        , 6,
        BIDE, 4,
        IDET, 4,
        , 1,
        DTSE, 1,
        , 6,
        DTS0, 8,
        DTS1, 8,
        DT00, 1,
        DT01, 1,
        DT02, 1,
        DT03, 1,
        , 4,
        LIDB, 1,
        C4WR, 1,
        C4AC, 1,
        ODDX, 1,
        CMPR, 1,
        , 3,
        PH01, 8,
        PH02, 8,
        PH03, 8,
        PPRQ, 8,
        PPLO, 8,
        PPRP, 8,
        PPOR, 8,
        TPRS, 8,
        TPMV, 8,
        MOR_, 8,
        RSV0, 8,
        IPAT, 4,
        IPSC, 1,
        IDMM, 1,
        IDMS, 2,
        HVCO, 3,
        IF1E, 1,
        ISSC, 1,
        LIDS, 1,
        IBIA, 2,
        IBTT, 8,
        ITVF, 4,
        ITVM, 4,
        TCG0, 1,
        TCG1, 1,
        , 6,
        SWGP, 8,
        IPMS, 8,
        IPMB, 120,
        IPMR, 24,
        IPMO, 24,
        IPMA, 8,
        , 32,
        ASFT, 8,
        DATD, 1,
        PJID, 2,
        FID_, 2,
        , 3,
        , 8,
        CHKC, 32,
        CHKE, 32,
        ATRB, 32,
        , 8,
        PPCR, 8,
        TPCR, 5,
        , 3,
        ATMB, 128,
        PPCA, 8,
        TPCA, 5,
        , 3,
        BFWB, 296
    }
    Field(MNVS, ByteAcc, NoLock, Preserve)
    {
        Offset(0xc00),
        WITM, 8,
        WSEL, 8,
        WLS0, 8,
        WLS1, 8,
        WLS2, 8,
        WLS3, 8,
        WLS4, 8,
        WLS5, 8,
        WLS6, 8,
        WLS7, 8,
        WENC, 8,
        WKBD, 8,
        WPTY, 8,
        WPAS, 1032,
        WPNW, 1032,
        WSPM, 8,
        WSPS, 8,
        WSMN, 8,
        WSMX, 8,
        WSEN, 8,
        WSKB, 8
    }
    Field(MNVS, ByteAcc, NoLock, Preserve)
    {
        Offset(0xb00),
        DBGS, 1024
    }
    OperationRegion(GNVS, SystemMemory, 0xbb6c55e2, 0x200)
    Field(GNVS, AnyAcc, Lock, Preserve)
    {
        OSYS, 16,
        SMIF, 8,
        PRM0, 8,
        PRM1, 8,
        Offset(0x10),
        PWRS, 8,
        Offset(0x1e),
        Offset(0x28),
        APIC, 8,
        MPEN, 8,
        PCP0, 8,
        PCP1, 8,
        PPCM, 8,
        PPMF, 32,
        , 8,
        Offset(0x3c),
        IGDS, 8,
        TLST, 8,
        CADL, 8,
        PADL, 8,
        CSTE, 16,
        NSTE, 16,
        SSTE, 16,
        NDID, 8,
        DID1, 32,
        DID2, 32,
        DID3, 32,
        DID4, 32,
        DID5, 32,
        KSV0, 32,
        KSV1, 8,
        BDSP, 8,
        PTY1, 8,
        PTY2, 8,
        PSCL, 8,
        TVF1, 8,
        TVF2, 8,
        GETM, 8,
        BLCS, 8,
        BRTL, 8,
        ALSE, 8,
        ALAF, 8,
        LLOW, 8,
        LHIH, 8,
        Offset(0xaa),
        ASLB, 32,
        Offset(0xdf),
        PAVP, 8,
        Offset(0x15b),
        PNHM, 32,
        TBAB, 32,
        Offset(0x180),
        BUSH, 16,
        BUSM, 16,
        BUSL, 16,
        HIST, 32,
        LPST, 32,
        LWST, 32,
        FREH, 32,
        FREL, 32,
        SVST, 32,
        GVEN, 8,
        GVAC, 8,
        GVDC, 8,
        STCL, 8,
        APCL, 8,
        OSPX, 1,
        OSC4, 1,
        , 6,
        SPEN, 1,
        , 1,
        , 1,
        , 1,
        , 4,
        FTPS, 8,
        IPEN, 1
    }
    Scope(\_SB_)
    {
        Method(_INI, 0x0, NotSerialized)
        {
            If(CondRefOf(\_OSI, Local0))
            {
                If(\_OSI("Windows 2001"))
                {
                    Store(0x1, \WNTF)
                    Store(0x1, \WXPF)
                    Store(0x0, \WSPV)
                }
                If(\_OSI("Windows 2001 SP1"))
                {
                    Store(0x1, \WSPV)
                }
                If(\_OSI("Windows 2001 SP2"))
                {
                    Store(0x2, \WSPV)
                }
                If(\_OSI("Windows 2006"))
                {
                    Store(0x1, \WVIS)
                }
                If(\_OSI("Linux"))
                {
                    Store(0x1, \LNUX)
                }
                If(\_OSI("FreeBSD"))
                {
                    Store(0x1, \LNUX)
                }
            }
            Else
            {
                If(LEqual(\SCMP(\_OS_, "Microsoft Windows NT"), Zero))
                {
                    Store(0x1, \WNTF)
                }
            }
            If(LNot(LLess(\_REV, 0x2)))
            {
                Store(0x1, \H8DR)
            }
            Store(0x1, \OSIF)
            Store(\_SB_.PCI0.LPC_.EC__.AC__._PSR(), \PWRS)
            \_SB_.PCI0.LPC_.MOU_.MHID()
            If(\LNUX)
            {
                \_SB_.PCI0.LPC_.EC__.SAUM(0x2)
                \UCMS(0x1c)
            }
            Store(\SRAH, \_SB_.PCI0.RID_)
            If(VIGD)
            {
                Store(\SRHE, \_SB_.PCI0.VID_.RID_)
            }
            Else
            {
                Store(\SRHE, \_SB_.PCI0.PEG_.RID_)
            }
            Store(\SRE1, \_SB_.PCI0.EXP1.RID_)
            Store(\SRE2, \_SB_.PCI0.EXP2.RID_)
            Store(\SRE3, \_SB_.PCI0.EXP3.RID_)
            Store(\SRE4, \_SB_.PCI0.EXP4.RID_)
            Store(\SRU7, \_SB_.PCI0.EHC1.RID_)
            Store(\SRU8, \_SB_.PCI0.EHC2.RID_)
            Store(\SRLP, \_SB_.PCI0.LPC_.RID_)
            Store(\SRSA, \_SB_.PCI0.SAT1.RID_)
            Store(\SRSM, \_SB_.PCI0.SMBU.RID_)
        }
        Device(LNKA)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x1)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRA)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRA, 0x80, \_SB_.PCI0.LPC_.PIRA)
            }
            Name(BUFA, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFA, 0x1, IRA1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRA, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRA1)
                }
                Else
                {
                    Store(0x0, IRA1)
                }
                Return(BUFA)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRA2)
                FindSetRightBit(IRA2, Local0)
                And(\_SB_.PCI0.LPC_.PIRA, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRA)
            }
        }
        Device(LNKB)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x2)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRB)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRB, 0x80, \_SB_.PCI0.LPC_.PIRB)
            }
            Name(BUFB, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFB, 0x1, IRB1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRB, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRB1)
                }
                Else
                {
                    Store(0x0, IRB1)
                }
                Return(BUFB)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRB2)
                FindSetRightBit(IRB2, Local0)
                And(\_SB_.PCI0.LPC_.PIRB, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRB)
            }
        }
        Device(LNKC)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x3)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRC)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRC, 0x80, \_SB_.PCI0.LPC_.PIRC)
            }
            Name(BUFC, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFC, 0x1, IRC1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRC, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRC1)
                }
                Else
                {
                    Store(0x0, IRC1)
                }
                Return(BUFC)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRC2)
                FindSetRightBit(IRC2, Local0)
                And(\_SB_.PCI0.LPC_.PIRC, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRC)
            }
        }
        Device(LNKD)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x4)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRD)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRD, 0x80, \_SB_.PCI0.LPC_.PIRD)
            }
            Name(BUFD, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFD, 0x1, IRD1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRD, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRD1)
                }
                Else
                {
                    Store(0x0, IRD1)
                }
                Return(BUFD)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRD2)
                FindSetRightBit(IRD2, Local0)
                And(\_SB_.PCI0.LPC_.PIRD, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRD)
            }
        }
        Device(LNKE)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x5)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRE)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRE, 0x80, \_SB_.PCI0.LPC_.PIRE)
            }
            Name(BUFE, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFE, 0x1, IRE1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRE, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRE1)
                }
                Else
                {
                    Store(0x0, IRE1)
                }
                Return(BUFE)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRE2)
                FindSetRightBit(IRE2, Local0)
                And(\_SB_.PCI0.LPC_.PIRE, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRE)
            }
        }
        Device(LNKF)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x6)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRF)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRF, 0x80, \_SB_.PCI0.LPC_.PIRF)
            }
            Name(BUFF, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFF, 0x1, IRF1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRF, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRF1)
                }
                Else
                {
                    Store(0x0, IRF1)
                }
                Return(BUFF)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRF2)
                FindSetRightBit(IRF2, Local0)
                And(\_SB_.PCI0.LPC_.PIRF, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRF)
            }
        }
        Device(LNKG)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x7)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRG)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRG, 0x80, \_SB_.PCI0.LPC_.PIRG)
            }
            Name(BUFG, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFG, 0x1, IRG1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRG, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRG1)
                }
                Else
                {
                    Store(0x0, IRG1)
                }
                Return(BUFG)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRG2)
                FindSetRightBit(IRG2, Local0)
                And(\_SB_.PCI0.LPC_.PIRG, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRG)
            }
        }
        Device(LNKH)
        {
            Name(_HID, 0xf0cd041)
            Name(_UID, 0x8)
            Method(_STA, 0x0, NotSerialized)
            {
                If(LNot(VPIR(\_SB_.PCI0.LPC_.PIRH)))
                {
                    Return(0x9)
                }
                Else
                {
                    Return(0xb)
                }
            }
            Name(_PRS, Buffer(0x6)
            {
	0x23, 0xf8, 0x0e, 0x18, 0x79, 0x00
            })
            Method(_DIS, 0x0, NotSerialized)
            {
                Or(\_SB_.PCI0.LPC_.PIRH, 0x80, \_SB_.PCI0.LPC_.PIRH)
            }
            Name(BUFH, Buffer(0x6)
            {
	0x23, 0x00, 0x00, 0x18, 0x79, 0x00
            })
            CreateWordField(BUFH, 0x1, IRH1)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.PCI0.LPC_.PIRH, 0x8f, Local0)
                If(VPIR(Local0))
                {
                    ShiftLeft(0x1, Local0, IRH1)
                }
                Else
                {
                    Store(0x0, IRH1)
                }
                Return(BUFH)
            }
            Method(_SRS, 0x1, NotSerialized)
            {
                CreateWordField(Arg0, 0x1, IRH2)
                FindSetRightBit(IRH2, Local0)
                And(\_SB_.PCI0.LPC_.PIRH, 0x70, Local1)
                Or(Local1, Decrement(Local0), Local1)
                Store(Local1, \_SB_.PCI0.LPC_.PIRH)
            }
        }
        Method(VPIR, 0x1, NotSerialized)
        {
            Store(0x1, Local0)
            If(And(Arg0, 0x80, ))
            {
                Store(0x0, Local0)
            }
            Else
            {
                And(Arg0, 0xf, Local1)
                If(LLess(Local1, 0x3))
                {
                    Store(0x0, Local0)
                }
                Else
                {
                    If(LOr(LEqual(Local1, 0x8), LEqual(Local1, 0xd)))
                    {
                        Store(0x0, Local0)
                    }
                }
            }
            Return(Local0)
        }
        Device(MEM_)
        {
            Name(_HID, 0x10cd041)
            Name(MEMS, Buffer(0xce)
            {
	0x86, 0x09, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x40, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x80, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0xc0, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x40, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x80, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0xc0, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x40, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x80, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0xc0, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x01, 0x00,
	0x86, 0x09, 0x00, 0x01, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0xee, 0x01,
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0xc0, 0xfe, 0x00, 0x00, 0x14, 0x00,
	0x86, 0x09, 0x00, 0x00, 0x00, 0xc0, 0xd4, 0xfe, 0x00, 0x40, 0x2b, 0x01,
	0x79, 0x00
            })
            CreateDWordField(MEMS, 0x14, MC0L)
            CreateDWordField(MEMS, 0x20, MC4L)
            CreateDWordField(MEMS, 0x2c, MC8L)
            CreateDWordField(MEMS, 0x38, MCCL)
            CreateDWordField(MEMS, 0x44, MD0L)
            CreateDWordField(MEMS, 0x50, MD4L)
            CreateDWordField(MEMS, 0x5c, MD8L)
            CreateDWordField(MEMS, 0x68, MDCL)
            CreateDWordField(MEMS, 0x74, ME0L)
            CreateDWordField(MEMS, 0x80, ME4L)
            CreateDWordField(MEMS, 0x8c, ME8L)
            CreateDWordField(MEMS, 0x98, MECL)
            CreateBitField(MEMS, 0x78, MC0W)
            CreateBitField(MEMS, 0xd8, MC4W)
            CreateBitField(MEMS, 0x138, MC8W)
            CreateBitField(MEMS, 0x198, MCCW)
            CreateBitField(MEMS, 0x1f8, MD0W)
            CreateBitField(MEMS, 0x258, MD4W)
            CreateBitField(MEMS, 0x2b8, MD8W)
            CreateBitField(MEMS, 0x318, MDCW)
            CreateBitField(MEMS, 0x378, ME0W)
            CreateBitField(MEMS, 0x3d8, ME4W)
            CreateBitField(MEMS, 0x438, ME8W)
            CreateBitField(MEMS, 0x498, MECW)
            CreateDWordField(MEMS, 0xac, MEB1)
            CreateDWordField(MEMS, 0xb0, MEL1)
            CreateDWordField(MEMS, 0xbc, MEL2)
            CreateDWordField(MEMS, 0xc8, MEL3)
            Method(_CRS, 0x0, NotSerialized)
            {
                And(\_SB_.UNCR.SAD_.PAM1, 0x3, Local0)
                If(Local0)
                {
                    Store(0x4000, MC0L)
                    If(And(Local0, 0x2, ))
                    {
                        Store(0x1, MC0W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM1, 0x30, Local0)
                If(Local0)
                {
                    Store(0x4000, MC4L)
                    If(And(Local0, 0x20, ))
                    {
                        Store(0x1, MC4W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM2, 0x3, Local0)
                If(Local0)
                {
                    Store(0x4000, MC8L)
                    If(And(Local0, 0x2, ))
                    {
                        Store(0x1, MC8W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM2, 0x30, Local0)
                If(Local0)
                {
                    Store(0x4000, MCCL)
                    If(And(Local0, 0x20, ))
                    {
                        Store(0x1, MCCW)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM3, 0x3, Local0)
                If(Local0)
                {
                    Store(0x4000, MD0L)
                    If(And(Local0, 0x2, ))
                    {
                        Store(0x1, MD0W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM3, 0x30, Local0)
                If(Local0)
                {
                    Store(0x4000, MD4L)
                    If(And(Local0, 0x20, ))
                    {
                        Store(0x1, MD4W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM4, 0x3, Local0)
                If(Local0)
                {
                    Store(0x4000, MD8L)
                    If(And(Local0, 0x2, ))
                    {
                        Store(0x1, MD8W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM4, 0x30, Local0)
                If(Local0)
                {
                    Store(0x4000, MDCL)
                    If(And(Local0, 0x20, ))
                    {
                        Store(0x1, MDCW)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM5, 0x3, Local0)
                If(Local0)
                {
                    Store(0x4000, ME0L)
                    If(And(Local0, 0x2, ))
                    {
                        Store(0x1, ME0W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM5, 0x30, Local0)
                If(Local0)
                {
                    Store(0x4000, ME4L)
                    If(And(Local0, 0x20, ))
                    {
                        Store(0x1, ME4W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM6, 0x3, Local0)
                If(Local0)
                {
                    Store(0x4000, ME8L)
                    If(And(Local0, 0x2, ))
                    {
                        Store(0x1, ME8W)
                    }
                }
                And(\_SB_.UNCR.SAD_.PAM6, 0x30, Local0)
                If(Local0)
                {
                    Store(0x4000, MECL)
                    If(And(Local0, 0x20, ))
                    {
                        Store(0x1, MECW)
                    }
                }
                Subtract(\MEMX, MEB1, MEL1)
                If(LNot(LEqual(\_SB_.PCI0.LPC_.TPM_._STA(), 0xf)))
                {
                    Store(0x1400000, MEL2)
                    Store(0x0, MEL3)
                }
                Return(MEMS)
            }
        }
        Device(LID_)
        {
            Name(_HID, 0xd0cd041)
            Method(_LID, 0x0, NotSerialized)
            {
                If(\H8DR)
                {
                    Return(\_SB_.PCI0.LPC_.EC__.HPLD)
                }
                Else
                {
                    If(And(\RBEC(0x46), 0x4, ))
                    {
                        Return(0x1)
                    }
                    Else
                    {
                        Return(0x0)
                    }
                }
            }
            Method(_PRW, 0x0, NotSerialized)
            {
                Return(Package(0x2)
                {
                    0x1d,
                    0x3
                })
            }
            Method(_PSW, 0x1, NotSerialized)
            {
                If(\H8DR)
                {
                    If(Arg0)
                    {
                        Store(0x1, \_SB_.PCI0.LPC_.EC__.HWLO)
                    }
                    Else
                    {
                        Store(0x0, \_SB_.PCI0.LPC_.EC__.HWLO)
                    }
                }
                Else
                {
                    If(Arg0)
                    {
                        \MBEC(0x32, 0xff, 0x4)
                    }
                    Else
                    {
                        \MBEC(0x32, 0xfb, 0x0)
                    }
                }
            }
        }
        Device(SLPB)
        {
            Name(_HID, 0xe0cd041)
            Method(_PRW, 0x0, NotSerialized)
            {
                Return(Package(0x2)
                {
                    0x1d,
                    0x3
                })
            }
            Method(_PSW, 0x1, NotSerialized)
            {
                If(\H8DR)
                {
                    If(Arg0)
                    {
                        Store(0x1, \_SB_.PCI0.LPC_.EC__.HWFN)
                    }
                    Else
                    {
                        Store(0x0, \_SB_.PCI0.LPC_.EC__.HWFN)
                    }
                }
                Else
                {
                    If(Arg0)
                    {
                        \MBEC(0x32, 0xff, 0x10)
                    }
                    Else
                    {
                        \MBEC(0x32, 0xef, 0x0)
                    }
                }
            }
        }
        Device(UNCR)
        {
            Name(_BBN, 0xff)
            Name(_ADR, 0x0)
            Name(RID_, 0x0)
            Name(_HID, 0x30ad041)
            Name(_CRS, Buffer(0x12)
            {
	0x88, 0x0d, 0x00, 0x02, 0x0c, 0x00, 0x00, 0x00, 0xff, 0x00, 0xff, 0x00,
	0x00, 0x00, 0x01, 0x00, 0x79, 0x00
            })
            Device(SAD_)
            {
                Name(_ADR, 0x1)
                Name(RID_, 0x0)
                OperationRegion(SADC, PCI_Config, 0x0, 0x100)
                Field(SADC, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x40),
                    PAM0, 8,
                    PAM1, 8,
                    PAM2, 8,
                    PAM3, 8,
                    PAM4, 8,
                    PAM5, 8,
                    PAM6, 8
                }
            }
        }
        Device(PCI0)
        {
            Name(_BBN, 0x0)
            Name(_ADR, 0x0)
            Name(RID_, 0x0)
            Name(_S3D, 0x2)
            Name(LRRT, Package(0x19)
            {
                Package(0x4)
                {
                    0x1ffff,
                    0x0,
                    \_SB_.LNKA,
                    0x0
                },
                Package(0x4)
                {
                    0x2ffff,
                    0x0,
                    \_SB_.LNKA,
                    0x0
                },
                Package(0x4)
                {
                    0x3ffff,
                    0x0,
                    \_SB_.LNKA,
                    0x0
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x0,
                    \_SB_.LNKA,
                    0x0
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x1,
                    \_SB_.LNKB,
                    0x0
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x2,
                    \_SB_.LNKC,
                    0x0
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x3,
                    \_SB_.LNKD,
                    0x0
                },
                Package(0x4)
                {
                    0x19ffff,
                    0x0,
                    \_SB_.LNKE,
                    0x0
                },
                Package(0x4)
                {
                    0x1affff,
                    0x0,
                    \_SB_.LNKE,
                    0x0
                },
                Package(0x4)
                {
                    0x1affff,
                    0x1,
                    \_SB_.LNKF,
                    0x0
                },
                Package(0x4)
                {
                    0x1affff,
                    0x2,
                    \_SB_.LNKG,
                    0x0
                },
                Package(0x4)
                {
                    0x1affff,
                    0x3,
                    \_SB_.LNKH,
                    0x0
                },
                Package(0x4)
                {
                    0x1bffff,
                    0x1,
                    \_SB_.LNKB,
                    0x0
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x0,
                    \_SB_.LNKE,
                    0x0
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x1,
                    \_SB_.LNKF,
                    0x0
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x2,
                    \_SB_.LNKG,
                    0x0
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x3,
                    \_SB_.LNKH,
                    0x0
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x0,
                    \_SB_.LNKA,
                    0x0
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x1,
                    \_SB_.LNKB,
                    0x0
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x2,
                    \_SB_.LNKC,
                    0x0
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x3,
                    \_SB_.LNKD,
                    0x0
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x0,
                    \_SB_.LNKH,
                    0x0
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x1,
                    \_SB_.LNKA,
                    0x0
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x2,
                    \_SB_.LNKB,
                    0x0
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x3,
                    \_SB_.LNKD,
                    0x0
                }
            })
            Name(ARRT, Package(0x19)
            {
                Package(0x4)
                {
                    0x1ffff,
                    0x0,
                    0x0,
                    0x10
                },
                Package(0x4)
                {
                    0x2ffff,
                    0x0,
                    0x0,
                    0x10
                },
                Package(0x4)
                {
                    0x3ffff,
                    0x0,
                    0x0,
                    0x10
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x0,
                    0x0,
                    0x10
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x1,
                    0x0,
                    0x11
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x2,
                    0x0,
                    0x12
                },
                Package(0x4)
                {
                    0x16ffff,
                    0x3,
                    0x0,
                    0x13
                },
                Package(0x4)
                {
                    0x19ffff,
                    0x0,
                    0x0,
                    0x14
                },
                Package(0x4)
                {
                    0x1affff,
                    0x0,
                    0x0,
                    0x14
                },
                Package(0x4)
                {
                    0x1affff,
                    0x1,
                    0x0,
                    0x15
                },
                Package(0x4)
                {
                    0x1affff,
                    0x2,
                    0x0,
                    0x16
                },
                Package(0x4)
                {
                    0x1affff,
                    0x3,
                    0x0,
                    0x17
                },
                Package(0x4)
                {
                    0x1bffff,
                    0x1,
                    0x0,
                    0x11
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x0,
                    0x0,
                    0x14
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x1,
                    0x0,
                    0x15
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x2,
                    0x0,
                    0x16
                },
                Package(0x4)
                {
                    0x1cffff,
                    0x3,
                    0x0,
                    0x17
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x0,
                    0x0,
                    0x10
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x1,
                    0x0,
                    0x11
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x2,
                    0x0,
                    0x12
                },
                Package(0x4)
                {
                    0x1dffff,
                    0x3,
                    0x0,
                    0x13
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x0,
                    0x0,
                    0x17
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x1,
                    0x0,
                    0x10
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x2,
                    0x0,
                    0x11
                },
                Package(0x4)
                {
                    0x1fffff,
                    0x3,
                    0x0,
                    0x13
                }
            })
            Method(_PRT, 0x0, NotSerialized)
            {
                If(\GPIC)
                {
                    Return(ARRT)
                }
                Else
                {
                    Return(LRRT)
                }
            }
            Name(_HID, 0x80ad041)
            Name(_CID, 0x30ad041)
            OperationRegion(MHCS, PCI_Config, 0x40, 0xc0)
            Field(MHCS, DWordAcc, NoLock, Preserve)
            {
                Offset(0x70),
                , 4,
                TLUD, 12
            }
            Name(_CRS, Buffer(0x1c0)
            {
	0x88, 0x0d, 0x00, 0x02, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfe, 0x00,
	0x00, 0x00, 0xff, 0x00, 0x47, 0x01, 0xf8, 0x0c, 0xf8, 0x0c, 0x01, 0x08,
	0x88, 0x0d, 0x00, 0x01, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0xf7, 0x0c,
	0x00, 0x00, 0xf8, 0x0c, 0x88, 0x0d, 0x00, 0x01, 0x0c, 0x03, 0x00, 0x00,
	0x00, 0x0d, 0xff, 0xff, 0x00, 0x00, 0x00, 0xf3, 0x87, 0x17, 0x00, 0x00,
	0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a, 0x00, 0xff, 0xff,
	0x0b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x87, 0x17,
	0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00,
	0xff, 0x3f, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00,
	0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x0c, 0x00, 0xff, 0x7f, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x00, 0x00, 0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x80, 0x0c, 0x00, 0xff, 0xbf, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x40, 0x00, 0x00, 0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00,
	0x00, 0x00, 0x00, 0xc0, 0x0c, 0x00, 0xff, 0xff, 0x0c, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x87, 0x17, 0x00, 0x00, 0x0c, 0x03,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x00, 0xff, 0x3f, 0x0d, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x87, 0x17, 0x00, 0x00,
	0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x0d, 0x00, 0xff, 0x7f,
	0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x87, 0x17,
	0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x0d, 0x00,
	0xff, 0xbf, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00,
	0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0,
	0x0d, 0x00, 0xff, 0xff, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40,
	0x00, 0x00, 0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x0e, 0x00, 0xff, 0x3f, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x40, 0x00, 0x00, 0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x40, 0x0e, 0x00, 0xff, 0x7f, 0x0e, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x87, 0x17, 0x00, 0x00, 0x0c, 0x03,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x0e, 0x00, 0xff, 0xbf, 0x0e, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x87, 0x17, 0x00, 0x00,
	0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x0e, 0x00, 0xff, 0xff,
	0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x87, 0x17,
	0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00,
	0xff, 0xff, 0xbf, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xb0, 0xfe,
	0x87, 0x17, 0x00, 0x00, 0x0c, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0xd4, 0xfe, 0xff, 0xbf, 0xd4, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0,
	0x00, 0x00, 0x79, 0x00
            })
            CreateDWordField(_CRS, 0x68, C0LN)
            CreateDWordField(_CRS, 0x82, C4LN)
            CreateDWordField(_CRS, 0x9c, C8LN)
            CreateDWordField(_CRS, 0xb6, CCLN)
            CreateDWordField(_CRS, 0xd0, D0LN)
            CreateDWordField(_CRS, 0xea, D4LN)
            CreateDWordField(_CRS, 0x104, D8LN)
            CreateDWordField(_CRS, 0x11e, DCLN)
            CreateDWordField(_CRS, 0x138, E0LN)
            CreateDWordField(_CRS, 0x152, E4LN)
            CreateDWordField(_CRS, 0x16c, E8LN)
            CreateDWordField(_CRS, 0x186, ECLN)
            CreateDWordField(_CRS, 0x194, XXMN)
            CreateDWordField(_CRS, 0x198, XXMX)
            CreateDWordField(_CRS, 0x1a0, XXLN)
            CreateDWordField(_CRS, 0x1ae, F4MN)
            CreateDWordField(_CRS, 0x1b2, F4MX)
            CreateDWordField(_CRS, 0x1ba, F4LN)
            Method(_INI, 0x0, Serialized)
            {
                If(LNot(\OSIF))
                {
                    \_SB_._INI()
                }
                If(LEqual(\PNHM, 0x106e0))
                {
                    ShiftLeft(Increment(\_SB_.PCI0.IIO1.TOUD), 0x1a, Local0)
                }
                Else
                {
                    ShiftLeft(TLUD, 0x14, Local0)
                }
                Store(Local0, \MEMX)
                Store(Local0, XXMN)
                Add(Subtract(XXMX, XXMN, ), 0x1, XXLN)
                If(LNot(LEqual(And(\TPRS, 0x1, ), 0x1)))
                {
                    Store(0x0, F4LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM1, 0x3, ))
                {
                    Store(0x0, C0LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM1, 0x30, ))
                {
                    Store(0x0, C4LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM2, 0x3, ))
                {
                    Store(0x0, C8LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM2, 0x30, ))
                {
                    Store(0x0, CCLN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM3, 0x3, ))
                {
                    Store(0x0, D0LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM3, 0x30, ))
                {
                    Store(0x0, D4LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM4, 0x3, ))
                {
                    Store(0x0, D8LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM4, 0x30, ))
                {
                    Store(0x0, DCLN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM5, 0x3, ))
                {
                    Store(0x0, E0LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM5, 0x30, ))
                {
                    Store(0x0, E4LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM6, 0x3, ))
                {
                    Store(0x0, E8LN)
                }
                If(And(\_SB_.UNCR.SAD_.PAM6, 0x30, ))
                {
                    Store(0x0, ECLN)
                }
            }
            Name(SUPP, 0x0)
            Name(CTRL, 0x0)
            Method(_OSC, 0x4, NotSerialized)
            {
                CreateDWordField(Arg3, 0x0, CDW1)
                CreateDWordField(Arg3, 0x4, CDW2)
                CreateDWordField(Arg3, 0x8, CDW3)
                CreateDWordField(Arg0, 0x0, IID0)
                CreateDWordField(Arg0, 0x4, IID1)
                CreateDWordField(Arg0, 0x8, IID2)
                CreateDWordField(Arg0, 0xc, IID3)
                Name(UID0, Buffer(0x10)
                {
	0x5b, 0x4d, 0xdb, 0x33, 0xf7, 0x1f, 0x1c, 0x40, 0x96, 0x57, 0x74, 0x41,
	0xc0, 0x3d, 0xd7, 0x66
                })
                CreateDWordField(UID0, 0x0, EID0)
                CreateDWordField(UID0, 0x4, EID1)
                CreateDWordField(UID0, 0x8, EID2)
                CreateDWordField(UID0, 0xc, EID3)
                If(LAnd(LAnd(LEqual(IID0, EID0), LEqual(IID1, EID1)), LAnd(LEqual(IID2, EID2), LEqual(IID3, EID3))))
                {
                    Store(CDW2, SUPP)
                    Store(CDW3, CTRL)
                    And(CTRL, 0xd, CTRL)
                    If(LNot(And(CDW1, 0x1, )))
                    {
                        If(And(CTRL, 0x1, ))
                        {
                            If(LNot(\VIGD))
                            {
                                Store(0x0, \_SB_.PCI0.PEG_.HPGP)
                                Store(0x0, \_SB_.PCI0.PEG_.GMGP)
                            }
                            Store(0x0, \_SB_.PCI0.EXP4.HPCE)
                            Store(0x1, \_SB_.PCI0.EXP4.HPCS)
                            Store(0x1, \_SB_.PCI0.EXP4.PDC_)
                            Store(0x1, \NHPS)
                        }
                        If(And(CTRL, 0x4, ))
                        {
                            If(LNot(\VIGD))
                            {
                                Store(0x0, \_SB_.PCI0.PEG_.PMGP)
                                Store(0x0, \_SB_.PCI0.PEG_.GMGP)
                            }
                            If(\_SB_.PCI0.EXP1.PMCE)
                            {
                                Store(0x0, \_SB_.PCI0.EXP1.PMCE)
                                Store(0x1, \_SB_.PCI0.EXP1.PMCS)
                            }
                            If(\_SB_.PCI0.EXP2.PMCE)
                            {
                                Store(0x0, \_SB_.PCI0.EXP2.PMCE)
                                Store(0x1, \_SB_.PCI0.EXP2.PMCS)
                            }
                            If(\_SB_.PCI0.EXP3.PMCE)
                            {
                                Store(0x0, \_SB_.PCI0.EXP3.PMCE)
                                Store(0x1, \_SB_.PCI0.EXP3.PMCS)
                            }
                            If(\_SB_.PCI0.EXP4.PMCE)
                            {
                                Store(0x0, \_SB_.PCI0.EXP4.PMCE)
                                Store(0x1, \_SB_.PCI0.EXP4.PMCS)
                            }
                            If(\_SB_.PCI0.EXP5.PMCE)
                            {
                                Store(0x0, \_SB_.PCI0.EXP5.PMCE)
                                Store(0x1, \_SB_.PCI0.EXP5.PMCS)
                            }
                            Store(0x0, \_SB_.PCI0.LPC_.EXPE)
                            Store(0x1, \NPME)
                        }
                    }
                    If(LNot(LEqual(Arg1, 0x1)))
                    {
                        Or(CDW1, 0xa, CDW1)
                    }
                    If(LNot(LEqual(CDW3, CTRL)))
                    {
                        Or(CDW1, 0x10, CDW1)
                    }
                    Store(CTRL, CDW3)
                }
                Else
                {
                    Or(CDW1, 0x6, CDW1)
                }
                Return(Arg3)
            }
            Mutex(MDGS, 0x7)
            Name(VDEE, 0x1)
            Name(VDDA, Buffer(0x2)
            {
            })
            CreateBitField(VDDA, 0x0, VUPC)
            CreateBitField(VDDA, 0x1, VQDL)
            CreateBitField(VDDA, 0x2, VQDC)
            CreateBitField(VDDA, 0x3, VQD0)
            CreateBitField(VDDA, 0x4, VQD1)
            CreateBitField(VDDA, 0x5, VQD2)
            CreateBitField(VDDA, 0x6, VSDL)
            CreateBitField(VDDA, 0x7, VSDC)
            CreateBitField(VDDA, 0x8, VSD0)
            CreateBitField(VDDA, 0x9, VSD1)
            CreateBitField(VDDA, 0xa, VSD2)
            CreateBitField(VDDA, 0xb, VSD3)
            CreateBitField(VDDA, 0xc, VSD4)
            CreateBitField(VDDA, 0xd, VSD5)
            CreateBitField(VDDA, 0xe, MSWT)
            Device(VID_)
            {
                Name(_ADR, 0x20000)
                Name(RID_, 0x0)
                OperationRegion(VPCG, PCI_Config, 0x0, 0x100)
                Field(VPCG, DWordAcc, NoLock, Preserve)
                {
                    Offset(0xd4),
                    VPWR, 8
                }
                Name(_S3D, 0x3)
                Method(_INI, 0x0, NotSerialized)
                {
                    Noop
                }
                Method(_PS0, 0x0, NotSerialized)
                {
                    Noop
                }
                Method(_PS1, 0x0, NotSerialized)
                {
                    Noop
                }
                Method(_PS2, 0x0, NotSerialized)
                {
                    Noop
                }
                Method(_PS3, 0x0, NotSerialized)
                {
                    Noop
                }
                Method(VSWT, 0x0, NotSerialized)
                {
                    GHDS(0x0)
                }
                Method(VLOC, 0x1, NotSerialized)
                {
                    If(LEqual(Arg0, \_SB_.LID_._LID()))
                    {
                        \VSLD(Arg0)
                        If(LEqual(VPWR, 0x0))
                        {
                            Store(Arg0, CLID)
                            GNOT(0x2, 0x0)
                        }
                    }
                }
                Method(_DOS, 0x1, NotSerialized)
                {
                    And(Arg0, 0x3, Arg0)
                    If(LEqual(Arg0, 0x2))
                    {
                        Store(0x14, Local0)
                        While(Local0)
                        {
                            Decrement(Local0)
                            Acquire(MDGS, 0xffff)
                            If(LEqual(0x0, MSWT))
                            {
                                Store(0x1, MSWT)
                                Store(0x0, Local0)
                                Store(Arg0, VDEE)
                            }
                            Release(MDGS)
                            Sleep(0xc8)
                        }
                    }
                    Else
                    {
                        Acquire(MDGS, 0xffff)
                        If(LEqual(VDEE, 0x2))
                        {
                            Store(0x0, MSWT)
                        }
                        If(LGreater(Arg0, 0x2))
                        {
                            Store(0x1, VDEE)
                        }
                        Else
                        {
                            Store(Arg0, VDEE)
                        }
                        Release(MDGS)
                    }
                }
                Method(_DOD, 0x0, NotSerialized)
                {
                    Store(0x0, NDID)
                    If(LNot(LEqual(DIDL, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL2, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL3, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL4, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL5, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL6, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL7, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LNot(LEqual(DDL8, 0x0)))
                    {
                        Increment(NDID)
                    }
                    If(LEqual(NDID, 0x1))
                    {
                        Name(TMP1, Package(0x1)
                        {
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP1, 0x0, ))
                        Return(TMP1)
                    }
                    If(LEqual(NDID, 0x2))
                    {
                        Name(TMP2, Package(0x2)
                        {
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP2, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP2, 0x1, ))
                        Return(TMP2)
                    }
                    If(LEqual(NDID, 0x3))
                    {
                        Name(TMP3, Package(0x3)
                        {
                            0xffffffff,
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP3, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP3, 0x1, ))
                        Store(Or(0x10000, And(0xf0f, DDL3, ), ), Index(TMP3, 0x2, ))
                        Return(TMP3)
                    }
                    If(LEqual(NDID, 0x4))
                    {
                        Name(TMP4, Package(0x4)
                        {
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP4, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP4, 0x1, ))
                        Store(Or(0x10000, And(0xf0f, DDL3, ), ), Index(TMP4, 0x2, ))
                        Store(Or(0x10000, And(0xf0f, DDL4, ), ), Index(TMP4, 0x3, ))
                        Return(TMP4)
                    }
                    If(LEqual(NDID, 0x5))
                    {
                        Name(TMP5, Package(0x5)
                        {
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP5, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP5, 0x1, ))
                        Store(Or(0x10000, And(0xf0f, DDL3, ), ), Index(TMP5, 0x2, ))
                        Store(Or(0x10000, And(0xf0f, DDL4, ), ), Index(TMP5, 0x3, ))
                        Store(Or(0x10000, And(0xf0f, DDL5, ), ), Index(TMP5, 0x4, ))
                        Return(TMP5)
                    }
                    If(LEqual(NDID, 0x6))
                    {
                        Name(TMP6, Package(0x6)
                        {
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP6, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP6, 0x1, ))
                        Store(Or(0x10000, And(0xf0f, DDL3, ), ), Index(TMP6, 0x2, ))
                        Store(Or(0x10000, And(0xf0f, DDL4, ), ), Index(TMP6, 0x3, ))
                        Store(Or(0x10000, And(0xf0f, DDL5, ), ), Index(TMP6, 0x4, ))
                        Store(Or(0x10000, And(0xf0f, DDL6, ), ), Index(TMP6, 0x5, ))
                        Return(TMP6)
                    }
                    If(LEqual(NDID, 0x7))
                    {
                        Name(TMP7, Package(0x7)
                        {
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP7, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP7, 0x1, ))
                        Store(Or(0x10000, And(0xf0f, DDL3, ), ), Index(TMP7, 0x2, ))
                        Store(Or(0x10000, And(0xf0f, DDL4, ), ), Index(TMP7, 0x3, ))
                        Store(Or(0x10000, And(0xf0f, DDL5, ), ), Index(TMP7, 0x4, ))
                        Store(Or(0x10000, And(0xf0f, DDL6, ), ), Index(TMP7, 0x5, ))
                        Store(Or(0x10000, And(0xf0f, DDL7, ), ), Index(TMP7, 0x6, ))
                        Return(TMP7)
                    }
                    If(LGreater(NDID, 0x7))
                    {
                        Name(TMP8, Package(0x8)
                        {
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff,
                            0xffffffff
                        })
                        Store(Or(0x10000, And(0xf0f, DIDL, ), ), Index(TMP8, 0x0, ))
                        Store(Or(0x10000, And(0xf0f, DDL2, ), ), Index(TMP8, 0x1, ))
                        Store(Or(0x10000, And(0xf0f, DDL3, ), ), Index(TMP8, 0x2, ))
                        Store(Or(0x10000, And(0xf0f, DDL4, ), ), Index(TMP8, 0x3, ))
                        Store(Or(0x10000, And(0xf0f, DDL5, ), ), Index(TMP8, 0x4, ))
                        Store(Or(0x10000, And(0xf0f, DDL6, ), ), Index(TMP8, 0x5, ))
                        Store(Or(0x10000, And(0xf0f, DDL7, ), ), Index(TMP8, 0x6, ))
                        Store(Or(0x10000, And(0xf0f, DDL8, ), ), Index(TMP8, 0x7, ))
                        Return(TMP8)
                    }
                    Return(Package(0x1)
                    {
                        0x400
                    })
                }
                Method(VDSW, 0x1, NotSerialized)
                {
                    If(LEqual(VPWR, 0x0))
                    {
                        GDCK(Arg0)
                    }
                }
                Method(VCAD, 0x1, NotSerialized)
                {
                    Store(0x0, Local0)
                    If(LEqual(And(DIDL, 0xf0f, ), Arg0))
                    {
                        Store(0xd, Local0)
                    }
                    Else
                    {
                        If(LEqual(And(DDL2, 0xf0f, ), Arg0))
                        {
                            Store(0xd, Local0)
                        }
                        Else
                        {
                            If(LEqual(And(DDL3, 0xf0f, ), Arg0))
                            {
                                Store(0xd, Local0)
                            }
                            Else
                            {
                                If(LEqual(And(DDL4, 0xf0f, ), Arg0))
                                {
                                    Store(0xd, Local0)
                                }
                                Else
                                {
                                    If(LEqual(And(DDL5, 0xf0f, ), Arg0))
                                    {
                                        Store(0xd, Local0)
                                    }
                                    Else
                                    {
                                        If(LEqual(And(DDL6, 0xf0f, ), Arg0))
                                        {
                                            Store(0xd, Local0)
                                        }
                                        Else
                                        {
                                            If(LEqual(And(DDL7, 0xf0f, ), Arg0))
                                            {
                                                Store(0xd, Local0)
                                            }
                                            Else
                                            {
                                                If(LEqual(And(DDL8, 0xf0f, ), Arg0))
                                                {
                                                    Store(0xd, Local0)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    If(LEqual(And(CPDL, 0xf0f, ), Arg0))
                    {
                        Or(0x10, Local0, Local0)
                    }
                    Else
                    {
                        If(LEqual(And(CPL2, 0xf0f, ), Arg0))
                        {
                            Or(0x10, Local0, Local0)
                        }
                        Else
                        {
                            If(LEqual(And(CPL3, 0xf0f, ), Arg0))
                            {
                                Or(0x10, Local0, Local0)
                            }
                            Else
                            {
                                If(LEqual(And(CPL4, 0xf0f, ), Arg0))
                                {
                                    Or(0x10, Local0, Local0)
                                }
                                Else
                                {
                                    If(LEqual(And(CPL5, 0xf0f, ), Arg0))
                                    {
                                        Or(0x10, Local0, Local0)
                                    }
                                    Else
                                    {
                                        If(LEqual(And(CPL6, 0xf0f, ), Arg0))
                                        {
                                            Or(0x10, Local0, Local0)
                                        }
                                        Else
                                        {
                                            If(LEqual(And(CPL7, 0xf0f, ), Arg0))
                                            {
                                                Or(0x10, Local0, Local0)
                                            }
                                            Else
                                            {
                                                If(LEqual(And(CPL8, 0xf0f, ), Arg0))
                                                {
                                                    Or(0x10, Local0, Local0)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    If(LEqual(And(CADL, 0xf0f, ), Arg0))
                    {
                        Or(0x2, Local0, Local0)
                    }
                    Else
                    {
                        If(LEqual(And(CAL2, 0xf0f, ), Arg0))
                        {
                            Or(0x2, Local0, Local0)
                        }
                        Else
                        {
                            If(LEqual(And(CAL3, 0xf0f, ), Arg0))
                            {
                                Or(0x2, Local0, Local0)
                            }
                            Else
                            {
                                If(LEqual(And(CAL4, 0xf0f, ), Arg0))
                                {
                                    Or(0x2, Local0, Local0)
                                }
                                Else
                                {
                                    If(LEqual(And(CAL5, 0xf0f, ), Arg0))
                                    {
                                        Or(0x2, Local0, Local0)
                                    }
                                    Else
                                    {
                                        If(LEqual(And(CAL6, 0xf0f, ), Arg0))
                                        {
                                            Or(0x2, Local0, Local0)
                                        }
                                        Else
                                        {
                                            If(LEqual(And(CAL7, 0xf0f, ), Arg0))
                                            {
                                                Or(0x2, Local0, Local0)
                                            }
                                            Else
                                            {
                                                If(LEqual(And(CAL8, 0xf0f, ), Arg0))
                                                {
                                                    Or(0x2, Local0, Local0)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Return(Local0)
                }
                Method(NDDS, 0x1, NotSerialized)
                {
                    If(LEqual(And(NADL, 0xf0f, ), Arg0))
                    {
                        Return(0x1)
                    }
                    Else
                    {
                        If(LEqual(And(NDL2, 0xf0f, ), Arg0))
                        {
                            Return(0x1)
                        }
                        Else
                        {
                            If(LEqual(And(NDL3, 0xf0f, ), Arg0))
                            {
                                Return(0x1)
                            }
                            Else
                            {
                                If(LEqual(And(NDL4, 0xf0f, ), Arg0))
                                {
                                    Return(0x1)
                                }
                                Else
                                {
                                    If(LEqual(And(NDL5, 0xf0f, ), Arg0))
                                    {
                                        Return(0x1)
                                    }
                                    Else
                                    {
                                        If(LEqual(And(NDL6, 0xf0f, ), Arg0))
                                        {
                                            Return(0x1)
                                        }
                                        Else
                                        {
                                            If(LEqual(And(NDL7, 0xf0f, ), Arg0))
                                            {
                                                Return(0x1)
                                            }
                                            Else
                                            {
                                                If(LEqual(And(NDL8, 0xf0f, ), Arg0))
                                                {
                                                    Return(0x1)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Return(0x0)
                }
                Device(LCD0)
                {
                    Name(_ADR, 0x400)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x400))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x400))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSDL)
                    }
                }
                Device(CRT0)
                {
                    Name(_ADR, 0x100)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x100))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x100))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSDC)
                    }
                }
                Device(DVI0)
                {
                    Name(_ADR, 0x300)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x300))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x300))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSD0)
                    }
                }
                Device(DVI1)
                {
                    Name(_ADR, 0x301)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x301))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x301))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSD1)
                    }
                }
                Device(DVI2)
                {
                    Name(_ADR, 0x302)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x302))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x302))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSD2)
                    }
                }
                Device(DVI3)
                {
                    Name(_ADR, 0x303)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x303))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x303))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSD3)
                    }
                }
                Device(DVI4)
                {
                    Name(_ADR, 0x304)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x304))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x304))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSD4)
                    }
                }
                Device(DVI5)
                {
                    Name(_ADR, 0x305)
                    Method(_DCS, 0x0, NotSerialized)
                    {
                        Return(VCAD(0x305))
                    }
                    Method(_DGS, 0x0, NotSerialized)
                    {
                        Return(NDDS(0x305))
                    }
                    Method(_DSS, 0x1, NotSerialized)
                    {
                        And(Arg0, 0x1, VSD5)
                    }
                }
                OperationRegion(IGDP, PCI_Config, 0x40, 0xc0)
                Field(IGDP, AnyAcc, NoLock, Preserve)
                {
                    Offset(0x12),
                    , 1,
                    GIVD, 1,
                    , 2,
                    GUMA, 3,
                    , 9,
                    , 4,
                    GMFN, 1,
                    , 27,
                    Offset(0xa4),
                    ASLE, 8,
                    , 24,
                    GSSE, 1,
                    GSSB, 14,
                    GSES, 1,
                    Offset(0xb0),
                    , 12,
                    CDVL, 1,
                    , 3,
                    , 24,
                    LBPC, 8,
                    Offset(0xbc),
                    ASLS, 32
                }
                OperationRegion(IGDM, SystemMemory, \ASLB, 0x2000)
                Field(IGDM, AnyAcc, NoLock, Preserve)
                {
                    SIGN, 128,
                    SIZE, 32,
                    OVER, 32,
                    SVER, 256,
                    VVER, 128,
                    GVER, 128,
                    MBOX, 32,
                    DMOD, 32,
                    Offset(0x100),
                    DRDY, 32,
                    CSTS, 32,
                    CEVT, 32,
                    Offset(0x120),
                    DIDL, 32,
                    DDL2, 32,
                    DDL3, 32,
                    DDL4, 32,
                    DDL5, 32,
                    DDL6, 32,
                    DDL7, 32,
                    DDL8, 32,
                    CPDL, 32,
                    CPL2, 32,
                    CPL3, 32,
                    CPL4, 32,
                    CPL5, 32,
                    CPL6, 32,
                    CPL7, 32,
                    CPL8, 32,
                    CADL, 32,
                    CAL2, 32,
                    CAL3, 32,
                    CAL4, 32,
                    CAL5, 32,
                    CAL6, 32,
                    CAL7, 32,
                    CAL8, 32,
                    NADL, 32,
                    NDL2, 32,
                    NDL3, 32,
                    NDL4, 32,
                    NDL5, 32,
                    NDL6, 32,
                    NDL7, 32,
                    NDL8, 32,
                    ASLP, 32,
                    TIDX, 32,
                    CHPD, 32,
                    CLID, 32,
                    CDCK, 32,
                    SXSW, 32,
                    EVTS, 32,
                    CNOT, 32,
                    NRDY, 32,
                    Offset(0x200),
                    SCIE, 1,
                    GEFC, 4,
                    GXFC, 3,
                    GESF, 8,
                    , 16,
                    PARM, 32,
                    DSLP, 32,
                    Offset(0x300),
                    ARDY, 32,
                    ASLC, 32,
                    TCHE, 32,
                    ALSI, 32,
                    BCLP, 32,
                    PFIT, 32,
                    CBLV, 32,
                    BCLM, 320,
                    CPFM, 32,
                    EPFM, 32,
                    PLUT, 592,
                    PFMB, 32,
                    CCDV, 32,
                    PCFT, 32,
                    Offset(0x400),
                    GVD1, 49152,
                    PHED, 32,
                    BDDC, 2048
                }
                Name(DBTB, Package(0x15)
                {
                    0x0,
                    0x7,
                    0x38,
                    0x1c0,
                    0xe00,
                    0x3f,
                    0x1c7,
                    0xe07,
                    0x1f8,
                    0xe38,
                    0xfc0,
                    0x0,
                    0x0,
                    0x0,
                    0x0,
                    0x0,
                    0x7000,
                    0x7007,
                    0x7038,
                    0x71c0,
                    0x7e00
                })
                Name(DBTC, Package(0x15)
                {
                    0x0,
                    0x2,
                    0x1,
                    0x8,
                    0x0,
                    0x3,
                    0xa,
                    0x0,
                    0x9,
                    0x0,
                    0x0,
                    0x0,
                    0x0,
                    0x0,
                    0x0,
                    0x0,
                    0x4,
                    0x6,
                    0x5,
                    0xc,
                    0x0
                })
                Name(SUCC, 0x1)
                Name(NVLD, 0x2)
                Name(CRIT, 0x4)
                Name(NCRT, 0x6)
                Method(GSCI, 0x0, Serialized)
                {
                    If(LEqual(GEFC, 0x4))
                    {
                        Store(GBDA(), GXFC)
                    }
                    If(LEqual(GEFC, 0x6))
                    {
                        Store(SBCB(), GXFC)
                    }
                    Store(0x0, GEFC)
                    Store(0x1, \_SB_.PCI0.LPC_.SCIS)
                    Store(0x0, GSSE)
                    Store(0x0, SCIE)
                    Return(Zero)
                }
                Method(GBDA, 0x0, Serialized)
                {
                    If(LEqual(GESF, 0x0))
                    {
                        Store(0x201, PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x1))
                    {
                        Store(0x202, PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x4))
                    {
                        And(PARM, 0xefff0000, PARM)
                        And(PARM, ShiftLeft(DerefOf(Index(DBTB, IBTT, )), 0x10, ), PARM)
                        Or(IBTT, PARM, PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x5))
                    {
                        If(\_SB_.LID_._LID())
                        {
                            Store(0x1, LIDS)
                        }
                        Else
                        {
                            Store(0x0, LIDS)
                        }
                        Store(IPSC, PARM)
                        Add(PARM, 0x1, PARM)
                        Add(PARM, 0x300, PARM)
                        Add(PARM, 0x10000, PARM)
                        Or(PARM, ShiftLeft(LIDS, 0x10, ), PARM)
                        Or(PARM, ShiftLeft(IBIA, 0x14, ), PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x6))
                    {
                        Store(ITVF, PARM)
                        Or(PARM, ShiftLeft(ITVM, 0x4, ), PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x7))
                    {
                        Store(0x1, PARM)
                        Or(PARM, ShiftLeft(0x3, 0xb, ), PARM)
                        Or(PARM, ShiftLeft(0x3, 0x11, ), PARM)
                        Or(PARM, ShiftLeft(0x85, 0x15, ), PARM)
                        Store(0x1, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0xa))
                    {
                        Store(0x0, PARM)
                        Store(0x0, GESF)
                        Return(SUCC)
                    }
                    Store(Zero, GESF)
                    Return(CRIT)
                }
                Name(EXTD, 0x0)
                Method(SBCB, 0x0, Serialized)
                {
                    If(LEqual(GESF, 0x0))
                    {
                        Store(0x101, PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x1))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x3))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x4))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x5))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x6))
                    {
                        Store(And(PARM, 0xf, ), ITVF)
                        Store(ShiftRight(And(PARM, 0xf0, ), 0x4, ), ITVM)
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x7))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x8))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x9))
                    {
                        And(PARM, 0x80000000, EXTD)
                        And(PARM, 0xff, Local0)
                        If(LNot(LGreater(Local0, 0x14)))
                        {
                            Store(DerefOf(Index(DBTC, Local0, )), IBTT)
                        }
                        Else
                        {
                            Store(0x0, IBTT)
                        }
                        If(IBTT)
                        {
                            \UCMS(0x17)
                        }
                        And(PARM, 0xff, IBTT)
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0xa))
                    {
                        Store(Subtract(And(PARM, 0x3, ), 0x1, ), IPSC)
                        \UCMS(0x1a)
                        If(And(ShiftRight(PARM, 0x8, ), 0xff, ))
                        {
                            And(ShiftRight(PARM, 0x8, ), 0xff, IPAT)
                            Decrement(IPAT)
                        }
                        And(ShiftRight(PARM, 0x14, ), 0x7, IBIA)
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0xb))
                    {
                        And(ShiftRight(PARM, 0x1, ), 0x1, IF1E)
                        If(And(PARM, ShiftLeft(0xf, 0xd, ), ))
                        {
                            And(ShiftRight(PARM, 0xd, ), 0xf, IDMS)
                            Store(0x0, IDMM)
                        }
                        Else
                        {
                            And(ShiftRight(PARM, 0x11, ), 0xf, IDMS)
                            Store(0x1, IDMM)
                        }
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x10))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x11))
                    {
                        Store(ShiftLeft(LIDS, 0x8, ), PARM)
                        Add(PARM, 0x100, PARM)
                        Store(Zero, GESF)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x12))
                    {
                        If(And(PARM, 0x1, ))
                        {
                            If(LEqual(ShiftRight(PARM, 0x1, ), 0x1))
                            {
                                Store(0x1, ISSC)
                            }
                            Else
                            {
                                Store(Zero, GESF)
                                Return(CRIT)
                            }
                        }
                        Else
                        {
                            Store(0x0, ISSC)
                        }
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x13))
                    {
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    If(LEqual(GESF, 0x14))
                    {
                        And(PARM, 0xf, PAVP)
                        Store(Zero, GESF)
                        Store(Zero, PARM)
                        Return(SUCC)
                    }
                    Store(Zero, GESF)
                    Return(SUCC)
                }
                Method(PDRD, 0x0, NotSerialized)
                {
                    If(LNot(DRDY))
                    {
                        Sleep(ASLP)
                    }
                    Return(LNot(DRDY))
                }
                Method(PSTS, 0x0, NotSerialized)
                {
                    If(LGreater(CSTS, 0x2))
                    {
                        Sleep(ASLP)
                    }
                    Return(LEqual(CSTS, 0x3))
                }
                Method(GNOT, 0x2, NotSerialized)
                {
                    If(PDRD())
                    {
                        Return(0x1)
                    }
                    Store(Arg0, CEVT)
                    Store(0x3, CSTS)
                    If(LAnd(LEqual(CHPD, 0x0), LEqual(Arg1, 0x0)))
                    {
                        If(LNot(LEqual(Arg0, 0x1)))
                        {
                            If(LAnd(\WXPF, LNot(\WVIS)))
                            {
                                Notify(\_SB_.PCI0, Arg1)
                            }
                            Else
                            {
                                Notify(\_SB_.PCI0.VID_, Arg1)
                            }
                        }
                    }
                    Notify(\_SB_.PCI0.VID_, 0x80)
                    Return(0x0)
                }
                Method(GHDS, 0x1, NotSerialized)
                {
                    Store(Arg0, TIDX)
                    Return(GNOT(0x1, 0x0))
                }
                Method(GLID, 0x1, NotSerialized)
                {
                    Store(Arg0, CLID)
                    Return(GNOT(0x2, 0x0))
                }
                Method(GLIS, 0x1, NotSerialized)
                {
                    Store(Arg0, CLID)
                    Return(0x0)
                }
                Method(GDCK, 0x1, NotSerialized)
                {
                    Store(Arg0, CDCK)
                    Return(GNOT(0x4, 0x80))
                }
                Method(GDCS, 0x1, NotSerialized)
                {
                    Store(Arg0, CDCK)
                }
                Method(PARD, 0x0, NotSerialized)
                {
                    If(LNot(ARDY))
                    {
                        Sleep(ASLP)
                    }
                    Return(LNot(ARDY))
                }
                Method(AINT, 0x2, NotSerialized)
                {
                    If(LNot(And(TCHE, ShiftLeft(0x1, Arg0, ), )))
                    {
                        Return(0x1)
                    }
                    If(PARD())
                    {
                        Return(0x1)
                    }
                    If(LEqual(Arg0, 0x2))
                    {
                        XOr(PFIT, 0x7, PFIT)
                        Or(PFIT, 0x80000000, PFIT)
                        Store(0x4, ASLC)
                    }
                    Else
                    {
                        If(LEqual(Arg0, 0x1))
                        {
                            Store(Arg1, BCLP)
                            Or(BCLP, 0x80000000, BCLP)
                            Store(0xa, ASLC)
                        }
                        Else
                        {
                            If(LEqual(Arg0, 0x3))
                            {
                                Store(Arg1, PFMB)
                                Or(PFMB, 0x80000100, PFMB)
                            }
                            Else
                            {
                                If(LEqual(Arg0, 0x0))
                                {
                                    Store(Arg1, ALSI)
                                    Store(0x1, ASLC)
                                }
                                Else
                                {
                                    Return(0x1)
                                }
                            }
                        }
                    }
                    Store(0x1, ASLE)
                    Return(0x0)
                }
            }
            Device(LPC_)
            {
                Name(_ADR, 0x1f0000)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
                Device(SIO_)
                {
                    Name(_HID, 0x20cd041)
                    Name(_UID, 0x0)
                    Name(SCRS, Buffer(0x11e)
                    {
	0x47, 0x01, 0x10, 0x00, 0x10, 0x00, 0x01, 0x10, 0x47, 0x01, 0x90, 0x00,
	0x90, 0x00, 0x01, 0x10, 0x47, 0x01, 0x24, 0x00, 0x24, 0x00, 0x01, 0x02,
	0x47, 0x01, 0x28, 0x00, 0x28, 0x00, 0x01, 0x02, 0x47, 0x01, 0x2c, 0x00,
	0x2c, 0x00, 0x01, 0x02, 0x47, 0x01, 0x30, 0x00, 0x30, 0x00, 0x01, 0x02,
	0x47, 0x01, 0x34, 0x00, 0x34, 0x00, 0x01, 0x02, 0x47, 0x01, 0x38, 0x00,
	0x38, 0x00, 0x01, 0x02, 0x47, 0x01, 0x3c, 0x00, 0x3c, 0x00, 0x01, 0x02,
	0x47, 0x01, 0xa4, 0x00, 0xa4, 0x00, 0x01, 0x02, 0x47, 0x01, 0xa8, 0x00,
	0xa8, 0x00, 0x01, 0x02, 0x47, 0x01, 0xac, 0x00, 0xac, 0x00, 0x01, 0x02,
	0x47, 0x01, 0xb0, 0x00, 0xb0, 0x00, 0x01, 0x06, 0x47, 0x01, 0xb8, 0x00,
	0xb8, 0x00, 0x01, 0x02, 0x47, 0x01, 0xbc, 0x00, 0xbc, 0x00, 0x01, 0x02,
	0x47, 0x01, 0x50, 0x00, 0x50, 0x00, 0x01, 0x04, 0x47, 0x01, 0x72, 0x00,
	0x72, 0x00, 0x01, 0x06, 0x47, 0x01, 0x4e, 0x16, 0x4e, 0x16, 0x01, 0x02,
	0x47, 0x01, 0x2e, 0x00, 0x2e, 0x00, 0x01, 0x02, 0x47, 0x01, 0x00, 0x10,
	0x00, 0x10, 0x01, 0x80, 0x47, 0x01, 0x80, 0x11, 0x80, 0x11, 0x01, 0x80,
	0x47, 0x01, 0x00, 0x08, 0x00, 0x08, 0x01, 0x10, 0x47, 0x01, 0xe0, 0x15,
	0xe0, 0x15, 0x01, 0x10, 0x47, 0x01, 0x00, 0x16, 0x00, 0x16, 0x01, 0x42,
	0x47, 0x01, 0x44, 0x16, 0x44, 0x16, 0x01, 0x3c, 0x86, 0x09, 0x00, 0x01,
	0x00, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x10, 0x86, 0x09, 0x00, 0x01,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x86, 0x09, 0x00, 0x01,
	0x00, 0xc0, 0xd1, 0xfe, 0x00, 0x40, 0x00, 0x00, 0x86, 0x09, 0x00, 0x01,
	0x00, 0x00, 0xd1, 0xfe, 0x00, 0x40, 0x00, 0x00, 0x86, 0x09, 0x00, 0x01,
	0x00, 0x80, 0xd1, 0xfe, 0x00, 0x10, 0x00, 0x00, 0x86, 0x09, 0x00, 0x01,
	0x00, 0x90, 0xd1, 0xfe, 0x00, 0x10, 0x00, 0x00, 0x86, 0x09, 0x00, 0x01,
	0x00, 0x50, 0xd4, 0xfe, 0x00, 0x70, 0x00, 0x00, 0x79, 0x00
                    })
                    CreateDWordField(SCRS, 0xd8, TRMB)
                    Method(_CRS, 0x0, NotSerialized)
                    {
                        Store(\TBAB, TRMB)
                        If(LEqual(\_SB_.PCI0.LPC_.TPM_._STA(), 0xf))
                        {
                            Return(SCRS)
                        }
                        Else
                        {
                            Subtract(SizeOf(SCRS), 0x2, Local0)
                            Name(BUF0, Buffer(Local0)
                            {
                            })
                            Add(Local0, SizeOf(\_SB_.PCI0.LPC_.TPM_.BUF1), Local0)
                            Name(BUF1, Buffer(Local0)
                            {
                            })
                            Store(SCRS, BUF0)
                            Concatenate(BUF0, \_SB_.PCI0.LPC_.TPM_.BUF1, BUF1)
                            Return(BUF1)
                        }
                    }
                }
                OperationRegion(LPCS, PCI_Config, 0x0, 0x100)
                Field(LPCS, AnyAcc, NoLock, Preserve)
                {
                    Offset(0x60),
                    PIRA, 8,
                    PIRB, 8,
                    PIRC, 8,
                    PIRD, 8,
                    SERQ, 8,
                    , 24,
                    PIRE, 8,
                    PIRF, 8,
                    PIRG, 8,
                    PIRH, 8,
                    Offset(0x80),
                    XU1A, 3,
                    , 1,
                    XU2A, 3,
                    , 1,
                    XPA_, 2,
                    , 2,
                    XFA_, 1,
                    , 3,
                    XU1E, 1,
                    XU2E, 1,
                    XPE_, 1,
                    XFE_, 1,
                    , 4,
                    XGML, 1,
                    XGMH, 1,
                    , 6,
                    XG1E, 1,
                    , 1,
                    XG1A, 14,
                    , 16,
                    XG2E, 1,
                    , 1,
                    XG2A, 14,
                    Offset(0xa0),
                    , 2,
                    CLKR, 1,
                    , 7,
                    EXPE, 1,
                    , 5
                }
                OperationRegion(LPIO, SystemIO, 0x1180, 0x80)
                Field(LPIO, DWordAcc, NoLock, Preserve)
                {
                    Offset(0xc),
                    GL00, 8,
                    GL01, 8,
                    , 6,
                    GLIS, 1,
                    , 1,
                    GL03, 8
                }
                OperationRegion(PMIO, SystemIO, 0x1000, 0x80)
                Field(PMIO, AnyAcc, NoLock, Preserve)
                {
                    Offset(0x42),
                    , 1,
                    SWGE, 1,
                    Offset(0x64),
                    , 9,
                    SCIS, 1,
                    , 6
                }
                OperationRegion(IO_T, SystemIO, 0x800, 0x10)
                Field(IO_T, ByteAcc, NoLock, Preserve)
                {
                    TRPI, 16,
                    , 16,
                    , 16,
                    , 16,
                    TRP0, 8,
                    , 8,
                    , 8,
                    , 8,
                    , 8,
                    , 8,
                    , 8,
                    , 8
                }
                Device(PIC_)
                {
                    Name(_HID, 0xd041)
                    Name(_CRS, Buffer(0x1d)
                    {
	0x47, 0x01, 0x20, 0x00, 0x20, 0x00, 0x01, 0x02, 0x47, 0x01, 0xa0, 0x00,
	0xa0, 0x00, 0x01, 0x02, 0x47, 0x01, 0xd0, 0x04, 0xd0, 0x04, 0x01, 0x02,
	0x22, 0x04, 0x00, 0x79, 0x00
                    })
                }
                Device(TIMR)
                {
                    Name(_HID, 0x1d041)
                    Name(_CRS, Buffer(0xd)
                    {
	0x47, 0x01, 0x40, 0x00, 0x40, 0x00, 0x01, 0x04, 0x22, 0x01, 0x00, 0x79,
	0x00
                    })
                }
                Device(HPET)
                {
                    Name(_HID, 0x301d041)
                    Method(_STA, 0x0, NotSerialized)
                    {
                        If(LAnd(\WNTF, LNot(\WXPF)))
                        {
                            Return(0x0)
                        }
                        Else
                        {
                            Return(0xf)
                        }
                        Return(0x0)
                    }
                    Name(_CRS, Buffer(0xe)
                    {
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0xd0, 0xfe, 0x00, 0x04, 0x00, 0x00,
	0x79, 0x00
                    })
                }
                Device(DMAC)
                {
                    Name(_HID, 0x2d041)
                    Name(_CRS, Buffer(0x1d)
                    {
	0x47, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x10, 0x47, 0x01, 0x80, 0x00,
	0x80, 0x00, 0x01, 0x10, 0x47, 0x01, 0xc0, 0x00, 0xc0, 0x00, 0x01, 0x20,
	0x2a, 0x10, 0x05, 0x79, 0x00
                    })
                }
                Device(SPKR)
                {
                    Name(_HID, 0x8d041)
                    Name(_CRS, Buffer(0xa)
                    {
	0x47, 0x01, 0x61, 0x00, 0x61, 0x00, 0x01, 0x01, 0x79, 0x00
                    })
                }
                Device(FPU_)
                {
                    Name(_HID, 0x40cd041)
                    Name(_CRS, Buffer(0xd)
                    {
	0x47, 0x01, 0xf0, 0x00, 0xf0, 0x00, 0x01, 0x01, 0x22, 0x00, 0x20, 0x79,
	0x00
                    })
                }
                Device(RTC_)
                {
                    Name(_HID, 0xbd041)
                    Name(_CRS, Buffer(0xd)
                    {
	0x47, 0x01, 0x70, 0x00, 0x70, 0x00, 0x01, 0x02, 0x22, 0x00, 0x01, 0x79,
	0x00
                    })
                }
                Device(KBD_)
                {
                    Method(_HID, 0x0, Serialized)
                    {
                        If(LEqual(\PJID, 0x0))
                        {
                            Return(0x1000ae30)
                        }
                        Else
                        {
                            Return(0x303d041)
                        }
                    }
                    Name(_CID, 0x303d041)
                    Name(_CRS, Buffer(0x15)
                    {
	0x47, 0x01, 0x60, 0x00, 0x60, 0x00, 0x01, 0x01, 0x47, 0x01, 0x64, 0x00,
	0x64, 0x00, 0x01, 0x01, 0x22, 0x02, 0x00, 0x79, 0x00
                    })
                }
                Device(MOU_)
                {
                    Name(_HID, 0x80374d24)
                    Name(_CID, 0x130fd041)
                    Name(_CRS, Buffer(0x5)
                    {
	0x22, 0x00, 0x10, 0x79, 0x00
                    })
                    Method(MHID, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.PADD)
                        {
                            Store(0x80374d24, _HID)
                        }
                        Else
                        {
                            Store(0x1800ae30, _HID)
                        }
                    }
                }
                OperationRegion(IMGA, SystemIO, 0x15e0, 0x10)
                Field(IMGA, ByteAcc, NoLock, Preserve)
                {
                    , 8,
                    , 8,
                    , 8,
                    WAKR, 16,
                    Offset(0xc),
                    GAIX, 8,
                    , 8,
                    GADT, 8,
                    , 8
                }
                IndexField(GAIX, GADT, ByteAcc, NoLock, Preserve)
                {
                    Offset(0x50),
                    , 3,
                    IVPW, 1,
                    DVPW, 1,
                    BLPL, 1,
                    , 2,
                    TP4R, 1,
                    PADR, 1,
                    BPAD, 1,
                    , 1,
                    , 1,
                    PADD, 1,
                    Offset(0x60),
                    EPWG, 1,
                    , 1,
                    CSON, 1,
                    DSCI, 1,
                    DSCS, 1,
                    DLAN, 1,
                    Offset(0xc2),
                    GAID, 8
                }
                OperationRegion(CFGS, SystemIO, 0x164e, 0x2)
                Field(CFGS, ByteAcc, NoLock, Preserve)
                {
                    NDXS, 8,
                    ATAS, 8
                }
                IndexField(NDXS, ATAS, ByteAcc, NoLock, Preserve)
                {
                    Offset(0x7),
                    LDNS, 8,
                    Offset(0x20),
                    , 8,
                    , 8,
                    , 8,
                    , 8,
                    , 8,
                    , 2,
                    PSES, 1,
                    , 4,
                    PNFS, 1,
                    DCDS, 1,
                    PPDS, 1,
                    SP2S, 1,
                    SP1S, 1,
                    , 1,
                    PSRS, 2,
                    , 1,
                    RIDS, 8,
                    , 8,
                    CCSS, 2,
                    CCES, 1,
                    MCSS, 1,
                    MESS, 1,
                    , 3,
                    , 8,
                    Offset(0x30),
                    LDAS, 1,
                    , 7,
                    Offset(0x60),
                    OHIS, 8,
                    OLWS, 8,
                    Offset(0x70),
                    RQNS, 4,
                    RQWS, 1,
                    , 3,
                    RQTS, 1,
                    RQLS, 1,
                    , 6,
                    , 16,
                    MA0S, 3,
                    , 5,
                    MA1S, 3,
                    , 5,
                    Offset(0xf0),
                    DTSC, 1,
                    DCF1, 1,
                    DCF2, 1,
                    DCF3, 1,
                    DCF4, 1,
                    DCF5, 1,
                    DCF6, 1,
                    DCF7, 1
                }
                IndexField(NDXS, ATAS, ByteAcc, NoLock, Preserve)
                {
                    Offset(0xf0),
                    TRSS, 1,
                    PMCS, 1,
                    BSYS, 1,
                    , 4,
                    SESS, 1
                }
                OperationRegion(NSDL, SystemIO, 0x164c, 0x1)
                Field(NSDL, ByteAcc, NoLock, Preserve)
                {
                    DLPC, 8
                }
                Device(DTR_)
                {
                    Name(LCFG, 0x0)
                    Name(LBAR, 0x0)
                    Method(_INI, 0x0, NotSerialized)
                    {
                        Store(0x3, LDNS)
                        Or(ShiftLeft(OHIS, 0x8, ), OLWS, LBAR)
                    }
                    Method(_HID, 0x0, NotSerialized)
                    {
                        If(\DATD)
                        {
                            Return(0xcf0235c)
                        }
                        Else
                        {
                            Return(0x4f0235c)
                        }
                    }
                    Method(_STA, 0x0, NotSerialized)
                    {
                        If(LNot(LEqual(\PJID, 0x0)))
                        {
                            Return(0x0)
                        }
                        Store(0x3, LDNS)
                        If(LAnd(LDAS, LCFG))
                        {
                            Return(0xf)
                        }
                        Else
                        {
                            Return(0xd)
                        }
                    }
                    Method(_DIS, 0x0, NotSerialized)
                    {
                        Store(0x0, LCFG)
                        Store(0x3, LDNS)
                        Store(0x0, RQNS)
                        Store(0x0, LDAS)
                        If(LEqual(LBAR, 0x200))
                        {
                            Store(0x0, XGML)
                        }
                        Else
                        {
                            If(LEqual(LBAR, 0x208))
                            {
                                Store(0x0, XGMH)
                            }
                            Else
                            {
                                Fatal(0x2, 0x90020000, 0x53)
                            }
                        }
                    }
                    Name(DTBF, Buffer(0xd)
                    {
	0x47, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x08, 0x22, 0x00, 0x00, 0x79,
	0x00
                    })
                    CreateWordField(DTBF, 0x2, S1MN)
                    CreateWordField(DTBF, 0x4, S1MX)
                    CreateWordField(DTBF, 0x9, S1IQ)
                    Method(_CRS, 0x0, NotSerialized)
                    {
                        Store(0x3, LDNS)
                        Or(ShiftLeft(OHIS, 0x8, ), OLWS, S1MN)
                        Store(S1MN, S1MX)
                        Store(RQNS, Local0)
                        If(Local0)
                        {
                            ShiftLeft(0x1, RQNS, S1IQ)
                        }
                        Else
                        {
                            Store(0x0, S1IQ)
                        }
                        Return(DTBF)
                    }
                    Name(_PRS, Buffer(0x37)
                    {
	0x31, 0x00, 0x47, 0x01, 0x00, 0x02, 0x00, 0x02, 0x01, 0x08, 0x22, 0x20,
	0x00, 0x31, 0x01, 0x47, 0x01, 0x08, 0x02, 0x08, 0x02, 0x01, 0x08, 0x22,
	0x08, 0x00, 0x31, 0x02, 0x47, 0x01, 0x00, 0x02, 0x00, 0x02, 0x01, 0x08,
	0x22, 0xd8, 0x00, 0x31, 0x02, 0x47, 0x01, 0x08, 0x02, 0x08, 0x02, 0x01,
	0x08, 0x22, 0xf0, 0x00, 0x38, 0x79, 0x00
                    })
                    Method(_SRS, 0x1, NotSerialized)
                    {
                        CreateByteField(Arg0, 0x2, RDIL)
                        CreateByteField(Arg0, 0x3, RDIH)
                        CreateWordField(Arg0, 0x2, RDIO)
                        CreateWordField(Arg0, 0x9, RDIQ)
                        Store(0x3, LDNS)
                        Store(0x0, LDAS)
                        Store(0x0, RQNS)
                        Store(RDIL, OLWS)
                        Store(RDIH, OHIS)
                        If(RDIQ)
                        {
                            FindSetRightBit(RDIQ, Local2)
                            Store(Decrement(Local2), RQNS)
                        }
                        Else
                        {
                            Store(0x0, RQNS)
                        }
                        Store(0x1, LDAS)
                        If(LEqual(RDIO, 0x200))
                        {
                            Store(0x1, XGML)
                            Store(0x0, XGMH)
                        }
                        Else
                        {
                            If(LEqual(RDIO, 0x208))
                            {
                                Store(0x0, XGML)
                                Store(0x1, XGMH)
                            }
                            Else
                            {
                                Fatal(0x2, 0x90020000, 0xda)
                            }
                        }
                        Store(RDIO, LBAR)
                        Store(0x1, LCFG)
                    }
                    Method(_PSC, 0x0, NotSerialized)
                    {
                        Store(0x3, LDNS)
                        If(DCF1)
                        {
                            Return(0x0)
                        }
                        Else
                        {
                            Return(0x3)
                        }
                    }
                    Method(_PS0, 0x0, NotSerialized)
                    {
                        Store(0x3, LDNS)
                        Store(0x1, DCF1)
                    }
                    Method(_PS3, 0x0, NotSerialized)
                    {
                        Store(0x3, LDNS)
                        Store(0x0, DCF1)
                    }
                }
                Device(TPM_)
                {
                    Method(_HID, 0x0, NotSerialized)
                    {
                        TPHY(0x0)
                        If(LEqual(TPMV, 0x1))
                        {
                            Return(0x201d824)
                        }
                        If(LEqual(TPMV, 0x2))
                        {
                            Return(0x435cf4d)
                        }
                        If(LEqual(TPMV, 0x3))
                        {
                            Return(0x2016d08)
                        }
                        If(LEqual(TPMV, 0x4))
                        {
                            Return(0x1016d08)
                        }
                        If(LOr(LEqual(TPMV, 0x5), LEqual(TPMV, 0x6)))
                        {
                            Return(0x10a35c)
                        }
                        If(LEqual(TPMV, 0x8))
                        {
                            Return(0x128d06)
                        }
                        If(LEqual(TPMV, 0x9))
                        {
                            Return("INTC0102")
                        }
                        If(LEqual(TPMV, 0xa))
                        {
                            Return("SMO1200")
                        }
                        Return(0x310cd041)
                    }
                    Name(_CID, 0x310cd041)
                    Name(_UID, 0x1)
                    Method(_STA, 0x0, NotSerialized)
                    {
                        TPHY(0x0)
                        If(TPRS)
                        {
                            Return(0xf)
                        }
                        Return(0x0)
                    }
                    Name(BUF1, Buffer(0xe)
                    {
	0x86, 0x09, 0x00, 0x00, 0x00, 0x00, 0xd4, 0xfe, 0x00, 0x50, 0x00, 0x00,
	0x79, 0x00
                    })
                    Method(_CRS, 0x0, Serialized)
                    {
                        Return(BUF1)
                    }
                    Method(UCMP, 0x2, NotSerialized)
                    {
                        If(LNot(LEqual(0x10, SizeOf(Arg0))))
                        {
                            Return(0x0)
                        }
                        If(LNot(LEqual(0x10, SizeOf(Arg1))))
                        {
                            Return(0x0)
                        }
                        Store(0x0, Local0)
                        While(LLess(Local0, 0x10))
                        {
                            If(LNot(LEqual(DerefOf(Index(Arg0, Local0, )), DerefOf(Index(Arg1, Local0, )))))
                            {
                                Return(0x0)
                            }
                            Increment(Local0)
                        }
                        Return(0x1)
                    }
                    Method(_DSM, 0x4, Serialized)
                    {
                        Name(PPRC, 0x0)
                        Name(RQS1, Package(0x2)
                        {
                            0xc,
                            0xd
                        })
                        Name(TTMP, Buffer(0x1)
                        {
	0x00
                        })
                        CreateByteField(TTMP, 0x0, TMPV)
                        If(LEqual(UCMP(Arg0, Buffer(0x10)
                        {
	0xa6, 0xfa, 0xdd, 0x3d, 0x1b, 0x36, 0xb4, 0x4e, 0xa4, 0x24, 0x8d, 0x10,
	0x08, 0x9d, 0x16, 0x53
                        }), 0x1))
                        {
                            If(LEqual(Arg2, 0x0))
                            {
                                Return(Buffer(0x1)
                                {
	0x7f
                                })
                            }
                            If(LEqual(Arg2, 0x1))
                            {
                                Return(Buffer(0x4)
                                {
	0x31, 0x2e, 0x30, 0x00
                                })
                            }
                            If(LEqual(Arg2, 0x2))
                            {
                                If(TPRS)
                                {
                                    Store(0x0, PPRC)
                                    If(LFLS())
                                    {
                                        Store(0x2, PPRC)
                                    }
                                    Else
                                    {
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x0))
                                        {
                                            Store(0x0, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x1))
                                        {
                                            Store(0x1, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x2))
                                        {
                                            Store(0x2, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x3))
                                        {
                                            Store(0x3, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x4))
                                        {
                                            Store(0x4, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x5))
                                        {
                                            Store(0x5, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x6))
                                        {
                                            Store(0x6, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x7))
                                        {
                                            Store(0x7, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x8))
                                        {
                                            Store(0x8, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x9))
                                        {
                                            Store(0x9, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0xa))
                                        {
                                            Store(0xa, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0xb))
                                        {
                                            Store(0xb, PPRQ)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0xc))
                                        {
                                            Store(0xc, PPRQ)
                                            Return(0x1)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0xd))
                                        {
                                            Store(0xd, PPRQ)
                                            Return(0x1)
                                        }
                                        If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0xe))
                                        {
                                            Store(0xe, PPRQ)
                                        }
                                        If(LNot(LLess(DerefOf(Index(Arg3, 0x0, )), 0xf)))
                                        {
                                            Return(0x1)
                                        }
                                        SFLS()
                                        Return(PPRC)
                                    }
                                }
                                Return(0x1)
                            }
                            If(LEqual(Arg2, 0x3))
                            {
                                Name(TMP1, Package(0x2)
                                {
                                    0x0,
                                    0xffffffff
                                })
                                If(LFLS())
                                {
                                    Store(0x1, Index(TMP1, 0x0, ))
                                    Return(TMP1)
                                }
                                Store(PPRQ, Index(TMP1, 0x1, ))
                                Return(TMP1)
                            }
                            If(LEqual(Arg2, 0x4))
                            {
                                Return(0x1)
                            }
                            If(LEqual(Arg2, 0x5))
                            {
                                Name(TMP2, Package(0x3)
                                {
                                    0x0,
                                    0xffffffff,
                                    0xffffffff
                                })
                                If(LFLS())
                                {
                                    Store(0x1, Index(TMP2, 0x0, ))
                                    Return(TMP2)
                                }
                                Store(PPLO, Index(TMP2, 0x1, ))
                                If(LGreater(PPLO, 0xe))
                                {
                                    Store(0xfffffff1, Index(TMP2, 0x2, ))
                                    Return(TMP2)
                                }
                                If(LEqual(PPRQ, 0x1f))
                                {
                                    Store(0xfffffff1, Index(TMP2, 0x2, ))
                                    Return(TMP2)
                                }
                                If(PPOR)
                                {
                                    Store(0xfffffff0, Index(TMP2, 0x2, ))
                                    Return(TMP2)
                                }
                                Store(0x0, Index(TMP2, 0x2, ))
                                Return(TMP2)
                            }
                            If(LEqual(Arg2, 0x6))
                            {
                                CreateByteField(Arg3, 0x4, LAN0)
                                CreateByteField(Arg3, 0x5, LAN1)
                                If(LOr(LEqual(LAN0, 0x65), LEqual(LAN0, 0x45)))
                                {
                                    If(LOr(LEqual(LAN1, 0x6e), LEqual(LAN1, 0x4e)))
                                    {
                                        Return(0x0)
                                    }
                                }
                                Return(0x1)
                            }
                            Return(0x1)
                        }
                        If(LEqual(UCMP(Arg0, Buffer(0x10)
                        {
	0xed, 0x54, 0x60, 0x37, 0x13, 0xcc, 0x75, 0x46, 0x90, 0x1c, 0x47, 0x56,
	0xd7, 0xf2, 0xd4, 0x5d
                        }), 0x1))
                        {
                            If(LEqual(Arg2, 0x0))
                            {
                                Return(Buffer(0x1)
                                {
	0x01
                                })
                            }
                            If(LEqual(Arg2, 0x1))
                            {
                                If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x0))
                                {
                                    If(LFLS())
                                    {
                                        Return(0x2)
                                    }
                                    Store(0x0, MOR_)
                                    SFLS()
                                    Return(0x0)
                                }
                                If(LEqual(DerefOf(Index(Arg3, 0x0, )), 0x1))
                                {
                                    If(LFLS())
                                    {
                                        Return(0x2)
                                    }
                                    Store(0x1, MOR_)
                                    SFLS()
                                    Return(0x0)
                                }
                            }
                            Return(0x1)
                        }
                        Return(Buffer(0x1)
                        {
	0x00
                        })
                    }
                    Method(LFLS, 0x0, NotSerialized)
                    {
                        Name(TMPB, Buffer(0x2)
                        {
	0x00, 0x00
                        })
                        CreateByteField(TMPB, 0x0, LPCT)
                        CreateByteField(TMPB, 0x1, SSUM)
                        TPHY(0x0)
                        Store(PH02, LPCT)
                        If(LPCT)
                        {
                            Store(0x0, SSUM)
                            Add(SSUM, PH01, SSUM)
                            Add(SSUM, PH02, SSUM)
                            Add(SSUM, PH03, SSUM)
                            Add(SSUM, PPRQ, SSUM)
                            Add(SSUM, PPLO, SSUM)
                            Add(SSUM, PPRP, SSUM)
                            Add(SSUM, PPOR, SSUM)
                            Add(SSUM, TPRS, SSUM)
                            Add(SSUM, TPMV, SSUM)
                            Add(SSUM, MOR_, SSUM)
                            Add(SSUM, RSV0, SSUM)
                            If(SSUM)
                            {
                            }
                            Else
                            {
                                Return(0x0)
                            }
                            Return(0x2)
                        }
                    }
                    Method(SFLS, 0x0, NotSerialized)
                    {
                        Name(TMPB, Buffer(0x2)
                        {
	0x00, 0x00
                        })
                        CreateByteField(TMPB, 0x0, LPCT)
                        CreateByteField(TMPB, 0x1, SSUM)
                        Store(PH02, LPCT)
                        If(LPCT)
                        {
                            Store(0x0, SSUM)
                            Add(SSUM, PH01, SSUM)
                            Add(SSUM, PH02, SSUM)
                            Add(SSUM, PH03, SSUM)
                            Add(SSUM, PPRQ, SSUM)
                            Add(SSUM, PPLO, SSUM)
                            Add(SSUM, PPRP, SSUM)
                            Add(SSUM, PPOR, SSUM)
                            Add(SSUM, TPRS, SSUM)
                            Add(SSUM, TPMV, SSUM)
                            Add(SSUM, MOR_, SSUM)
                            Add(SSUM, RSV0, SSUM)
                            Subtract(0x0, SSUM, PH03)
                            TPHY(0x1)
                            Return(0x0)
                        }
                        Else
                        {
                            Return(0x2)
                        }
                    }
                }
                Device(EC__)
                {
                    Name(_HID, 0x90cd041)
                    Name(_UID, 0x0)
                    Name(_GPE, 0x11)
                    Method(_REG, 0x2, NotSerialized)
                    {
                        If(LEqual(Arg0, 0x3))
                        {
                            Store(Arg1, \H8DR)
                        }
                    }
                    OperationRegion(ECOR, EmbeddedControl, 0x0, 0x100)
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        HDBM, 1,
                        , 1,
                        , 1,
                        HFNE, 1,
                        , 1,
                        , 1,
                        HLDM, 1,
                        , 1,
                        , 1,
                        BTCM, 1,
                        , 1,
                        , 1,
                        , 1,
                        HBPR, 1,
                        BTPC, 1,
                        , 1,
                        SLIS, 1,
                        HPWC, 2,
                        , 2,
                        SNLK, 1,
                        , 2,
                        , 1,
                        HETE, 1,
                        , 3,
                        HAUM, 2,
                        , 9,
                        HSPA, 1,
                        , 7,
                        HSUN, 8,
                        HSRP, 8,
                        , 32,
                        HLCL, 8,
                        , 8,
                        HFNS, 2,
                        , 6,
                        , 6,
                        NULS, 1,
                        HTAB, 1,
                        HAM0, 8,
                        HAM1, 8,
                        HAM2, 8,
                        HAM3, 8,
                        HAM4, 8,
                        HAM5, 8,
                        HAM6, 8,
                        HAM7, 8,
                        HAM8, 8,
                        HAM9, 8,
                        HAMA, 8,
                        HAMB, 8,
                        HAMC, 8,
                        HAMD, 8,
                        HAME, 8,
                        HAMF, 8,
                        , 24,
                        HANT, 8,
                        , 16,
                        , 1,
                        , 1,
                        HANA, 2,
                        , 1,
                        , 1,
                        , 26,
                        HATR, 8,
                        HT0H, 8,
                        HT0L, 8,
                        HT1H, 8,
                        HT1L, 8,
                        HFSP, 8,
                        , 6,
                        HMUT, 1,
                        , 1,
                        , 2,
                        HUWB, 1,
                        , 5,
                        HWPM, 1,
                        HWLB, 1,
                        HWLO, 1,
                        HWDK, 1,
                        HWFN, 1,
                        HWBT, 1,
                        HWRI, 1,
                        HWBU, 1,
                        HWPN, 2,
                        , 6,
                        , 7,
                        HPLO, 1,
                        , 8,
                        , 16,
                        HB0S, 7,
                        HB0A, 1,
                        HB1S, 7,
                        HB1A, 1,
                        HCMU, 1,
                        , 2,
                        OVRQ, 1,
                        DCBD, 1,
                        DCWL, 1,
                        DCWW, 1,
                        HB1I, 1,
                        , 1,
                        KBLT, 1,
                        BTPW, 1,
                        BTDT, 1,
                        HUBS, 1,
                        BDPW, 1,
                        BDDT, 1,
                        HUBB, 1,
                        Offset(0x46),
                        , 1,
                        BTWK, 1,
                        HPLD, 1,
                        , 1,
                        HPAC, 1,
                        BTST, 1,
                        , 2,
                        HPBU, 1,
                        , 1,
                        HBID, 1,
                        , 3,
                        HBCS, 1,
                        HPNF, 1,
                        , 1,
                        GSTS, 1,
                        , 2,
                        HLBU, 1,
                        BDST, 1,
                        HCBL, 1,
                        , 1,
                        , 1,
                        PSTA, 1,
                        , 22,
                        HTMH, 8,
                        HTML, 8,
                        HWAK, 16,
                        HMPR, 8,
                        , 7,
                        HMDN, 1,
                        Offset(0x78),
                        TMP0, 8,
                        Offset(0x80),
                        , 8,
                        HIID, 8,
                        , 8,
                        HFNI, 8,
                        HSPD, 16,
                        , 16,
                        TSL0, 7,
                        TSR0, 1,
                        TSL1, 7,
                        TSR1, 1,
                        TSL2, 7,
                        TSR2, 1,
                        TSL3, 7,
                        TSR3, 1,
                        , 8,
                        HDAA, 3,
                        HDAB, 3,
                        HDAC, 2,
                        Offset(0xb0),
                        HDEN, 32,
                        HDEP, 32,
                        HDEM, 8,
                        HDES, 8,
                        Offset(0xc8),
                        ATMX, 8,
                        HWAT, 8,
                        , 16,
                        PWMH, 8,
                        PWML, 8,
                        Offset(0xed),
                        , 4,
                        HDDD, 1
                    }
                    Method(_INI, 0x0, NotSerialized)
                    {
                        If(\H8DR)
                        {
                            Store(0x0, HSPA)
                        }
                        Else
                        {
                            \MBEC(0x5, 0xfe, 0x0)
                        }
                        BINI()
                        \_SB_.PCI0.LPC_.EC__.HKEY.WGIN()
                    }
                    Name(_CRS, Buffer(0x12)
                    {
	0x47, 0x01, 0x62, 0x00, 0x62, 0x00, 0x01, 0x01, 0x47, 0x01, 0x66, 0x00,
	0x66, 0x00, 0x01, 0x01, 0x79, 0x00
                    })
                    Method(LED_, 0x2, NotSerialized)
                    {
                        Or(Arg0, Arg1, Local0)
                        If(\H8DR)
                        {
                            Store(Local0, HLCL)
                        }
                        Else
                        {
                            \WBEC(0xc, Local0)
                        }
                    }
                    Name(BAON, 0x0)
                    Name(WBON, 0x0)
                    Method(BEEP, 0x1, NotSerialized)
                    {
                        If(LEqual(Arg0, 0x5))
                        {
                            Store(0x0, WBON)
                        }
                        Store(WBON, Local2)
                        If(BAON)
                        {
                            If(LEqual(Arg0, 0x0))
                            {
                                Store(0x0, BAON)
                                If(WBON)
                                {
                                    Store(0x3, Local0)
                                    Store(0x8, Local1)
                                }
                                Else
                                {
                                    Store(0x0, Local0)
                                    Store(0x0, Local1)
                                }
                            }
                            Else
                            {
                                Store(0xff, Local0)
                                Store(0xff, Local1)
                                If(LEqual(Arg0, 0x11))
                                {
                                    Store(0x0, WBON)
                                }
                                If(LEqual(Arg0, 0x10))
                                {
                                    Store(0x1, WBON)
                                }
                            }
                        }
                        Else
                        {
                            Store(Arg0, Local0)
                            Store(0xff, Local1)
                            If(LEqual(Arg0, 0xf))
                            {
                                Store(Arg0, Local0)
                                Store(0x8, Local1)
                                Store(0x1, BAON)
                            }
                            If(LEqual(Arg0, 0x11))
                            {
                                Store(0x0, Local0)
                                Store(0x0, Local1)
                                Store(0x0, WBON)
                            }
                            If(LEqual(Arg0, 0x10))
                            {
                                Store(0x3, Local0)
                                Store(0x8, Local1)
                                Store(0x1, WBON)
                            }
                        }
                        If(LEqual(Arg0, 0x3))
                        {
                            Store(0x0, WBON)
                            If(Local2)
                            {
                                Store(0x7, Local0)
                                If(LOr(LEqual(\SPS_, 0x3), LEqual(\SPS_, 0x4)))
                                {
                                    Store(0x0, Local2)
                                    Store(0xff, Local0)
                                    Store(0xff, Local1)
                                }
                            }
                        }
                        If(LEqual(Arg0, 0x7))
                        {
                            If(Local2)
                            {
                                Store(0x0, Local2)
                                Store(0xff, Local0)
                                Store(0xff, Local1)
                            }
                        }
                        If(\H8DR)
                        {
                            If(LAnd(Local2, LNot(WBON)))
                            {
                                Store(0x0, HSRP)
                                Store(0x0, HSUN)
                                Sleep(0x64)
                            }
                            If(LNot(LEqual(Local1, 0xff)))
                            {
                                Store(Local1, HSRP)
                            }
                            If(LNot(LEqual(Local0, 0xff)))
                            {
                                Store(Local0, HSUN)
                            }
                        }
                        Else
                        {
                            If(LAnd(Local2, LNot(WBON)))
                            {
                                \WBEC(0x7, 0x0)
                                \WBEC(0x6, 0x0)
                                Sleep(0x64)
                            }
                            If(LNot(LEqual(Local1, 0xff)))
                            {
                                \WBEC(0x7, Local1)
                            }
                            If(LNot(LEqual(Local0, 0xff)))
                            {
                                \WBEC(0x6, Local0)
                            }
                        }
                        If(LEqual(Arg0, 0x3))
                        {
                        }
                        If(LEqual(Arg0, 0x7))
                        {
                            Sleep(0x1f4)
                        }
                    }
                    Method(EVNT, 0x1, NotSerialized)
                    {
                        If(\H8DR)
                        {
                            If(Arg0)
                            {
                                Or(HAM7, 0x1, HAM7)
                                Or(HAM5, 0x4, HAM5)
                            }
                            Else
                            {
                                And(HAM7, 0xfe, HAM7)
                                And(HAM5, 0xfb, HAM5)
                            }
                        }
                        Else
                        {
                            If(Arg0)
                            {
                                \MBEC(0x17, 0xff, 0x1)
                                \MBEC(0x15, 0xff, 0x4)
                            }
                            Else
                            {
                                \MBEC(0x17, 0xfe, 0x0)
                                \MBEC(0x15, 0xfb, 0x0)
                            }
                        }
                    }
                    PowerResource(PUBS, 0x3, 0x0)
                    {
                        Method(_STA, 0x0, NotSerialized)
                        {
                            If(\H8DR)
                            {
                                Store(HUBS, Local0)
                            }
                            Else
                            {
                                And(\RBEC(0x3b), 0x10, Local0)
                            }
                            If(Local0)
                            {
                                Return(0x1)
                            }
                            Else
                            {
                                Return(0x0)
                            }
                        }
                        Method(_ON_, 0x0, NotSerialized)
                        {
                            If(\H8DR)
                            {
                                Store(0x1, HUBS)
                            }
                            Else
                            {
                                \MBEC(0x3b, 0xff, 0x10)
                            }
                        }
                        Method(_OFF, 0x0, NotSerialized)
                        {
                            If(\H8DR)
                            {
                                Store(0x0, HUBS)
                            }
                            Else
                            {
                                \MBEC(0x3b, 0xef, 0x0)
                            }
                        }
                    }
                    Method(CHKS, 0x0, NotSerialized)
                    {
                        Store(0x3e8, Local0)
                        While(HMPR)
                        {
                            Sleep(0x1)
                            Decrement(Local0)
                            If(LNot(Local0))
                            {
                                Return(0x8080)
                            }
                        }
                        If(HMDN)
                        {
                            Return(Zero)
                        }
                        Return(0x8081)
                    }
                    Method(LPMD, 0x0, NotSerialized)
                    {
                        Store(0x0, Local0)
                        Store(0x0, Local1)
                        Store(0x0, Local2)
                        If(\H8DR)
                        {
                            If(HPAC)
                            {
                                If(HPLO)
                                {
                                    Store(\LPST, Local0)
                                }
                                Else
                                {
                                    If(LLess(HWAT, 0x5a))
                                    {
                                        If(HB0A)
                                        {
                                            If(LOr(And(HB0S, 0x10, ), LLess(And(HB0S, 0xf, ), 0x2)))
                                            {
                                                Store(0x1, Local1)
                                            }
                                        }
                                        Else
                                        {
                                            Store(0x1, Local1)
                                        }
                                        If(HB1A)
                                        {
                                            If(LOr(And(HB1S, 0x10, ), LLess(And(HB1S, 0xf, ), 0x2)))
                                            {
                                                Store(0x1, Local2)
                                            }
                                        }
                                        Else
                                        {
                                            Store(0x1, Local2)
                                        }
                                        If(LAnd(Local1, Local2))
                                        {
                                            Store(\LPST, Local0)
                                        }
                                    }
                                }
                            }
                        }
                        Else
                        {
                            If(And(\RBEC(0x46), 0x10, ))
                            {
                                If(And(\RBEC(0x34), 0x80, ))
                                {
                                    Store(\LPST, Local0)
                                }
                                Else
                                {
                                    If(LLess(\RBEC(0xc9), 0x5a))
                                    {
                                        Store(\RBEC(0x38), Local3)
                                        If(And(Local3, 0x80, ))
                                        {
                                            If(LOr(And(Local3, 0x10, ), LLess(And(Local3, 0xf, ), 0x2)))
                                            {
                                                Store(0x1, Local1)
                                            }
                                        }
                                        Else
                                        {
                                            Store(0x1, Local2)
                                        }
                                        Store(\RBEC(0x39), Local3)
                                        If(And(Local3, 0x80, ))
                                        {
                                            If(LOr(And(Local3, 0x10, ), LLess(And(Local3, 0xf, ), 0x2)))
                                            {
                                                Store(0x1, Local1)
                                            }
                                        }
                                        Else
                                        {
                                            Store(0x1, Local2)
                                        }
                                        If(LAnd(Local1, Local2))
                                        {
                                            Store(\LPST, Local0)
                                        }
                                    }
                                }
                            }
                        }
                        Return(Local0)
                    }
                    Method(CLPM, 0x0, NotSerialized)
                    {
                        If(And(\PPMF, 0x1, ))
                        {
                            If(\OSPX)
                            {
                                \PNTF(0x80)
                            }
                            Else
                            {
                                Store(LPMD(), Local0)
                                If(Local0)
                                {
                                    \STEP(0x4)
                                }
                                Else
                                {
                                    \STEP(0x5)
                                }
                            }
                        }
                    }
                    Mutex(MCPU, 0x7)
                    Method(_Q10, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x1))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1001)
                        }
                    }
                    Method(_Q11, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x2))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1002)
                        }
                        Else
                        {
                            Noop
                        }
                    }
                    Method(_Q12, 0x0, NotSerialized)
                    {
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1003)
                    }
                    Method(_Q13, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.DHKC)
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1004)
                        }
                        Else
                        {
                            Notify(\_SB_.SLPB, 0x80)
                        }
                    }
                    Method(_Q64, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x10))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1005)
                        }
                    }
                    Method(_Q65, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x20))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1006)
                        }
                    }
                    Method(_Q16, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x40))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1007)
                        }
                        Else
                        {
                            If(VIGD)
                            {
                                \_SB_.PCI0.VID_.VSWT()
                            }
                            Else
                            {
                                \_SB_.PCI0.PEG_.VID_.VSWT()
                            }
                        }
                    }
                    Method(_Q17, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x80))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1008)
                        }
                        Else
                        {
                            If(LNot(\WNTF))
                            {
                                VEXP()
                            }
                        }
                    }
                    Method(_Q18, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x100))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1009)
                        }
                        Noop
                    }
                    Method(_Q66, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x200))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x100a)
                        }
                    }
                    Method(_Q1A, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x400))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x100b)
                        }
                    }
                    Method(_Q1B, 0x0, NotSerialized)
                    {
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x100c)
                    }
                    Method(_Q62, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x1000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x100d)
                        }
                    }
                    Method(_Q60, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x2000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x100e)
                        }
                    }
                    Method(_Q61, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x4000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x100f)
                        }
                    }
                    Method(_Q1F, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x20000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1012)
                        }
                        \UCMS(0xe)
                    }
                    Method(_Q67, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x40000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1013)
                        }
                    }
                    Method(_Q1C, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x1000000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1019)
                        }
                    }
                    Method(_Q1D, 0x0, NotSerialized)
                    {
                        If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x2000000))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x101a)
                        }
                    }
                    Method(_Q26, 0x0, NotSerialized)
                    {
                        If(VIGD)
                        {
                            If(\WVIS)
                            {
                                \VBTD()
                            }
                            \_SB_.PCI0.LPC_.EC__.BRNS()
                        }
                        Else
                        {
                            \UCMS(0x12)
                        }
                        Sleep(0x1f4)
                        Notify(AC__, 0x80)
                        Notify(\_TZ_.THM0, 0x80)
                        If(\WXPF)
                        {
                            Acquire(MCPU, 0xffff)
                        }
                        Store(0x1, PWRS)
                        If(And(\PPMF, 0x1, ))
                        {
                            If(\OSPX)
                            {
                                \PNTF(0x80)
                            }
                            Else
                            {
                                \STEP(0x0)
                            }
                        }
                        If(\WXPF)
                        {
                            Sleep(0x64)
                        }
                        If(\OSC4)
                        {
                            \PNTF(0x81)
                        }
                        If(\WXPF)
                        {
                            Release(MCPU)
                        }
                        ATMC()
                    }
                    Method(_Q27, 0x0, NotSerialized)
                    {
                        If(VIGD)
                        {
                            If(\WVIS)
                            {
                                \VBTD()
                            }
                            \_SB_.PCI0.LPC_.EC__.BRNS()
                        }
                        Else
                        {
                            \UCMS(0x12)
                        }
                        Sleep(0x1f4)
                        Notify(AC__, 0x80)
                        Notify(\_TZ_.THM0, 0x80)
                        If(\WXPF)
                        {
                            Acquire(MCPU, 0xffff)
                        }
                        Store(0x0, PWRS)
                        If(And(\PPMF, 0x1, ))
                        {
                            If(\OSPX)
                            {
                                \PNTF(0x80)
                            }
                            Else
                            {
                                \STEP(0x1)
                            }
                        }
                        If(\WXPF)
                        {
                            Sleep(0x64)
                        }
                        If(\OSC4)
                        {
                            \PNTF(0x81)
                        }
                        If(\WXPF)
                        {
                            Release(MCPU)
                        }
                        ATMC()
                    }
                    Method(_Q2A, 0x0, NotSerialized)
                    {
                        If(VIGD)
                        {
                            \_SB_.PCI0.VID_.GLIS(0x1)
                        }
                        If(VIGD)
                        {
                            \_SB_.PCI0.VID_.VLOC(0x1)
                        }
                        Else
                        {
                            \_SB_.PCI0.PEG_.VID_.VLOC(0x1)
                        }
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x5002)
                        Notify(\_SB_.LID_, 0x80)
                    }
                    Method(_Q2B, 0x0, NotSerialized)
                    {
                        If(VIGD)
                        {
                            \_SB_.PCI0.VID_.GLIS(0x0)
                        }
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x5001)
                        \UCMS(0xd)
                        Notify(\_SB_.LID_, 0x80)
                    }
                    Method(_Q5E, 0x0, NotSerialized)
                    {
                        If(\H8DR)
                        {
                            If(HPLD)
                            {
                                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x5009)
                            }
                        }
                    }
                    Method(_Q5F, 0x0, NotSerialized)
                    {
                        If(\H8DR)
                        {
                            If(HPLD)
                            {
                                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x500a)
                            }
                        }
                    }
                    Method(_Q3D, 0x0, NotSerialized)
                    {
                    }
                    Method(_Q48, 0x0, NotSerialized)
                    {
                        If(And(\PPMF, 0x1, ))
                        {
                            If(\OSPX)
                            {
                                \PNTF(0x80)
                            }
                            Else
                            {
                                \STEP(0x4)
                            }
                        }
                    }
                    Method(_Q49, 0x0, NotSerialized)
                    {
                        If(And(\PPMF, 0x1, ))
                        {
                            If(\OSPX)
                            {
                                \PNTF(0x80)
                            }
                            Else
                            {
                                \STEP(0x5)
                            }
                        }
                    }
                    Method(_Q7F, 0x0, NotSerialized)
                    {
                        Fatal(0x1, 0x80010000, 0x3f4)
                    }
                    Method(_Q4E, 0x0, NotSerialized)
                    {
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x6011)
                    }
                    Method(_Q4F, 0x0, NotSerialized)
                    {
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x6012)
                    }
                    Method(_Q46, 0x0, NotSerialized)
                    {
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x6012)
                    }
                    Method(_Q22, 0x0, NotSerialized)
                    {
                        CLPM()
                        If(HB0A)
                        {
                            Notify(BAT0, 0x80)
                        }
                        If(HB1A)
                        {
                            Notify(BAT1, 0x80)
                        }
                    }
                    Method(_Q4A, 0x0, NotSerialized)
                    {
                        CLPM()
                        Notify(BAT0, 0x81)
                    }
                    Method(_Q4B, 0x0, NotSerialized)
                    {
                        Notify(BAT0, 0x80)
                    }
                    Method(_Q4C, 0x0, NotSerialized)
                    {
                        CLPM()
                        _Q38()
                    }
                    Method(_Q4D, 0x0, NotSerialized)
                    {
                        If(And(^BAT1.B1ST, ^BAT1.XB1S, ))
                        {
                            Notify(BAT1, 0x80)
                        }
                    }
                    Method(_Q24, 0x0, NotSerialized)
                    {
                        CLPM()
                        Notify(BAT0, 0x80)
                    }
                    Method(_Q25, 0x0, NotSerialized)
                    {
                        If(And(^BAT1.B1ST, ^BAT1.XB1S, ))
                        {
                            CLPM()
                            Notify(BAT1, 0x80)
                        }
                    }
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        Offset(0xa0),
                        SBRC, 16,
                        SBFC, 16,
                        SBAE, 16,
                        SBRS, 16,
                        SBAC, 16,
                        SBVO, 16,
                        SBAF, 16,
                        SBBS, 16
                    }
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        Offset(0xa0),
                        , 15,
                        SBCM, 1,
                        SBMD, 16,
                        SBCC, 16
                    }
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        Offset(0xa0),
                        SBDC, 16,
                        SBDV, 16,
                        SBOM, 16,
                        SBSI, 16,
                        SBDT, 16,
                        SBSN, 16
                    }
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        Offset(0xa0),
                        SBCH, 32
                    }
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        Offset(0xa0),
                        SBMN, 128
                    }
                    Field(ECOR, ByteAcc, NoLock, Preserve)
                    {
                        Offset(0xa0),
                        SBDN, 128
                    }
                    Mutex(BATM, 0x7)
                    Method(GBIF, 0x3, NotSerialized)
                    {
                        Acquire(BATM, 0xffff)
                        If(Arg2)
                        {
                            Or(Arg0, 0x1, HIID)
                            Store(SBCM, Local7)
                            XOr(Local7, 0x1, Index(Arg1, 0x0, ))
                            Store(Arg0, HIID)
                            If(Local7)
                            {
                                Multiply(SBFC, 0xa, Local1)
                            }
                            Else
                            {
                                Store(SBFC, Local1)
                            }
                            Store(Local1, Index(Arg1, 0x2, ))
                            Or(Arg0, 0x2, HIID)
                            If(Local7)
                            {
                                Multiply(SBDC, 0xa, Local0)
                            }
                            Else
                            {
                                Store(SBDC, Local0)
                            }
                            Store(Local0, Index(Arg1, 0x1, ))
                            Divide(Local1, 0x14, Local2, Index(Arg1, 0x5, ))
                            If(Local7)
                            {
                                Store(0xc8, Index(Arg1, 0x6, ))
                            }
                            Else
                            {
                                If(SBDV)
                                {
                                    Divide(0x30d40, SBDV, Local2, Index(Arg1, 0x6, ))
                                }
                                Else
                                {
                                    Store(0x0, Index(Arg1, 0x6, ))
                                }
                            }
                            Store(SBDV, Index(Arg1, 0x4, ))
                            Store(SBSN, Local0)
                            Name(SERN, Buffer(0x6)
                            {
	0x20, 0x20, 0x20, 0x20, 0x20, 0x00
                            })
                            Store(0x4, Local2)
                            While(Local0)
                            {
                                Divide(Local0, 0xa, Local1, Local0)
                                Add(Local1, 0x30, Index(SERN, Local2, ))
                                Decrement(Local2)
                            }
                            Store(SERN, Index(Arg1, 0xa, ))
                            Or(Arg0, 0x6, HIID)
                            Store(SBDN, Index(Arg1, 0x9, ))
                            Or(Arg0, 0x4, HIID)
                            Name(BTYP, Buffer(0x5)
                            {
	0x00, 0x00, 0x00, 0x00, 0x00
                            })
                            Store(SBCH, BTYP)
                            Store(BTYP, Index(Arg1, 0xb, ))
                            Or(Arg0, 0x5, HIID)
                            Store(SBMN, Index(Arg1, 0xc, ))
                        }
                        Else
                        {
                            Store(0xffffffff, Index(Arg1, 0x1, ))
                            Store(0x0, Index(Arg1, 0x5, ))
                            Store(0x0, Index(Arg1, 0x6, ))
                            Store(0xffffffff, Index(Arg1, 0x2, ))
                        }
                        Release(BATM)
                        Return(Arg1)
                    }
                    Method(GBST, 0x4, NotSerialized)
                    {
                        Acquire(BATM, 0xffff)
                        If(And(Arg1, 0x20, ))
                        {
                            Store(0x2, Local0)
                        }
                        Else
                        {
                            If(And(Arg1, 0x40, ))
                            {
                                Store(0x1, Local0)
                            }
                            Else
                            {
                                Store(0x0, Local0)
                            }
                        }
                        If(And(Arg1, 0xf, ))
                        {
                        }
                        Else
                        {
                            Or(Local0, 0x4, Local0)
                        }
                        If(LEqual(And(Arg1, 0xf, ), 0xf))
                        {
                            Store(0x4, Local0)
                            Store(0x0, Local1)
                            Store(0x0, Local2)
                            Store(0x0, Local3)
                        }
                        Else
                        {
                            Store(Arg0, HIID)
                            Store(SBVO, Local3)
                            If(Arg2)
                            {
                                Multiply(SBRC, 0xa, Local2)
                            }
                            Else
                            {
                                Store(SBRC, Local2)
                            }
                            Store(SBAC, Local1)
                            If(LNot(LLess(Local1, 0x8000)))
                            {
                                If(And(Local0, 0x1, ))
                                {
                                    Subtract(0x10000, Local1, Local1)
                                }
                                Else
                                {
                                    Store(0x0, Local1)
                                }
                            }
                            Else
                            {
                                If(LNot(And(Local0, 0x2, )))
                                {
                                    Store(0x0, Local1)
                                }
                            }
                            If(Arg2)
                            {
                                Multiply(Local3, Local1, Local1)
                                Divide(Local1, 0x3e8, Local7, Local1)
                            }
                        }
                        Store(Local0, Index(Arg3, 0x0, ))
                        Store(Local1, Index(Arg3, 0x1, ))
                        Store(Local2, Index(Arg3, 0x2, ))
                        Store(Local3, Index(Arg3, 0x3, ))
                        Release(BATM)
                        Return(Arg3)
                    }
                    Device(BAT0)
                    {
                        Name(_HID, 0xa0cd041)
                        Name(_UID, 0x0)
                        Name(_PCL, Package(0x1)
                        {
                            \_SB_
                        })
                        Name(B0ST, 0x0)
                        Name(BT0I, Package(0xd)
                        {
                            0x0,
                            0xffffffff,
                            0xffffffff,
                            0x1,
                            0x2a30,
                            0x0,
                            0x0,
                            0x1,
                            0x1,
                            "",
                            "",
                            "",
                            ""
                        })
                        Name(BT0P, Package(0x4)
                        {
                        })
                        Method(_STA, 0x0, NotSerialized)
                        {
                            If(\H8DR)
                            {
                                Store(HB0A, B0ST)
                            }
                            Else
                            {
                                If(And(\RBEC(0x38), 0x80, ))
                                {
                                    Store(0x1, B0ST)
                                }
                                Else
                                {
                                    Store(0x0, B0ST)
                                }
                            }
                            If(B0ST)
                            {
                                Return(0x1f)
                            }
                            Else
                            {
                                Return(0xf)
                            }
                        }
                        Method(_BIF, 0x0, NotSerialized)
                        {
                            Store(0x0, Local7)
                            Store(0xa, Local6)
                            While(LAnd(LNot(Local7), Local6))
                            {
                                If(HB0A)
                                {
                                    If(LEqual(And(HB0S, 0xf, ), 0xf))
                                    {
                                        Sleep(0x3e8)
                                        Decrement(Local6)
                                    }
                                    Else
                                    {
                                        Store(0x1, Local7)
                                    }
                                }
                                Else
                                {
                                    Store(0x0, Local6)
                                }
                            }
                            Return(GBIF(0x0, BT0I, Local7))
                        }
                        Method(_BST, 0x0, NotSerialized)
                        {
                            XOr(DerefOf(Index(BT0I, 0x0, )), 0x1, Local0)
                            Return(GBST(0x0, HB0S, Local0, BT0P))
                        }
                        Method(_BTP, 0x1, NotSerialized)
                        {
                            And(HAM4, 0xef, HAM4)
                            If(Arg0)
                            {
                                Store(Arg0, Local1)
                                If(LNot(DerefOf(Index(BT0I, 0x0, ))))
                                {
                                    Divide(Local1, 0xa, Local0, Local1)
                                }
                                And(Local1, 0xff, HT0L)
                                And(ShiftRight(Local1, 0x8, ), 0xff, HT0H)
                                Or(HAM4, 0x10, HAM4)
                            }
                        }
                    }
                    Device(BAT1)
                    {
                        Name(_HID, 0xa0cd041)
                        Name(_UID, 0x1)
                        Name(_PCL, Package(0x1)
                        {
                            \_SB_
                        })
                        Name(B1ST, 0x0)
                        Name(XB1S, 0x1)
                        Name(BT1I, Package(0xd)
                        {
                            0x0,
                            0xffffffff,
                            0xffffffff,
                            0x1,
                            0x2a30,
                            0x0,
                            0x0,
                            0x1,
                            0x1,
                            "",
                            "",
                            "",
                            ""
                        })
                        Name(BT1P, Package(0x4)
                        {
                        })
                        Method(_STA, 0x0, NotSerialized)
                        {
                            If(\H8DR)
                            {
                                Store(HB1A, B1ST)
                            }
                            Else
                            {
                                If(And(\RBEC(0x39), 0x80, ))
                                {
                                    Store(0x1, B1ST)
                                }
                                Else
                                {
                                    Store(0x0, B1ST)
                                }
                            }
                            If(B1ST)
                            {
                                If(XB1S)
                                {
                                    Return(0x1f)
                                }
                                Else
                                {
                                    If(\WNTF)
                                    {
                                        Return(0x0)
                                    }
                                    Else
                                    {
                                        Return(0x1f)
                                    }
                                }
                            }
                            Else
                            {
                                If(\WNTF)
                                {
                                    Return(0x0)
                                }
                                Else
                                {
                                    Return(0xf)
                                }
                            }
                        }
                        Method(_BIF, 0x0, NotSerialized)
                        {
                            Store(0x0, Local7)
                            Store(0xa, Local6)
                            While(LAnd(LNot(Local7), Local6))
                            {
                                If(HB1A)
                                {
                                    If(LEqual(And(HB1S, 0xf, ), 0xf))
                                    {
                                        Sleep(0x3e8)
                                        Decrement(Local6)
                                    }
                                    Else
                                    {
                                        Store(0x1, Local7)
                                    }
                                }
                                Else
                                {
                                    Store(0x0, Local6)
                                }
                            }
                            Return(GBIF(0x10, BT1I, Local7))
                        }
                        Method(_BST, 0x0, NotSerialized)
                        {
                            XOr(DerefOf(Index(BT1I, 0x0, )), 0x1, Local0)
                            Return(GBST(0x10, HB1S, Local0, BT1P))
                        }
                        Method(_BTP, 0x1, NotSerialized)
                        {
                            And(HAM4, 0xdf, HAM4)
                            If(Arg0)
                            {
                                Store(Arg0, Local1)
                                If(LNot(DerefOf(Index(BT1I, 0x0, ))))
                                {
                                    Divide(Local1, 0xa, Local0, Local1)
                                }
                                And(Local1, 0xff, HT1L)
                                And(ShiftRight(Local1, 0x8, ), 0xff, HT1H)
                                Or(HAM4, 0x20, HAM4)
                            }
                        }
                    }
                    Device(AC__)
                    {
                        Name(_HID, "ACPI0003")
                        Name(_UID, 0x0)
                        Name(_PCL, Package(0x1)
                        {
                            \_SB_
                        })
                        Method(_PSR, 0x0, NotSerialized)
                        {
                            If(\H8DR)
                            {
                                Return(HPAC)
                            }
                            Else
                            {
                                If(And(\RBEC(0x46), 0x10, ))
                                {
                                    Return(0x1)
                                }
                                Else
                                {
                                    Return(0x0)
                                }
                            }
                        }
                        Method(_STA, 0x0, NotSerialized)
                        {
                            Return(0xf)
                        }
                    }
                    Device(HKEY)
                    {
                        Name(_HID, 0x68004d24)
                        Method(_STA, 0x0, NotSerialized)
                        {
                            Return(0xf)
                        }
                        Method(MHKV, 0x0, NotSerialized)
                        {
                            Return(0x100)
                        }
                        Name(DHKC, 0x0)
                        Name(DHKB, 0x1)
                        Mutex(XDHK, 0x7)
                        Name(DHKH, 0x0)
                        Name(DHKW, 0x0)
                        Name(DHKS, 0x0)
                        Name(DHKD, 0x0)
                        Name(DHKN, 0x80c)
                        Name(DHKT, 0x0)
                        Name(DHWW, 0x0)
                        Method(MHKA, 0x0, NotSerialized)
                        {
                            Return(0x7ffffff)
                        }
                        Method(MHKN, 0x0, NotSerialized)
                        {
                            Return(DHKN)
                        }
                        Method(MHKK, 0x1, NotSerialized)
                        {
                            If(DHKC)
                            {
                                Return(And(DHKN, Arg0, ))
                            }
                            Else
                            {
                                Return(Zero)
                            }
                        }
                        Method(MHKM, 0x2, NotSerialized)
                        {
                            Acquire(XDHK, 0xffff)
                            If(LGreater(Arg0, 0x20))
                            {
                                Noop
                            }
                            Else
                            {
                                ShiftLeft(One, Decrement(Arg0), Local0)
                                If(And(Local0, 0x7ffffff, ))
                                {
                                    If(Arg1)
                                    {
                                        Or(Local0, DHKN, DHKN)
                                    }
                                    Else
                                    {
                                        And(DHKN, XOr(Local0, 0xffffffff, ), DHKN)
                                    }
                                }
                                Else
                                {
                                    Noop
                                }
                            }
                            Release(XDHK)
                        }
                        Method(MHKS, 0x0, NotSerialized)
                        {
                            Notify(\_SB_.SLPB, 0x80)
                        }
                        Method(MHKC, 0x1, NotSerialized)
                        {
                            Store(Arg0, DHKC)
                        }
                        Method(MHKP, 0x0, NotSerialized)
                        {
                            Acquire(XDHK, 0xffff)
                            If(DHWW)
                            {
                                Store(DHWW, Local1)
                                Store(Zero, DHWW)
                            }
                            Else
                            {
                                If(DHKW)
                                {
                                    Store(DHKW, Local1)
                                    Store(Zero, DHKW)
                                }
                                Else
                                {
                                    If(DHKD)
                                    {
                                        Store(DHKD, Local1)
                                        Store(Zero, DHKD)
                                    }
                                    Else
                                    {
                                        If(DHKS)
                                        {
                                            Store(DHKS, Local1)
                                            Store(Zero, DHKS)
                                        }
                                        Else
                                        {
                                            If(DHKT)
                                            {
                                                Store(DHKT, Local1)
                                                Store(Zero, DHKT)
                                            }
                                            Else
                                            {
                                                Store(DHKH, Local1)
                                                Store(Zero, DHKH)
                                            }
                                        }
                                    }
                                }
                            }
                            Release(XDHK)
                            Return(Local1)
                        }
                        Method(MHKE, 0x1, NotSerialized)
                        {
                            Store(Arg0, DHKB)
                            Acquire(XDHK, 0xffff)
                            Store(Zero, DHKH)
                            Store(Zero, DHKW)
                            Store(Zero, DHKS)
                            Store(Zero, DHKD)
                            Store(Zero, DHKT)
                            Store(Zero, DHWW)
                            Release(XDHK)
                        }
                        Method(MHKQ, 0x1, NotSerialized)
                        {
                            If(DHKB)
                            {
                                If(DHKC)
                                {
                                    Acquire(XDHK, 0xffff)
                                    If(LLess(Arg0, 0x1000))
                                    {
                                    }
                                    Else
                                    {
                                        If(LLess(Arg0, 0x2000))
                                        {
                                            Store(Arg0, DHKH)
                                        }
                                        Else
                                        {
                                            If(LLess(Arg0, 0x3000))
                                            {
                                                Store(Arg0, DHKW)
                                            }
                                            Else
                                            {
                                                If(LLess(Arg0, 0x4000))
                                                {
                                                    Store(Arg0, DHKS)
                                                }
                                                Else
                                                {
                                                    If(LLess(Arg0, 0x5000))
                                                    {
                                                        Store(Arg0, DHKD)
                                                    }
                                                    Else
                                                    {
                                                        If(LLess(Arg0, 0x6000))
                                                        {
                                                            Store(Arg0, DHKH)
                                                        }
                                                        Else
                                                        {
                                                            If(LLess(Arg0, 0x7000))
                                                            {
                                                                Store(Arg0, DHKT)
                                                            }
                                                            Else
                                                            {
                                                                If(LLess(Arg0, 0x8000))
                                                                {
                                                                    Store(Arg0, DHWW)
                                                                }
                                                                Else
                                                                {
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    Release(XDHK)
                                    Notify(HKEY, 0x80)
                                }
                                Else
                                {
                                    If(LEqual(Arg0, 0x1004))
                                    {
                                        Notify(\_SB_.SLPB, 0x80)
                                    }
                                }
                            }
                        }
                        Method(MHKG, 0x0, NotSerialized)
                        {
                            Store(Zero, Local0)
                            ShiftLeft(\_SB_.PCI0.LPC_.EC__.HTAB, 0x3, Local0)
                            Return(Local0)
                        }
                        Method(MHKB, 0x1, NotSerialized)
                        {
                            If(LEqual(Arg0, 0x0))
                            {
                                \_SB_.PCI0.LPC_.EC__.BEEP(0x11)
                                Store(0x0, \LIDB)
                            }
                            Else
                            {
                                If(LEqual(Arg0, 0x1))
                                {
                                    \_SB_.PCI0.LPC_.EC__.BEEP(0x10)
                                    Store(0x1, \LIDB)
                                }
                                Else
                                {
                                }
                            }
                        }
                        Method(MHKD, 0x0, NotSerialized)
                        {
                            If(VIGD)
                            {
                                \_SB_.PCI0.VID_.VLOC(0x0)
                            }
                            Else
                            {
                                \_SB_.PCI0.PEG_.VID_.VLOC(0x0)
                            }
                        }
                        Method(MHQC, 0x1, NotSerialized)
                        {
                            If(\WNTF)
                            {
                                If(LEqual(Arg0, 0x0))
                                {
                                    Return(\CWAC)
                                }
                                Else
                                {
                                    If(LEqual(Arg0, 0x1))
                                    {
                                        Return(\CWAP)
                                    }
                                    Else
                                    {
                                        If(LEqual(Arg0, 0x2))
                                        {
                                            Return(\CWAT)
                                        }
                                        Else
                                        {
                                            Noop
                                        }
                                    }
                                }
                            }
                            Else
                            {
                                Noop
                            }
                            Return(0x0)
                        }
                        Method(MHGC, 0x0, NotSerialized)
                        {
                            If(\WNTF)
                            {
                                Acquire(XDHK, 0xffff)
                                If(CKC4(0x0))
                                {
                                    Store(0x3, Local0)
                                }
                                Else
                                {
                                    Store(0x4, Local0)
                                }
                                Release(XDHK)
                                Return(Local0)
                            }
                            Else
                            {
                                Noop
                            }
                            Return(0x0)
                        }
                        Method(MHSC, 0x1, NotSerialized)
                        {
                            If(LAnd(\CWAC, \WNTF))
                            {
                                Acquire(XDHK, 0xffff)
                                If(\OSC4)
                                {
                                    If(LEqual(Arg0, 0x3))
                                    {
                                        If(LNot(\CWAS))
                                        {
                                            \PNTF(0x81)
                                            Store(0x1, \CWAS)
                                        }
                                    }
                                    Else
                                    {
                                        If(LEqual(Arg0, 0x4))
                                        {
                                            If(\CWAS)
                                            {
                                                \PNTF(0x81)
                                                Store(0x0, \CWAS)
                                            }
                                        }
                                        Else
                                        {
                                            Noop
                                        }
                                    }
                                }
                                Release(XDHK)
                            }
                            Else
                            {
                                Noop
                            }
                        }
                        Method(CKC4, 0x1, NotSerialized)
                        {
                            Store(0x0, Local0)
                            If(\C4WR)
                            {
                                If(LNot(\C4AC))
                                {
                                    Or(Local0, 0x1, Local0)
                                }
                            }
                            If(\C4NA)
                            {
                                Or(Local0, 0x2, Local0)
                            }
                            If(LAnd(\CWAC, \CWAS))
                            {
                                Or(Local0, 0x4, Local0)
                            }
                            If(LAnd(\CWUE, \CWUS))
                            {
                                Or(Local0, 0x8, Local0)
                            }
                            And(Local0, Not(Arg0, ), Local0)
                            Return(Local0)
                        }
                        Method(MHQE, 0x0, NotSerialized)
                        {
                            Return(\C4WR)
                        }
                        Method(MHGE, 0x0, NotSerialized)
                        {
                            If(LAnd(\C4WR, \C4AC))
                            {
                                Return(0x4)
                            }
                            Return(0x3)
                        }
                        Method(MHSE, 0x1, NotSerialized)
                        {
                            If(\C4WR)
                            {
                                Store(\C4AC, Local0)
                                If(LEqual(Arg0, 0x3))
                                {
                                    Store(0x0, \C4AC)
                                    If(XOr(Local0, \C4AC, ))
                                    {
                                        If(\OSC4)
                                        {
                                            \PNTF(0x81)
                                        }
                                    }
                                }
                                Else
                                {
                                    If(LEqual(Arg0, 0x4))
                                    {
                                        Store(0x1, \C4AC)
                                        If(XOr(Local0, \C4AC, ))
                                        {
                                            If(\OSC4)
                                            {
                                                \PNTF(0x81)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Method(UAWO, 0x1, NotSerialized)
                        {
                            Return(\UAWS(Arg0))
                        }
                    }
                    Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
                    {
                        Name(WGBF, Buffer(0x1)
                        {
	0x00
                        })
                        CreateField(WGBF, 0x0, 0x2, HPWC)
                        CreateField(WGBF, 0x2, 0x2, HWPN)
                        Method(MHQP, 0x0, NotSerialized)
                        {
                            Return(XOr(0x1, \_SB_.PCI0.LPC_.EC__.PSTA, ))
                        }
                        Method(MHGP, 0x0, NotSerialized)
                        {
                            Return(And(\_SB_.PCI0.LPC_.EC__.HWPN, 0x3, ))
                        }
                        Method(MHSP, 0x1, NotSerialized)
                        {
                            And(Arg0, 0x3, \_SB_.PCI0.LPC_.EC__.HWPN)
                        }
                        Method(MHGW, 0x0, NotSerialized)
                        {
                            Return(\_SB_.PCI0.LPC_.EC__.HPWC)
                        }
                        Method(MHSW, 0x1, NotSerialized)
                        {
                            And(Arg0, 0x3, \_SB_.PCI0.LPC_.EC__.HPWC)
                        }
                        Method(SVWG, 0x0, NotSerialized)
                        {
                            Store(\_SB_.PCI0.LPC_.EC__.HPWC, HPWC)
                            Store(\_SB_.PCI0.LPC_.EC__.HWPN, HWPN)
                        }
                        Method(RTWG, 0x0, NotSerialized)
                        {
                            Store(HPWC, \_SB_.PCI0.LPC_.EC__.HPWC)
                            Store(HWPN, \_SB_.PCI0.LPC_.EC__.HWPN)
                        }
                    }
                    Method(_Q5C, 0x0, NotSerialized)
                    {
                        If(\H8DR)
                        {
                            If(HPLD)
                            {
                                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x500b)
                            }
                        }
                    }
                    Method(_Q5D, 0x0, NotSerialized)
                    {
                        If(\H8DR)
                        {
                            If(HPLD)
                            {
                                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x500c)
                            }
                        }
                    }
                    Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
                    {
                        Name(INDV, 0x0)
                        Method(MHQI, 0x0, NotSerialized)
                        {
                            If(And(\IPMS, 0x1, ))
                            {
                                Or(INDV, 0x1, INDV)
                            }
                            If(And(\IPMS, 0x2, ))
                            {
                                Or(INDV, 0x2, INDV)
                            }
                            If(And(\IPMS, 0x4, ))
                            {
                                Or(INDV, 0x100, INDV)
                            }
                            If(And(\IPMS, 0x8, ))
                            {
                                Or(INDV, 0x200, INDV)
                            }
                            If(And(\IPMS, 0x10, ))
                            {
                                Or(INDV, 0x4, INDV)
                            }
                            Return(INDV)
                        }
                        Method(MHGI, 0x1, NotSerialized)
                        {
                            Name(RETB, Buffer(0x10)
                            {
                            })
                            CreateByteField(RETB, 0x0, MHGS)
                            ShiftLeft(0x1, Arg0, Local0)
                            If(And(INDV, Local0, ))
                            {
                                If(LEqual(Arg0, 0x0))
                                {
                                    CreateField(RETB, 0x8, 0x78, BRBU)
                                    Store(\IPMB, BRBU)
                                    Store(0x10, MHGS)
                                }
                                Else
                                {
                                    If(LEqual(Arg0, 0x1))
                                    {
                                        CreateField(RETB, 0x8, 0x18, RRBU)
                                        Store(\IPMR, RRBU)
                                        Store(0x4, MHGS)
                                    }
                                    Else
                                    {
                                        If(LEqual(Arg0, 0x8))
                                        {
                                            CreateField(RETB, 0x10, 0x18, ODBU)
                                            CreateByteField(RETB, 0x1, MHGZ)
                                            Store(\IPMO, ODBU)
                                            If(LEqual(^^BDEV, 0x3))
                                            {
                                                If(\H8DR)
                                                {
                                                    Store(^^HPBU, Local1)
                                                }
                                                Else
                                                {
                                                    And(\RBEC(0x47), 0x1, Local1)
                                                }
                                                If(LNot(Local1))
                                                {
                                                    Or(0x4, MHGZ, MHGZ)
                                                }
                                                If(LEqual(^^BSTS, 0x0))
                                                {
                                                    Or(0x1, MHGZ, MHGZ)
                                                    Or(0x2, MHGZ, MHGZ)
                                                }
                                            }
                                            Store(0x5, MHGS)
                                        }
                                        Else
                                        {
                                            If(LEqual(Arg0, 0x9))
                                            {
                                                CreateField(RETB, 0x10, 0x8, AUBU)
                                                Store(\IPMA, AUBU)
                                                Store(0x1, Index(RETB, 0x1, ))
                                                Store(0x3, MHGS)
                                            }
                                            Else
                                            {
                                                If(LEqual(Arg0, 0x2))
                                                {
                                                    Store(\VDYN(0x0, 0x0), Local1)
                                                    And(Local1, 0xf, Index(RETB, 0x2, ))
                                                    ShiftRight(Local1, 0x4, Local1)
                                                    And(Local1, 0xf, Index(RETB, 0x1, ))
                                                    Store(0x3, MHGS)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Return(RETB)
                        }
                        Method(MHSI, 0x2, NotSerialized)
                        {
                            ShiftLeft(0x1, Arg0, Local0)
                            If(And(INDV, Local0, ))
                            {
                                If(LEqual(Arg0, 0x8))
                                {
                                    If(Arg1)
                                    {
                                        If(\H8DR)
                                        {
                                            Store(^^HPBU, Local1)
                                        }
                                        Else
                                        {
                                            And(\RBEC(0x47), 0x1, Local1)
                                        }
                                        If(LNot(Local1))
                                        {
                                            Store(^^BGID(0x0), ^^BDEV)
                                            ^^NBIN(Local1)
                                        }
                                    }
                                }
                                Else
                                {
                                    If(LEqual(Arg0, 0x2))
                                    {
                                        \VDYN(0x1, Arg1)
                                    }
                                }
                            }
                        }
                    }
                    Scope(\_SB_.PCI0.LPC_.EC__)
                    {
                        Method(_Q6A, 0x0, NotSerialized)
                        {
                            If(HDMC)
                            {
                                Noop
                            }
                            Else
                            {
                                If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x4000000))
                                {
                                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x101b)
                                }
                            }
                        }
                    }
                    Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
                    {
                        Method(MMTG, 0x0, NotSerialized)
                        {
                            Store(0x0, Local0)
                            If(HDMC)
                            {
                                Or(Local0, 0x10000, Local0)
                            }
                            Return(Local0)
                        }
                        Method(MMTS, 0x1, NotSerialized)
                        {
                            If(HDMC)
                            {
                                Noop
                            }
                            Else
                            {
                                If(LEqual(Arg0, 0x2))
                                {
                                    \_SB_.PCI0.LPC_.EC__.LED_(0xe, 0x80)
                                }
                                Else
                                {
                                    If(LEqual(Arg0, 0x3))
                                    {
                                        \_SB_.PCI0.LPC_.EC__.LED_(0xe, 0xc0)
                                    }
                                    Else
                                    {
                                        \_SB_.PCI0.LPC_.EC__.LED_(0xe, 0x0)
                                    }
                                }
                            }
                        }
                    }
                    Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
                    {
                        Method(PWMC, 0x0, NotSerialized)
                        {
                            Return(0x1)
                        }
                        Method(PWMG, 0x0, NotSerialized)
                        {
                            Store(\_SB_.PCI0.LPC_.EC__.PWMH, Local0)
                            ShiftLeft(Local0, 0x8, Local0)
                            Or(Local0, \_SB_.PCI0.LPC_.EC__.PWML, Local0)
                            Return(Local0)
                        }
                    }
                }
            }
            Device(PEG_)
            {
                Name(_ADR, 0x10000)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
                Name(LART, Package(0x2)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        \_SB_.LNKA,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        \_SB_.LNKB,
                        0x0
                    }
                })
                Name(AART, Package(0x2)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        0x0,
                        0x10
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        0x0,
                        0x11
                    }
                })
                Method(_PRT, 0x0, NotSerialized)
                {
                    If(\GPIC)
                    {
                        Return(AART)
                    }
                    Else
                    {
                        Return(LART)
                    }
                }
                Mutex(MDGS, 0x7)
                Name(VDEE, 0x1)
                Name(VDDA, Buffer(0x2)
                {
                })
                CreateBitField(VDDA, 0x0, VUPC)
                CreateBitField(VDDA, 0x1, VQDL)
                CreateBitField(VDDA, 0x2, VQDC)
                CreateBitField(VDDA, 0x3, VQDT)
                CreateBitField(VDDA, 0x4, VQDD)
                CreateBitField(VDDA, 0x5, VSDL)
                CreateBitField(VDDA, 0x6, VSDC)
                CreateBitField(VDDA, 0x7, VSDT)
                CreateBitField(VDDA, 0x8, VSDD)
                CreateBitField(VDDA, 0xa, MSWT)
                CreateBitField(VDDA, 0xb, VWST)
                Device(VID_)
                {
                    Name(_ADR, 0x0)
                    OperationRegion(VPCG, PCI_Config, 0x0, 0x100)
                    Field(VPCG, DWordAcc, NoLock, Preserve)
                    {
                        Offset(0x64),
                        VPWR, 8
                    }
                    Name(_S3D, 0x3)
                    Method(_INI, 0x0, NotSerialized)
                    {
                        \VUPS(0x2)
                        Store(\VCDL, VQDL)
                        Store(\VCDC, VQDC)
                        Store(\VCDT, VQDT)
                        Store(\VCDD, VQDD)
                    }
                    Method(_PS0, 0x0, NotSerialized)
                    {
                        Noop
                    }
                    Method(_PS1, 0x0, NotSerialized)
                    {
                        Noop
                    }
                    Method(_PS2, 0x0, NotSerialized)
                    {
                        Noop
                    }
                    Method(_PS3, 0x0, NotSerialized)
                    {
                        Noop
                    }
                    Method(VSWT, 0x0, NotSerialized)
                    {
                        If(\WVIS)
                        {
                            Store(\VEVT(0x7), Local0)
                        }
                        Else
                        {
                            Store(\VEVT(0x5), Local0)
                        }
                        And(0xf, Local0, Local1)
                        If(Local1)
                        {
                            ASWT(Local1, 0x1)
                        }
                    }
                    Method(VLOC, 0x1, NotSerialized)
                    {
                        If(LEqual(Arg0, \_SB_.LID_._LID()))
                        {
                            \VSLD(Arg0)
                            If(LEqual(And(VPWR, 0x3, ), 0x0))
                            {
                                If(Arg0)
                                {
                                    Store(\VEVT(0x1), Local0)
                                }
                                Else
                                {
                                    Store(\VEVT(0x2), Local0)
                                }
                                And(0xf, Local0, Local1)
                                If(Local1)
                                {
                                    ASWT(Local1, 0x0)
                                }
                            }
                        }
                    }
                    Method(_DOS, 0x1, NotSerialized)
                    {
                        If(LEqual(Arg0, 0x2))
                        {
                            Store(0x14, Local0)
                            While(Local0)
                            {
                                Decrement(Local0)
                                Acquire(MDGS, 0xffff)
                                If(LEqual(0x0, MSWT))
                                {
                                    Store(0x1, MSWT)
                                    Store(0x0, Local0)
                                    Store(Arg0, VDEE)
                                }
                                Release(MDGS)
                                Sleep(0xc8)
                            }
                        }
                        Else
                        {
                            Acquire(MDGS, 0xffff)
                            If(LEqual(VDEE, 0x2))
                            {
                                Store(0x0, MSWT)
                            }
                            If(LGreater(Arg0, 0x2))
                            {
                                Store(0x1, VDEE)
                            }
                            Else
                            {
                                Store(Arg0, VDEE)
                            }
                            Release(MDGS)
                        }
                    }
                    Method(_DOD, 0x0, NotSerialized)
                    {
                        Return(Package(0x2)
                        {
                            0x110,
                            0x80000100
                        })
                    }
                    Method(ASWT, 0x2, NotSerialized)
                    {
                        If(LEqual(0x1, VDEE))
                        {
                            And(0x1, Arg1, Local1)
                            \VSDS(Arg0, Local1)
                        }
                        Else
                        {
                            Store(0x14, Local0)
                            While(Local0)
                            {
                                Decrement(Local0)
                                Acquire(MDGS, 0xffff)
                                If(LEqual(0x0, MSWT))
                                {
                                    Store(0x0, Local0)
                                    If(And(0x1, Arg1, ))
                                    {
                                        Store(0x1, VUPC)
                                    }
                                    Else
                                    {
                                        Store(0x0, VUPC)
                                    }
                                    If(And(0x1, Arg0, ))
                                    {
                                        Store(0x1, VQDL)
                                    }
                                    Else
                                    {
                                        Store(0x0, VQDL)
                                    }
                                    If(And(0x2, Arg0, ))
                                    {
                                        Store(0x1, VQDC)
                                    }
                                    Else
                                    {
                                        Store(0x0, VQDC)
                                    }
                                    If(And(0x4, Arg0, ))
                                    {
                                        Store(0x1, VQDT)
                                    }
                                    Else
                                    {
                                        Store(0x0, VQDT)
                                    }
                                    If(And(0x8, Arg0, ))
                                    {
                                        Store(0x1, VQDD)
                                    }
                                    Else
                                    {
                                        Store(0x0, VQDD)
                                    }
                                }
                                Release(MDGS)
                                Sleep(0xc8)
                            }
                            If(And(0x2, Arg1, ))
                            {
                                Notify(VID_, 0x81)
                            }
                            Else
                            {
                                Notify(VID_, 0x80)
                            }
                        }
                    }
                    Method(VDSW, 0x1, NotSerialized)
                    {
                        If(LEqual(VPWR, 0x0))
                        {
                            If(Arg0)
                            {
                                Store(\VEVT(0x3), Local0)
                            }
                            Else
                            {
                                Store(\VEVT(0x4), Local0)
                            }
                            And(0xf, Local0, Local1)
                            If(Local1)
                            {
                                ASWT(Local1, 0x0)
                            }
                        }
                    }
                    Device(LCD0)
                    {
                        Name(_ADR, 0x110)
                        Method(_DCS, 0x0, NotSerialized)
                        {
                            \VUPS(0x0)
                            If(\VCDL)
                            {
                                Return(0x1f)
                            }
                            Else
                            {
                                Return(0x1d)
                            }
                        }
                        Method(_DGS, 0x0, NotSerialized)
                        {
                            Return(VQDL)
                        }
                        Method(_DSS, 0x1, NotSerialized)
                        {
                            And(Arg0, 0x1, VSDL)
                            If(And(Arg0, 0x80000000, ))
                            {
                                If(And(Arg0, 0x40000000, ))
                                {
                                    DSWT(0x2)
                                }
                                Else
                                {
                                    DSWT(0x1)
                                }
                            }
                        }
                    }
                    Device(CRT0)
                    {
                        Name(_ADR, 0x80000100)
                        Method(_DCS, 0x0, NotSerialized)
                        {
                            \VUPS(0x1)
                            If(\VCSS)
                            {
                                If(\VCDC)
                                {
                                    Return(0x1f)
                                }
                                Else
                                {
                                    Return(0x1d)
                                }
                            }
                            Else
                            {
                                If(\VCDC)
                                {
                                    Return(0xf)
                                }
                                Else
                                {
                                    Return(0xd)
                                }
                            }
                        }
                        Method(_DGS, 0x0, NotSerialized)
                        {
                            Return(VQDC)
                        }
                        Method(_DSS, 0x1, NotSerialized)
                        {
                            And(Arg0, 0x1, VSDC)
                            If(And(Arg0, 0x80000000, ))
                            {
                                If(And(Arg0, 0x40000000, ))
                                {
                                    DSWT(0x2)
                                }
                                Else
                                {
                                    DSWT(0x1)
                                }
                            }
                        }
                    }
                    Device(DVI0)
                    {
                        Name(_ADR, 0x210)
                        Method(_DCS, 0x0, NotSerialized)
                        {
                            \VUPS(0x0)
                            If(\VCDD)
                            {
                                Return(0x1f)
                            }
                            Else
                            {
                                Return(0x1d)
                            }
                        }
                        Method(_DGS, 0x0, NotSerialized)
                        {
                            Return(VQDD)
                        }
                        Method(_DSS, 0x1, NotSerialized)
                        {
                            And(Arg0, 0x1, VSDD)
                            If(And(Arg0, 0x80000000, ))
                            {
                                If(And(Arg0, 0x40000000, ))
                                {
                                    DSWT(0x2)
                                }
                                Else
                                {
                                    DSWT(0x1)
                                }
                            }
                        }
                    }
                    Device(DP0_)
                    {
                        Name(_ADR, 0x220)
                        Method(_DCS, 0x0, NotSerialized)
                        {
                            \VUPS(0x0)
                            If(\VCDT)
                            {
                                Return(0x1f)
                            }
                            Else
                            {
                                Return(0x1d)
                            }
                        }
                        Method(_DGS, 0x0, NotSerialized)
                        {
                            Return(VQDT)
                        }
                        Method(_DSS, 0x1, NotSerialized)
                        {
                            And(Arg0, 0x1, VSDT)
                            If(And(Arg0, 0x80000000, ))
                            {
                                If(And(Arg0, 0x40000000, ))
                                {
                                    DSWT(0x2)
                                }
                                Else
                                {
                                    DSWT(0x1)
                                }
                            }
                        }
                    }
                    Method(DSWT, 0x1, NotSerialized)
                    {
                        If(VSDL)
                        {
                            Store(0x1, Local0)
                        }
                        Else
                        {
                            Store(0x0, Local0)
                        }
                        If(VSDC)
                        {
                            Or(0x2, Local0, Local0)
                        }
                        If(VSDD)
                        {
                            Or(0x8, Local0, Local0)
                        }
                        If(Local0)
                        {
                            If(VUPC)
                            {
                                \VSDS(Local0, Arg0)
                            }
                        }
                        Else
                        {
                            Noop
                        }
                    }
                }
                OperationRegion(PEGC, PCI_Config, 0x0, 0x100)
                Field(PEGC, DWordAcc, NoLock, Preserve)
                {
                    Offset(0xec),
                    GMGP, 1,
                    HPGP, 1,
                    PMGP, 1
                }
            }
            Device(IIO1)
            {
                Name(_ADR, 0x80000)
                Name(RID_, 0x0)
                OperationRegion(IIOC, PCI_Config, 0x0, 0x100)
                Field(IIOC, DWordAcc, NoLock, Preserve)
                {
                    Offset(0xd0),
                    , 26,
                    TOUD, 6
                }
            }
            Device(IGBE)
            {
                Name(_ADR, 0x190000)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
                Name(_PRW, Package(0x2)
                {
                    0xd,
                    0x4
                })
            }
            Device(EXP1)
            {
                Name(_ADR, 0x1c0000)
                Name(RID_, 0x0)
                OperationRegion(PECS, PCI_Config, 0x0, 0x100)
                Field(PECS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x62),
                    PS__, 1,
                    PP__, 1,
                    Offset(0xdb),
                    , 7,
                    PMCE, 1,
                    , 24,
                    , 7,
                    PMCS, 1
                }
                Name(_PRW, Package(0x2)
                {
                    0x9,
                    0x4
                })
                Name(LPRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        \_SB_.LNKA,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        \_SB_.LNKB,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        \_SB_.LNKC,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        \_SB_.LNKD,
                        0x0
                    }
                })
                Name(APRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        0x0,
                        0x10
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        0x0,
                        0x11
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        0x0,
                        0x12
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        0x0,
                        0x13
                    }
                })
                Method(_PRT, 0x0, NotSerialized)
                {
                    If(\GPIC)
                    {
                        Return(APRT)
                    }
                    Else
                    {
                        Return(LPRT)
                    }
                }
            }
            Device(EXP2)
            {
                Name(_ADR, 0x1c0001)
                Name(RID_, 0x0)
                OperationRegion(PECS, PCI_Config, 0x0, 0x100)
                Field(PECS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x62),
                    PS__, 1,
                    PP__, 1,
                    Offset(0xdb),
                    , 7,
                    PMCE, 1,
                    , 24,
                    , 7,
                    PMCS, 1
                }
                Name(_PRW, Package(0x2)
                {
                    0x9,
                    0x4
                })
                Name(LPRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        \_SB_.LNKB,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        \_SB_.LNKC,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        \_SB_.LNKD,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        \_SB_.LNKA,
                        0x0
                    }
                })
                Name(APRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        0x0,
                        0x11
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        0x0,
                        0x12
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        0x0,
                        0x13
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        0x0,
                        0x10
                    }
                })
                Method(_PRT, 0x0, NotSerialized)
                {
                    If(\GPIC)
                    {
                        Return(APRT)
                    }
                    Else
                    {
                        Return(LPRT)
                    }
                }
            }
            Device(EXP3)
            {
                Name(_ADR, 0x1c0002)
                Name(RID_, 0x0)
                OperationRegion(PECS, PCI_Config, 0x0, 0x100)
                Field(PECS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x62),
                    PS__, 1,
                    PP__, 1,
                    Offset(0xdb),
                    , 7,
                    PMCE, 1,
                    , 24,
                    , 7,
                    PMCS, 1
                }
                Name(_PRW, Package(0x2)
                {
                    0x9,
                    0x4
                })
                Name(LPRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        \_SB_.LNKC,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        \_SB_.LNKD,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        \_SB_.LNKA,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        \_SB_.LNKB,
                        0x0
                    }
                })
                Name(APRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        0x0,
                        0x12
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        0x0,
                        0x13
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        0x0,
                        0x10
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        0x0,
                        0x11
                    }
                })
                Method(_PRT, 0x0, NotSerialized)
                {
                    If(\GPIC)
                    {
                        Return(APRT)
                    }
                    Else
                    {
                        Return(LPRT)
                    }
                }
            }
            Device(EXP4)
            {
                Name(_ADR, 0x1c0003)
                Name(RID_, 0x0)
                OperationRegion(PECS, PCI_Config, 0x0, 0x100)
                Field(PECS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x5a),
                    , 3,
                    PDC_, 1,
                    , 2,
                    PDS_, 1,
                    , 1,
                    Offset(0x62),
                    PS__, 1,
                    PP__, 1,
                    Offset(0xdb),
                    , 6,
                    HPCE, 1,
                    PMCE, 1,
                    , 24,
                    , 6,
                    HPCS, 1,
                    PMCS, 1
                }
                Method(_INI, 0x0, NotSerialized)
                {
                    Store(PDS_, PDSF)
                }
                Name(_PRW, Package(0x2)
                {
                    0x9,
                    0x4
                })
                Name(LPRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        \_SB_.LNKD,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        \_SB_.LNKA,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        \_SB_.LNKB,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        \_SB_.LNKC,
                        0x0
                    }
                })
                Name(APRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        0x0,
                        0x13
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        0x0,
                        0x10
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        0x0,
                        0x11
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        0x0,
                        0x12
                    }
                })
                Method(_PRT, 0x0, NotSerialized)
                {
                    If(\GPIC)
                    {
                        Return(APRT)
                    }
                    Else
                    {
                        Return(LPRT)
                    }
                }
                Name(PDSF, 0x0)
                Device(SLOT)
                {
                    Name(_ADR, 0x0)
                    Method(_RMV, 0x0, NotSerialized)
                    {
                        Return(0x1)
                    }
                }
            }
            Device(EXP5)
            {
                Name(_ADR, 0x1c0004)
                Name(RID_, 0x0)
                OperationRegion(PECS, PCI_Config, 0x0, 0x100)
                Field(PECS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x62),
                    PS__, 1,
                    PP__, 1,
                    Offset(0xdb),
                    , 7,
                    PMCE, 1,
                    , 24,
                    , 7,
                    PMCS, 1
                }
                Name(_PRW, Package(0x2)
                {
                    0x9,
                    0x4
                })
                Name(LPRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        \_SB_.LNKA,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        \_SB_.LNKB,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        \_SB_.LNKC,
                        0x0
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        \_SB_.LNKD,
                        0x0
                    }
                })
                Name(APRT, Package(0x4)
                {
                    Package(0x4)
                    {
                        0xffff,
                        0x0,
                        0x0,
                        0x10
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x1,
                        0x0,
                        0x11
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x2,
                        0x0,
                        0x12
                    },
                    Package(0x4)
                    {
                        0xffff,
                        0x3,
                        0x0,
                        0x13
                    }
                })
                Method(_PRT, 0x0, NotSerialized)
                {
                    If(\GPIC)
                    {
                        Return(APRT)
                    }
                    Else
                    {
                        Return(LPRT)
                    }
                }
            }
            Device(PCI1)
            {
                Name(_ADR, 0x1e0000)
                Name(RID_, 0x0)
            }
            Device(SAT1)
            {
                Name(_ADR, 0x1f0002)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
            }
            Device(SAT2)
            {
                Name(_ADR, 0x1f0005)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
            }
            Device(SMBU)
            {
                Name(_ADR, 0x1f0003)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
            }
            Device(EHC1)
            {
                Name(_ADR, 0x1d0000)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
                OperationRegion(EHCS, PCI_Config, 0x0, 0x100)
                Field(EHCS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x62),
                    PWKI, 1,
                    PWUC, 8,
                    , 7
                }
                Name(_PR0, Package(0x1)
                {
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Name(_PR1, Package(0x1)
                {
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Name(_PR2, Package(0x1)
                {
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Method(_INI, 0x0, NotSerialized)
                {
                    Store(0x1, PWKI)
                    Store(0x23, PWUC)
                }
                Name(_PRW, Package(0x3)
                {
                    0xd,
                    0x3,
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Device(URTH)
                {
                    Name(_ADR, 0x0)
                    Device(URMH)
                    {
                        Name(_ADR, 0x1)
                        Name(_UPC, Package(0x4)
                        {
                            0xff,
                            0xff,
                            0x0,
                            0x0
                        })
                        Name(_PLD, Buffer(0x10)
                        {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                        })
                        Device(PRT0)
                        {
                            Name(_ADR, 0x1)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0x0,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x61, 0x10, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT1)
                        {
                            Name(_ADR, 0x2)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0x0,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x59, 0x10, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT2)
                        {
                            Name(_ADR, 0x3)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT3)
                        {
                            Name(_ADR, 0x4)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT4)
                        {
                            Name(_ADR, 0x5)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT5)
                        {
                            Name(_ADR, 0x6)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0x2,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x12, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT6)
                        {
                            Name(_ADR, 0x7)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRT7)
                        {
                            Name(_ADR, 0x8)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                    }
                }
            }
            Device(EHC2)
            {
                Name(_ADR, 0x1a0000)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
                OperationRegion(EHCS, PCI_Config, 0x0, 0x100)
                Field(EHCS, DWordAcc, NoLock, Preserve)
                {
                    Offset(0x62),
                    PWKI, 1,
                    PWUC, 6,
                    , 9
                }
                Name(_PR0, Package(0x1)
                {
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Name(_PR1, Package(0x1)
                {
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Name(_PR2, Package(0x1)
                {
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Method(_INI, 0x0, NotSerialized)
                {
                    Store(0x1, PWKI)
                    Store(0x13, PWUC)
                }
                Name(_PRW, Package(0x3)
                {
                    0xd,
                    0x3,
                    \_SB_.PCI0.LPC_.EC__.PUBS
                })
                Device(URTH)
                {
                    Name(_ADR, 0x0)
                    Device(URMH)
                    {
                        Name(_ADR, 0x1)
                        Name(_UPC, Package(0x4)
                        {
                            0xff,
                            0xff,
                            0x0,
                            0x0
                        })
                        Name(_PLD, Buffer(0x10)
                        {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                        })
                        Device(PRT8)
                        {
                            Name(_ADR, 0x1)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0x0,
                                0x0,
                                0x0
                            })
                            Method(_PLD, 0x0, Serialized)
                            {
                                If(LEqual(\PJID, 0x0))
                                {
                                    Return(Buffer(0x10)
                                    {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x59, 0x12, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                                    })
                                }
                                Else
                                {
                                    Return(Buffer(0x10)
                                    {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x10, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                                    })
                                }
                            }
                        }
                        Device(PRT9)
                        {
                            Name(_ADR, 0x2)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0x0,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x12, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRTA)
                        {
                            Name(_ADR, 0x3)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRTB)
                        {
                            Name(_ADR, 0x4)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRTC)
                        {
                            Name(_ADR, 0x5)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0x0,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6b, 0x11, 0x00, 0x00,
	0x03, 0x00, 0x00, 0x00
                            })
                        }
                        Device(PRTD)
                        {
                            Name(_ADR, 0x6)
                            Name(_UPC, Package(0x4)
                            {
                                0xff,
                                0xff,
                                0x0,
                                0x0
                            })
                            Name(_PLD, Buffer(0x10)
                            {
	0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x1c, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
                            })
                        }
                    }
                }
            }
            Device(HDEF)
            {
                Name(_ADR, 0x1b0000)
                Name(_S3D, 0x3)
                Name(RID_, 0x0)
                Name(_PRW, Package(0x2)
                {
                    0xd,
                    0x4
                })
                Method(_PSW, 0x1, NotSerialized)
                {
                    Noop
                }
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Name(BDEV, 0xff)
            Name(BSTS, 0x0)
            Name(BHKE, 0x0)
            Method(_Q2C, 0x0, NotSerialized)
            {
                If(LEqual(BSTS, 0x0))
                {
                    Store(BGID(0x0), BDEV)
                    NBRE(BDEV)
                }
            }
            Method(_Q2D, 0x0, NotSerialized)
            {
                Store(BGID(0x0), BDEV)
                NBIN(BDEV)
            }
            Method(_Q38, 0x0, NotSerialized)
            {
                Store(BGID(0x0), Local0)
                If(LEqual(Local0, 0xf))
                {
                    BDIS()
                    \BHDP(0x1, 0x0)
                    NBEJ(BDEV)
                    Store(Local0, BDEV)
                    If(LEqual(\BIDE, 0x3))
                    {
                        Store(0x0, \_SB_.PCI0.SAT1.PRIM.GTME)
                        Store(0x0, \_SB_.PCI0.SAT1.SCND.GTME)
                    }
                }
                Else
                {
                    If(HPBU)
                    {
                    }
                    Else
                    {
                        Store(Local0, BDEV)
                        NBIN(Local0)
                    }
                }
            }
            Name(ODEJ, 0x0)
            Method(_Q44, 0x0, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.CSON)
                {
                    Store(0x1, ODEJ)
                    Store(BGID(0x0), BDEV)
                    NBIN(BDEV)
                    Store(0x0, ODEJ)
                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x3006)
                }
            }
            Method(NBRE, 0x1, NotSerialized)
            {
                If(LLess(Arg0, 0xc))
                {
                    If(LEqual(\BIDE, 0x3))
                    {
                        Notify(\_SB_.PCI0.SAT1.SCND.MSTR, 0x3)
                    }
                    Else
                    {
                        Notify(\_SB_.PCI0.SAT1.PRT1, 0x3)
                    }
                }
                If(LEqual(Arg0, 0x10))
                {
                    If(LOr(HPAC, HB0A))
                    {
                        If(\WNTF)
                        {
                            Notify(\_SB_.PCI0.LPC_.EC__.BAT1, 0x3)
                        }
                    }
                    Else
                    {
                        LED_(0x4, 0xc0)
                        BEEP(0xf)
                        Store(0x2, BSTS)
                    }
                }
            }
            Method(NBEJ, 0x1, NotSerialized)
            {
                If(LEqual(BSTS, 0x0))
                {
                    If(LLess(Arg0, 0xc))
                    {
                        If(LEqual(\BIDE, 0x3))
                        {
                            Notify(\_SB_.PCI0.SAT1.SCND.MSTR, 0x1)
                        }
                        Else
                        {
                            Notify(\_SB_.PCI0.SAT1.PRT1, 0x1)
                        }
                    }
                    If(LEqual(Arg0, 0x10))
                    {
                        If(\WNTF)
                        {
                            Notify(\_SB_.PCI0.LPC_.EC__.BAT1, 0x1)
                        }
                        Else
                        {
                            Notify(\_SB_.PCI0.LPC_.EC__.BAT1, 0x81)
                        }
                    }
                }
                LED_(0x4, 0x0)
                BEEP(0x0)
                Store(0x0, BSTS)
            }
            Method(NBIN, 0x1, NotSerialized)
            {
                If(LLess(Arg0, 0xc))
                {
                    BEN_(0x1)
                    LED_(0x4, 0x80)
                    If(LEqual(\BIDE, 0x3))
                    {
                        Notify(\_SB_.PCI0.SAT1.SCND.MSTR, 0x1)
                    }
                    Else
                    {
                        Notify(\_SB_.PCI0.SAT1.PRT1, 0x1)
                    }
                }
                If(LEqual(Arg0, 0x10))
                {
                    LED_(0x4, 0x80)
                    If(\WNTF)
                    {
                        Store(0x1, \_SB_.PCI0.LPC_.EC__.BAT1.XB1S)
                        Notify(\_SB_.PCI0.LPC_.EC__.BAT1, 0x1)
                    }
                    Else
                    {
                        Notify(\_SB_.PCI0.LPC_.EC__.BAT1, 0x81)
                    }
                }
                BEEP(0x0)
                Store(0x0, BSTS)
            }
            Method(BSCN, 0x0, NotSerialized)
            {
                If(LNot(HPBU))
                {
                    Store(BGID(0x0), Local0)
                    Store(Local0, BDEV)
                    If(LLess(Local0, 0xc))
                    {
                        If(LEqual(Local0, 0x6))
                        {
                            BEN_(0x2)
                        }
                        Else
                        {
                            BEN_(0x1)
                        }
                        LED_(0x4, 0x80)
                    }
                    BEEP(0x0)
                    Store(0x0, BSTS)
                }
            }
            Method(BEJ0, 0x1, NotSerialized)
            {
                If(Arg0)
                {
                    BDIS()
                    LED_(0x4, 0x0)
                    \BHDP(0x1, 0x0)
                    Store(0x1, BSTS)
                    If(BHKE)
                    {
                        Store(0x0, BHKE)
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x3003)
                    }
                }
                Else
                {
                    LED_(0x4, 0x80)
                    Store(0x0, BSTS)
                }
            }
            Method(BEJF, 0x0, NotSerialized)
            {
                If(LNot(\_SB_.PCI0.LPC_.CSON))
                {
                    Store(0x1, \_SB_.PCI0.LPC_.CSON)
                    Store(0xf, \IDET)
                }
                LED_(0x4, 0x0)
                \BHDP(0x1, 0x0)
                Store(0x1, BSTS)
            }
            Method(BEJS, 0x0, NotSerialized)
            {
                If(LNot(\_SB_.PCI0.LPC_.GLIS))
                {
                    Store(0x1, \_SB_.PCI0.LPC_.GLIS)
                }
            }
            Method(BEJ3, 0x1, NotSerialized)
            {
                If(Arg0)
                {
                    BDIS()
                    Store(0x1, BSTS)
                }
                Else
                {
                    Store(0x0, BSTS)
                }
            }
            Method(BPTS, 0x1, NotSerialized)
            {
                Store(0x1, HDBM)
                If(LNot(LEqual(BSTS, 0x0)))
                {
                    Store(0xf, BDEV)
                    Store(0x0, BSTS)
                }
                Store(0x0, BHKE)
                Store(0x1, Local0)
                If(LNot(LEqual(BDEV, 0xf)))
                {
                }
                Else
                {
                    Store(0x0, Local0)
                }
                If(LNot(LLess(Arg0, 0x4)))
                {
                    Store(0x0, Local0)
                }
                If(Local0)
                {
                    BUWK(0x1)
                }
                Else
                {
                    LED_(0x4, 0x0)
                    BUWK(0x0)
                }
            }
            Method(BWAK, 0x1, NotSerialized)
            {
                BUWK(0x0)
                Store(BGID(0x0), Local0)
                If(LEqual(BSTS, 0x0))
                {
                    If(LNot(LEqual(Local0, BDEV)))
                    {
                        NBEJ(BDEV)
                        Store(Local0, BDEV)
                        NBIN(Local0)
                    }
                    Else
                    {
                        If(LOr(\LFDC, LNot(LEqual(BDEV, 0xd))))
                        {
                            If(LNot(LEqual(Local0, 0xf)))
                            {
                                LED_(0x4, 0x80)
                                If(HPBU)
                                {
                                    Or(ShiftLeft(Arg0, 0x8, ), 0x2005, BHKE)
                                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(BHKE)
                                    If(LNot(LGreater(Arg0, 0x2)))
                                    {
                                    }
                                    Else
                                    {
                                        NBRE(Local0)
                                    }
                                }
                            }
                        }
                    }
                }
                If(LLess(BDEV, 0xc))
                {
                    Store(0x0, \_SB_.PCI0.LPC_.GLIS)
                }
                Else
                {
                    Store(0x1, \_SB_.PCI0.LPC_.GLIS)
                }
            }
            Method(BDIS, 0x0, NotSerialized)
            {
                If(LNot(\_SB_.PCI0.LPC_.CSON))
                {
                    If(LNot(\_SB_.PCI0.LPC_.GLIS))
                    {
                        Store(0x1, \_SB_.PCI0.LPC_.GLIS)
                    }
                    Store(0x1, \_SB_.PCI0.LPC_.CSON)
                    Store(0xf, \IDET)
                }
            }
            Method(BPON, 0x1, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.CSON)
                {
                    Store(0x0, \_SB_.PCI0.LPC_.CSON)
                    If(\_SB_.PCI0.LPC_.GLIS)
                    {
                        Store(0x0, \_SB_.PCI0.LPC_.GLIS)
                    }
                }
            }
            Method(BEN_, 0x1, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.CSON)
                {
                    BPON(Arg0)
                    If(LAnd(Arg0, LEqual(ODEJ, 0x1)))
                    {
                        Store(0x9, \_SB_.PCI0.LPC_.EC__.HANT)
                    }
                    If(Arg0)
                    {
                        IRDY()
                    }
                }
            }
            Method(BSTA, 0x1, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.CSON)
                {
                    Return(0x0)
                }
                BINI()
                If(LEqual(Arg0, 0x1))
                {
                    Return(LLess(BDEV, 0xc))
                }
                Return(0x0)
            }
            Method(BUWK, 0x1, NotSerialized)
            {
                If(\H8DR)
                {
                    If(Arg0)
                    {
                        Store(0x1, \_SB_.PCI0.LPC_.EC__.HWBU)
                    }
                    Else
                    {
                        Store(0x0, \_SB_.PCI0.LPC_.EC__.HWBU)
                    }
                }
                Else
                {
                    If(Arg0)
                    {
                        \MBEC(0x32, 0xff, 0x80)
                    }
                    Else
                    {
                        \MBEC(0x32, 0x7f, 0x0)
                    }
                }
            }
            Method(BINI, 0x0, NotSerialized)
            {
                If(LEqual(BDEV, 0xff))
                {
                    Store(BGID(0x0), BDEV)
                }
            }
            Method(BGID, 0x1, NotSerialized)
            {
                If(Arg0)
                {
                    Store(0xff, Local0)
                }
                Else
                {
                    If(\H8DR)
                    {
                        Store(HPBU, Local1)
                        Store(HBID, Local2)
                    }
                    Else
                    {
                        Store(RBEC(0x47), Local2)
                        And(Local2, 0x1, Local1)
                        And(Local2, 0x4, Local2)
                        ShiftRight(Local2, 0x2, Local2)
                    }
                    If(Local2)
                    {
                        Store(0xf, Local0)
                    }
                    Else
                    {
                        If(HDUB)
                        {
                            Store(0xf, Local0)
                        }
                        Else
                        {
                            If(LOr(LEqual(\IDET, 0x3), LEqual(\IDET, 0x6)))
                            {
                                Store(\IDET, Local0)
                            }
                            Else
                            {
                                Store(0x7, Local0)
                            }
                        }
                    }
                    If(LEqual(Local0, 0xf))
                    {
                        If(\H8DR)
                        {
                            If(HB1A)
                            {
                                Store(0x10, Local0)
                            }
                        }
                        Else
                        {
                            If(And(\RBEC(0x39), 0x80, ))
                            {
                                Store(0x10, Local0)
                            }
                        }
                    }
                    If(LEqual(Local0, 0xf))
                    {
                    }
                }
                If(LAnd(\HDUB, LLess(Local0, 0xc)))
                {
                    Store(0xf, Local0)
                }
                Return(Local0)
            }
            Method(IRDY, 0x0, NotSerialized)
            {
                Store(0x1f4, Local0)
                Store(0x3c, Local1)
                Store(Zero, Local2)
                While(Local1)
                {
                    Sleep(Local0)
                    Store(\BCHK(), Local3)
                    If(LNot(Local3))
                    {
                        Break
                    }
                    If(LEqual(Local3, 0x2))
                    {
                        Store(One, Local2)
                        Break
                    }
                    Decrement(Local1)
                }
                Return(Local2)
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__.BAT1)
        {
            Method(_EJ0, 0x1, NotSerialized)
            {
                Store(0x0, B1ST)
                Store(0x0, XB1S)
                \_SB_.PCI0.LPC_.EC__.BEJ0(Arg0)
            }
        }
        Scope(\_SB_)
        {
            Device(GDCK)
            {
                Name(_HID, 0x79004d24)
                Name(_CID, 0x150cd041)
                Method(_STA, 0x0, NotSerialized)
                {
                    Store(GGID(), Local0)
                    Store(0x0, Local1)
                    If(LEqual(Local0, 0x3))
                    {
                        Store(\_SB_.PCI0.LPC_.EC__.SSTA(), Local1)
                    }
                    Return(Local1)
                }
                Method(_INI, 0x0, NotSerialized)
                {
                    \_SB_.PCI0.LPC_.EC__.SINI()
                    \_SB_.PCI0.LPC_.EC__.DATT(0x2, 0x1)
                    If(LEqual(GGID(), 0x7))
                    {
                        \_SB_.PCI0.LPC_.EC__.DATT(0x1, 0x0)
                        \_SB_.PCI0.LPC_.EC__.DATT(0x0, 0x1)
                    }
                    Else
                    {
                        \_SB_.PCI0.LPC_.EC__.DATT(0x1, 0x1)
                        \_SB_.PCI0.LPC_.EC__.DATT(0x0, 0x0)
                    }
                    \_SB_.PCI0.LPC_.EC__.DDWK(0x0)
                    Store(0x1, \_SB_.PCI0.LPC_.DSCI)
                }
                Method(_DCK, 0x1, NotSerialized)
                {
                    Store(0x0, Local0)
                    If(LEqual(GGID(), 0x3))
                    {
                        Store(\_SB_.PCI0.LPC_.EC__.SDCK(Arg0), Local0)
                    }
                    If(\VIGD)
                    {
                        \_SB_.PCI0.VID_.VDSW(Arg0)
                    }
                    Return(Local0)
                }
                Name(UDOP, 0x0)
                Method(_EJ0, 0x1, NotSerialized)
                {
                    If(LEqual(GGID(), 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.SEJ0(Arg0)
                    }
                }
                Method(_EJ3, 0x1, NotSerialized)
                {
                    If(LEqual(GGID(), 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.SEJ3(Arg0)
                    }
                }
                Method(_EJ4, 0x1, NotSerialized)
                {
                    If(LEqual(GGID(), 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.SEJ4(Arg0)
                    }
                }
                Method(PEJ3, 0x0, NotSerialized)
                {
                    If(LEqual(GGID(), 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.PSE3()
                    }
                }
                Method(_BDN, 0x0, NotSerialized)
                {
                    Store(0x0, Local0)
                    If(LEqual(GGID(), 0x3))
                    {
                        Store(\_SB_.PCI0.LPC_.EC__.SLBN(), Local0)
                    }
                    Return(Local0)
                }
                Method(_UID, 0x0, NotSerialized)
                {
                    Store(0x0, Local0)
                    If(LEqual(GGID(), 0x3))
                    {
                        Store(\_SB_.PCI0.LPC_.EC__.SLUD(), Local0)
                    }
                    Return(Local0)
                }
                Method(GPTS, 0x1, NotSerialized)
                {
                    \_SB_.PCI0.LPC_.EC__.SPTS(Arg0)
                }
                Method(GWAK, 0x1, NotSerialized)
                {
                    \_SB_.PCI0.LPC_.EC__.SWAK(Arg0)
                    \_SB_.PCI0.LPC_.EC__.DDWK(0x0)
                }
                Method(GGPE, 0x0, NotSerialized)
                {
                    If(LEqual(GGID(), 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.SGPE()
                    }
                }
                Name(G_ID, 0xffffffff)
                Method(GGID, 0x0, NotSerialized)
                {
                    Store(G_ID, Local0)
                    If(LEqual(Local0, 0xffffffff))
                    {
                        ShiftRight(\_SB_.PCI0.LPC_.GL00, 0x3, Local0)
                        And(Local0, 0x7, Local0)
                        If(LEqual(Local0, 0x0))
                        {
                            Store(0x3, Local0)
                        }
                        Store(Local0, G_ID)
                    }
                    Return(Local0)
                }
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Method(_Q50, 0x0, NotSerialized)
            {
                Store(\_SB_.GDCK.GGID(), Local0)
                If(LNot(LEqual(Local0, 0x7)))
                {
                    \_SB_.PCI0.LPC_.EC__.LED_(0x8, 0x80)
                    \_SB_.PCI0.LPC_.EC__.LED_(0x9, 0xc0)
                    If(LEqual(Local0, 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.SPEJ()
                    }
                    Notify(\_SB_.GDCK, 0x3)
                }
            }
            Method(_Q45, 0x0, NotSerialized)
            {
                Store(0xffffffff, \_SB_.GDCK.G_ID)
                Store(0xffffffff, \_SB_.PCI0.LPC_.EC__.SLID)
                Store(\_SB_.GDCK.GGID(), Local0)
                If(LEqual(Local0, 0x7))
                {
                    GUSB(0x0)
                    \_SB_.PCI0.LPC_.EC__.BEJS()
                    Notify(\_SB_.GDCK, 0x3)
                }
                If(LEqual(Local0, 0x3))
                {
                    ASSI(0x0)
                    Sleep(0x64)
                    If(\H8DR)
                    {
                        Store(SLIS, Local1)
                    }
                    Else
                    {
                        And(\RBEC(0x2), 0x1, Local1)
                    }
                    If(LEqual(Local1, 0x1))
                    {
                        Notify(\_SB_.GDCK, 0x0)
                    }
                }
            }
            Method(GUSB, 0x1, NotSerialized)
            {
                Store(\_SB_.PCI0.LPC_.GL03, Local0)
                Or(Local0, ShiftLeft(Arg0, 0x4, ), \_SB_.PCI0.LPC_.GL03)
                If(\H8DR)
                {
                    Store(Arg0, SLIS)
                }
                Else
                {
                    \MBEC(0x2, 0xfe, Arg0)
                }
            }
            Method(DATT, 0x2, NotSerialized)
            {
                Store(0x0, Local0)
                If(LEqual(Arg0, 0x0))
                {
                    If(LEqual(Arg1, 0x1))
                    {
                        If(\H8DR)
                        {
                            Or(HAM6, 0x80, HAM6)
                        }
                        Else
                        {
                            \MBEC(0x16, 0xff, 0x80)
                        }
                        Store(0x1, Local0)
                    }
                    If(LEqual(Arg1, 0x0))
                    {
                        If(\H8DR)
                        {
                            And(HAM6, 0x7f, HAM6)
                        }
                        Else
                        {
                            \MBEC(0x16, 0x7f, 0x0)
                        }
                    }
                    If(LEqual(Arg1, 0x2))
                    {
                        If(\H8DR)
                        {
                            If(And(HAM6, 0x80, ))
                            {
                                Store(0x1, Local0)
                            }
                        }
                        Else
                        {
                            If(And(\RBEC(0x16), 0x80, ))
                            {
                                Store(0x1, Local0)
                            }
                        }
                    }
                }
                If(LEqual(Arg0, 0x1))
                {
                    If(LEqual(Arg1, 0x1))
                    {
                        If(\H8DR)
                        {
                            Or(HAMA, 0x1, HAMA)
                        }
                        Else
                        {
                            \MBEC(0x1a, 0xff, 0x1)
                        }
                        Store(0x1, Local0)
                    }
                    If(LEqual(Arg1, 0x0))
                    {
                        If(\H8DR)
                        {
                            And(HAMA, 0xfe, HAMA)
                        }
                        Else
                        {
                            \MBEC(0x1a, 0xfe, 0x0)
                        }
                    }
                    If(LEqual(Arg1, 0x2))
                    {
                        If(\H8DR)
                        {
                            If(And(HAMA, 0x1, ))
                            {
                                Store(0x1, Local0)
                            }
                        }
                        Else
                        {
                            If(And(\RBEC(0x1a), 0x1, ))
                            {
                                Store(0x1, Local0)
                            }
                        }
                    }
                }
                If(LEqual(Arg0, 0x2))
                {
                    If(LEqual(Arg1, 0x1))
                    {
                        If(\H8DR)
                        {
                            Or(HAMB, 0x1, HAMB)
                        }
                        Else
                        {
                            \MBEC(0x1b, 0xff, 0x1)
                        }
                        Store(0x1, Local0)
                    }
                    If(LEqual(Arg1, 0x0))
                    {
                        If(\H8DR)
                        {
                            And(HAMB, 0xfe, HAMB)
                        }
                        Else
                        {
                            \MBEC(0x1b, 0xfe, 0x0)
                        }
                    }
                    If(LEqual(Arg1, 0x2))
                    {
                        If(\H8DR)
                        {
                            If(And(HAMB, 0x1, ))
                            {
                                Store(0x1, Local0)
                            }
                        }
                        Else
                        {
                            If(And(\RBEC(0x1b), 0x1, ))
                            {
                                Store(0x1, Local0)
                            }
                        }
                    }
                }
                Return(Local0)
            }
            Method(DDWK, 0x1, NotSerialized)
            {
                Store(0x0, Local0)
                If(LEqual(Arg0, 0x1))
                {
                    If(\H8DR)
                    {
                        Store(One, HWDK)
                    }
                    Else
                    {
                        \MBEC(0x32, 0xff, 0x8)
                    }
                    Store(0x1, Local0)
                }
                If(LEqual(Arg0, 0x0))
                {
                    If(\H8DR)
                    {
                        Store(Zero, HWDK)
                    }
                    Else
                    {
                        \MBEC(0x32, 0xf7, 0x0)
                    }
                }
                If(LEqual(Arg0, 0x2))
                {
                    If(\H8DR)
                    {
                        If(HWDK)
                        {
                            Store(0x1, Local0)
                        }
                    }
                    Else
                    {
                        If(And(\RBEC(0x32), 0x8, ))
                        {
                            Store(0x1, Local0)
                        }
                    }
                }
                Return(Local0)
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Name(SLID, 0xffffffff)
            Name(SIDB, 0xffffffff)
            Name(SFLG, 0x0)
            Name(SUCT, 0x0)
            Name(SHKE, 0x0)
            Method(SLBN, 0x0, NotSerialized)
            {
                Store(0x0, Local0)
                If(LEqual(\_SB_.GDCK.GGID(), 0x3))
                {
                    Return(0x200ae30)
                }
            }
            Method(SLUD, 0x0, NotSerialized)
            {
                Return(0x0)
            }
            Method(SSTA, 0x0, NotSerialized)
            {
                SUDK()
                SUDT()
                If(LEqual(GSID(), 0x3))
                {
                    Store(0xf, Local0)
                }
                Else
                {
                    If(LNot(\W98F))
                    {
                        Store(0x0, Local0)
                    }
                    Else
                    {
                        Store(0xc, Local0)
                    }
                }
                If(\W98F)
                {
                    Store(HIDS(Local0), Local0)
                }
                Return(Local0)
            }
            Method(SINI, 0x0, NotSerialized)
            {
                If(LEqual(GSID(), 0x3))
                {
                    Or(SFLG, 0x400, SFLG)
                }
                Else
                {
                    And(SFLG, Not(0x400, ), SFLG)
                }
            }
            Method(SPTS, 0x1, NotSerialized)
            {
                If(LAnd(LNot(LLess(Arg0, 0x1)), LNot(LGreater(Arg0, 0x4))))
                {
                    Store(0x0, SHKE)
                    Store(0x0, SIDB)
                    If(And(SFLG, 0x2, ))
                    {
                        Store(0x0, SLID)
                        And(\_SB_.PCI0.LPC_.GL03, 0xef, Local0)
                        Store(Local0, \_SB_.PCI0.LPC_.GL03)
                        And(SFLG, Not(0x2, ), SFLG)
                    }
                    If(LEqual(GSID(), 0x3))
                    {
                        If(LEqual(Arg0, 0x3))
                        {
                            \_SB_.PCI0.LPC_.EC__.DDWK(0x1)
                        }
                        Store(0x3, SIDB)
                    }
                    Or(SFLG, 0x100, SFLG)
                }
            }
            Method(SWAK, 0x1, NotSerialized)
            {
                Store(0xffffffff, SLID)
                If(LAnd(LNot(LLess(Arg0, 0x1)), LNot(LGreater(Arg0, 0x4))))
                {
                    If(LEqual(SIDB, 0x3))
                    {
                        If(LEqual(GSID(), 0x3))
                        {
                            LED_(0x3, 0x80)
                            ShiftLeft(Arg0, 0x8, SHKE)
                        }
                        Else
                        {
                            GUSB(0x0)
                            Notify(\_SB_.GDCK, 0x0)
                            And(SFLG, Not(0x400, ), SFLG)
                        }
                    }
                    Else
                    {
                        If(LEqual(GSID(), 0x3))
                        {
                            ASSI(0x0)
                            Sleep(0x64)
                            WSDK()
                        }
                        Else
                        {
                            Noop
                        }
                    }
                    And(SFLG, Not(0x100, ), SFLG)
                    And(SFLG, Not(0x2, ), SFLG)
                    And(SFLG, Not(0x10, ), SFLG)
                    And(SFLG, Not(0x20, ), SFLG)
                }
            }
            Method(SGPE, 0x0, NotSerialized)
            {
                Or(SFLG, 0x8, SFLG)
            }
            Method(SDCK, 0x1, NotSerialized)
            {
                If(\H8DR)
                {
                    If(Arg0)
                    {
                        BSCN()
                        \_SB_.PCI0.LPC_.EC__.DATT(0x1, 0x1)
                        Or(SFLG, 0x400, SFLG)
                    }
                    Else
                    {
                        Or(SFLG, 0x2, SFLG)
                        GUSB(0x0)
                        Store(0x1, HB1I)
                        If(\WNTF)
                        {
                            Store(0x0, \_SB_.PCI0.LPC_.EC__.BAT1.B1ST)
                            Store(0x0, \_SB_.PCI0.LPC_.EC__.BAT1.XB1S)
                        }
                        BEJF()
                        \_SB_.PCI0.LPC_.EC__.DATT(0x1, 0x0)
                        And(SFLG, Not(0x400, ), SFLG)
                    }
                }
                Return(0x1)
            }
            Method(SEJ0, 0x1, NotSerialized)
            {
                Store(0x0, SLID)
                If(Arg0)
                {
                    ASSI(0x1)
                }
                LED_(0x3, 0x0)
                SUDI()
                And(SFLG, Not(0x2, ), SFLG)
            }
            Method(SEJ3, 0x1, NotSerialized)
            {
                Or(SFLG, 0x10, SFLG)
                If(LEqual(\SPS_, 0x3))
                {
                    PSE3()
                }
            }
            Method(SEJ4, 0x1, NotSerialized)
            {
                Or(SFLG, 0x20, SFLG)
            }
            Method(PSE3, 0x0, NotSerialized)
            {
                If(And(SFLG, 0x10, ))
                {
                    LED_(0x3, 0x0)
                }
            }
            Name(SHDE, 0x0)
            Method(WSDK, 0x0, NotSerialized)
            {
                If(And(SFLG, Or(0x10, 0x20, ), ))
                {
                    SDCK(0x1)
                    If(\W98F)
                    {
                        Or(SFLG, 0x200, SFLG)
                        Store(0x5, SHDE)
                    }
                }
                Else
                {
                    Notify(\_SB_.GDCK, 0x0)
                }
            }
            Method(HIDS, 0x1, NotSerialized)
            {
                Store(Arg0, Local0)
                If(And(SFLG, 0x200, ))
                {
                    If(LEqual(Arg0, 0xf))
                    {
                        If(\W98F)
                        {
                            Store(0xc, Local0)
                        }
                        Decrement(SHDE)
                        If(LNot(SHDE))
                        {
                            And(SFLG, Not(0x200, ), SFLG)
                            Notify(\_SB_.GDCK, 0x0)
                        }
                    }
                    Else
                    {
                        And(SFLG, Not(0x200, ), SFLG)
                    }
                }
                Return(Local0)
            }
            Method(SUDK, 0x0, NotSerialized)
            {
                If(And(SFLG, 0x8, ))
                {
                    If(LNot(And(SFLG, 0x100, )))
                    {
                        Or(SHKE, 0x2004, SHKE)
                        If(LEqual(\UOPT, 0x0))
                        {
                            \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(SHKE)
                        }
                        If(\W98F)
                        {
                            Notify(\_SB_.GDCK, 0x1)
                        }
                        Else
                        {
                            Notify(\_SB_.GDCK, 0x3)
                        }
                        And(SFLG, Not(0x8, ), SFLG)
                    }
                }
            }
            Method(SUDI, 0x0, NotSerialized)
            {
                If(\WNTF)
                {
                    Store(0x1, SUCT)
                }
                Else
                {
                    Store(0x5, SUCT)
                }
            }
            Method(SUDT, 0x0, NotSerialized)
            {
                If(And(SHKE, 0x2004, ))
                {
                    If(LEqual(GSID(), 0x0))
                    {
                        If(LNot(Decrement(SUCT)))
                        {
                            Store(0x0, SHKE)
                            If(LEqual(\UOPT, 0x0))
                            {
                                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x4003)
                            }
                        }
                    }
                }
            }
            Method(GSID, 0x0, NotSerialized)
            {
                If(LEqual(SLID, 0xffffffff))
                {
                    Store(0xffffffff, \_SB_.GDCK.G_ID)
                    If(LEqual(\_SB_.GDCK.GGID(), 0x3))
                    {
                        Store(0x3, SLID)
                    }
                    Else
                    {
                        Store(0x0, SLID)
                    }
                }
                Return(SLID)
            }
            Method(SPEJ, 0x0, NotSerialized)
            {
                LED_(0x3, 0x0)
                Sleep(0xc8)
                LED_(0x3, 0x80)
                Sleep(0xc8)
                LED_(0x3, 0x0)
                Sleep(0xc8)
                LED_(0x3, 0x80)
                Sleep(0xc8)
                LED_(0x3, 0x0)
                Sleep(0xc8)
                LED_(0x3, 0x80)
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Method(_Q43, 0x0, NotSerialized)
            {
                \UCMS(0x18)
            }
            Method(SAUM, 0x1, NotSerialized)
            {
                If(LGreater(Arg0, 0x3))
                {
                    Noop
                }
                Else
                {
                    If(\H8DR)
                    {
                        Store(Arg0, HAUM)
                    }
                    Else
                    {
                        \MBEC(0x3, 0x9f, ShiftLeft(Arg0, 0x5, ))
                    }
                }
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Name(BRTW, Package(0x12)
            {
                0x64,
                0x64,
                0x2,
                0x3,
                0x4,
                0x7,
                0x9,
                0xb,
                0xd,
                0x11,
                0x14,
                0x18,
                0x1c,
                0x20,
                0x28,
                0x32,
                0x42,
                0x64
            })
            Name(BRTB, Package(0x3)
            {
                Package(0x10)
                {
                    0x4,
                    0x8,
                    0xb,
                    0x11,
                    0x16,
                    0x1c,
                    0x22,
                    0x2b,
                    0x33,
                    0x3c,
                    0x47,
                    0x52,
                    0x65,
                    0x80,
                    0xa8,
                    0xe8
                },
                Package(0x10)
                {
                    0x4,
                    0x8,
                    0xb,
                    0x11,
                    0x16,
                    0x1c,
                    0x22,
                    0x2b,
                    0x33,
                    0x3c,
                    0x47,
                    0x52,
                    0x65,
                    0x80,
                    0xa8,
                    0xe8
                },
                Package(0x5)
                {
                    0x11d,
                    0x271,
                    0x8,
                    0x4,
                    0x8
                }
            })
            Method(_Q14, 0x0, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x8000))
                {
                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1010)
                }
                If(\NBCF)
                {
                    If(\VIGD)
                    {
                        Notify(\_SB_.PCI0.VID_.LCD0, 0x86)
                    }
                    Else
                    {
                        Notify(\_SB_.PCI0.PEG_.VID_.LCD0, 0x86)
                    }
                }
                Else
                {
                    If(\VIGD)
                    {
                        Store(\UCMS(0x15), \BRLV)
                        Store(\BRLV, Local0)
                        If(LNot(LEqual(Local0, 0xf)))
                        {
                            Increment(Local0)
                            Store(Local0, \BRLV)
                        }
                        \UCMS(0x16)
                        \_SB_.PCI0.LPC_.EC__.BRNS()
                    }
                    Else
                    {
                        \UCMS(0x4)
                    }
                }
            }
            Method(_Q15, 0x0, NotSerialized)
            {
                If(\VCDB)
                {
                    Store(0x0, \VCDB)
                    Store(\UCMS(0x15), \BRLV)
                    \UCMS(0x16)
                    \_SB_.PCI0.LPC_.EC__.BRNS()
                    Return(0x0)
                }
                If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x10000))
                {
                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1011)
                }
                If(\NBCF)
                {
                    If(\VIGD)
                    {
                        Notify(\_SB_.PCI0.VID_.LCD0, 0x87)
                    }
                    Else
                    {
                        Notify(\_SB_.PCI0.PEG_.VID_.LCD0, 0x87)
                    }
                }
                Else
                {
                    If(\VIGD)
                    {
                        Store(\UCMS(0x15), \BRLV)
                        Store(\BRLV, Local0)
                        If(Local0)
                        {
                            Decrement(Local0)
                            Store(Local0, \BRLV)
                        }
                        \UCMS(0x16)
                        \_SB_.PCI0.LPC_.EC__.BRNS()
                    }
                    Else
                    {
                        \UCMS(0x5)
                    }
                }
            }
            Method(BRNS, 0x0, NotSerialized)
            {
                Store(\BRLV, Local0)
                If(\_SB_.PCI0.VID_.DRDY)
                {
                    If(LEqual(0x0, Local0))
                    {
                        Store(DerefOf(Index(DerefOf(Index(BRTB, 0x2, )), 0x3, )), Local1)
                        Store(DerefOf(Index(DerefOf(Index(BRTB, 0x2, )), 0x0, )), Local2)
                    }
                    Else
                    {
                        Store(DerefOf(Index(DerefOf(Index(BRTB, 0x2, )), 0x4, )), Local1)
                        Store(DerefOf(Index(DerefOf(Index(BRTB, 0x2, )), 0x1, )), Local2)
                    }
                    Or(Local1, ShiftLeft(Local2, 0x9, ), Local2)
                    \_SB_.PCI0.VID_.AINT(0x3, Local2)
                    Store(0x0, Local1)
                    If(\BRHB)
                    {
                        Store(0x1, Local1)
                    }
                    Store(DerefOf(Index(DerefOf(Index(BRTB, Local1, )), Local0, )), Local2)
                    \_SB_.PCI0.VID_.AINT(0x1, Local2)
                }
                Else
                {
                    \UCMS(0x12)
                }
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Method(_Q19, 0x0, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x800000))
                {
                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1018)
                }
                \UCMS(0x3)
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Method(_Q63, 0x0, NotSerialized)
            {
                If(\_SB_.PCI0.LPC_.EC__.HKEY.MHKK(0x80000))
                {
                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x1014)
                }
                \UCMS(0xb)
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Method(_Q70, 0x0, NotSerialized)
            {
                FNST()
            }
            Method(_Q72, 0x0, NotSerialized)
            {
                FNST()
            }
            Method(_Q73, 0x0, NotSerialized)
            {
                FNST()
            }
            Method(FNST, 0x0, NotSerialized)
            {
                If(\H8DR)
                {
                    Store(HFNS, Local0)
                    Store(HFNE, Local1)
                }
                Else
                {
                    And(\RBEC(0xe), 0x3, Local0)
                    And(\RBEC(0x0), 0x8, Local1)
                }
                If(Local1)
                {
                    If(LEqual(Local0, 0x0))
                    {
                        \UCMS(0x11)
                    }
                    If(LEqual(Local0, 0x1))
                    {
                        \UCMS(0xf)
                    }
                    If(LEqual(Local0, 0x2))
                    {
                        \UCMS(0x10)
                    }
                }
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
        {
            Name(WGFL, 0x0)
            Method(WLSW, 0x0, NotSerialized)
            {
                Return(\_SB_.PCI0.LPC_.EC__.GSTS)
            }
            Method(GWAN, 0x0, NotSerialized)
            {
                Store(0x0, Local0)
                If(And(WGFL, 0x1, ))
                {
                    Or(Local0, 0x1, Local0)
                }
                If(And(WGFL, 0x8, ))
                {
                    Return(Local0)
                }
                If(WPWS())
                {
                    Or(Local0, 0x2, Local0)
                }
                If(And(WGFL, 0x4, ))
                {
                    Or(Local0, 0x4, Local0)
                }
                Return(Local0)
            }
            Method(SWAN, 0x1, NotSerialized)
            {
                If(And(Arg0, 0x2, ))
                {
                    WPWC(0x1)
                }
                Else
                {
                    WPWC(0x0)
                }
                If(And(Arg0, 0x4, ))
                {
                    Or(WGFL, 0x4, WGFL)
                    \WGSV(0x2)
                }
                Else
                {
                    And(WGFL, Not(0x4, ), WGFL)
                    \WGSV(0x3)
                }
            }
            Method(GBDC, 0x0, NotSerialized)
            {
                Store(0x0, Local0)
                If(And(WGFL, 0x10, ))
                {
                    Or(Local0, 0x1, Local0)
                }
                If(And(WGFL, 0x80, ))
                {
                    Return(Local0)
                }
                If(BPWS())
                {
                    Or(Local0, 0x2, Local0)
                }
                If(And(WGFL, 0x40, ))
                {
                    Or(Local0, 0x4, Local0)
                }
                Return(Local0)
            }
            Method(SBDC, 0x1, NotSerialized)
            {
                If(And(Arg0, 0x2, ))
                {
                    BPWC(0x1)
                }
                Else
                {
                    BPWC(0x0)
                }
                If(And(Arg0, 0x4, ))
                {
                    Or(WGFL, 0x40, WGFL)
                    \BLTH(0x2)
                }
                Else
                {
                    And(WGFL, Not(0x40, ), WGFL)
                    \BLTH(0x3)
                }
            }
            Method(GUWB, 0x0, NotSerialized)
            {
                Store(0x0, Local0)
                If(And(WGFL, 0x100, ))
                {
                    Or(Local0, 0x1, Local0)
                }
                If(UPWS())
                {
                    Or(Local0, 0x2, Local0)
                }
                Return(Local0)
            }
            Method(SUWB, 0x1, NotSerialized)
            {
                If(And(Arg0, 0x2, ))
                {
                    UPWC(0x1)
                }
                Else
                {
                    UPWC(0x0)
                }
            }
            Method(WPWS, 0x0, NotSerialized)
            {
                If(\H8DR)
                {
                    Store(\_SB_.PCI0.LPC_.EC__.DCWW, Local0)
                }
                Else
                {
                    Store(ShiftRight(And(\RBEC(0x3a), 0x40, ), 0x6, ), Local0)
                }
                Return(Local0)
            }
            Method(WTGL, 0x0, NotSerialized)
            {
                If(And(WGFL, 0x1, ))
                {
                    WPWC(LNot(WPWS()))
                }
            }
            Method(WPWC, 0x1, NotSerialized)
            {
                If(LAnd(Arg0, LAnd(And(WGFL, 0x1, ), LNot(And(WGFL, 0x8, )))))
                {
                    If(\H8DR)
                    {
                        Store(One, \_SB_.PCI0.LPC_.EC__.DCWW)
                    }
                    Else
                    {
                        \MBEC(0x3a, 0xff, 0x40)
                    }
                    Or(WGFL, 0x2, WGFL)
                }
                Else
                {
                    If(\H8DR)
                    {
                        Store(Zero, \_SB_.PCI0.LPC_.EC__.DCWW)
                    }
                    Else
                    {
                        \MBEC(0x3a, 0xbf, 0x0)
                    }
                    And(WGFL, Not(0x2, ), WGFL)
                }
            }
            Method(BPWS, 0x0, NotSerialized)
            {
                If(\H8DR)
                {
                    Store(\_SB_.PCI0.LPC_.EC__.DCBD, Local0)
                }
                Else
                {
                    Store(ShiftRight(And(\RBEC(0x3a), 0x10, ), 0x4, ), Local0)
                }
                Return(Local0)
            }
            Method(BTGL, 0x0, NotSerialized)
            {
                If(And(WGFL, 0x10, ))
                {
                    BPWC(LNot(BPWS()))
                }
            }
            Method(BPWC, 0x1, NotSerialized)
            {
                If(LAnd(Arg0, LAnd(And(WGFL, 0x10, ), LNot(And(WGFL, 0x80, )))))
                {
                    If(\H8DR)
                    {
                        Store(One, \_SB_.PCI0.LPC_.EC__.DCBD)
                    }
                    Else
                    {
                        \MBEC(0x3a, 0xff, 0x10)
                    }
                    Or(WGFL, 0x20, WGFL)
                }
                Else
                {
                    If(\H8DR)
                    {
                        Store(Zero, \_SB_.PCI0.LPC_.EC__.DCBD)
                    }
                    Else
                    {
                        \MBEC(0x3a, 0xef, 0x0)
                    }
                    And(WGFL, Not(0x20, ), WGFL)
                }
            }
            Method(UPWS, 0x0, NotSerialized)
            {
                If(\H8DR)
                {
                    Store(\_SB_.PCI0.LPC_.EC__.HUWB, Local0)
                }
                Else
                {
                    Store(ShiftRight(And(\RBEC(0x31), 0x4, ), 0x2, ), Local0)
                }
                Return(Local0)
            }
            Method(UPWC, 0x1, NotSerialized)
            {
                If(LAnd(Arg0, And(WGFL, 0x100, )))
                {
                    If(\H8DR)
                    {
                        Store(One, \_SB_.PCI0.LPC_.EC__.HUWB)
                    }
                    Else
                    {
                        \MBEC(0x31, 0xff, 0x4)
                    }
                    Or(WGFL, 0x200, WGFL)
                }
                Else
                {
                    If(\H8DR)
                    {
                        Store(Zero, \_SB_.PCI0.LPC_.EC__.HUWB)
                    }
                    Else
                    {
                        \MBEC(0x31, 0xfb, 0x0)
                    }
                    And(WGFL, Not(0x200, ), WGFL)
                }
                \UCMS(0x1b)
            }
            Method(WGIN, 0x0, NotSerialized)
            {
                Store(0x0, WGFL)
                Store(\WGSV(0x1), WGFL)
                If(WPWS())
                {
                    Or(WGFL, 0x2, WGFL)
                }
                If(BPWS())
                {
                    Or(WGFL, 0x20, WGFL)
                }
                If(UPWS())
                {
                    Or(WGFL, 0x200, WGFL)
                }
            }
            Method(WGPS, 0x1, NotSerialized)
            {
                If(LNot(LLess(Arg0, 0x4)))
                {
                    \BLTH(0x5)
                }
                If(LNot(And(WGFL, 0x4, )))
                {
                    WPWC(0x0)
                }
                If(LNot(And(WGFL, 0x40, )))
                {
                    BPWC(0x0)
                }
            }
            Method(WGWK, 0x1, NotSerialized)
            {
                If(And(WGFL, 0x20, ))
                {
                    BPWC(0x1)
                }
                If(And(WGFL, 0x2, ))
                {
                    WPWC(0x1)
                }
            }
        }
        Scope(\_SB_.PCI0.LPC_.EC__)
        {
            Method(_Q41, 0x0, NotSerialized)
            {
                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x7000)
            }
        }
        Device(WMI1)
        {
            Name(_HID, 0x140cd041)
            Name(_UID, 0x1)
            Name(_WDG, Buffer(0xa0)
            {
	0x0e, 0x23, 0xf5, 0x51, 0x77, 0x96, 0xcd, 0x46, 0xa1, 0xcf, 0xc0, 0xb2,
	0x3e, 0xe3, 0x4d, 0xb7, 0x41, 0x30, 0x80, 0x05, 0x64, 0x9a, 0x47, 0x98,
	0xf5, 0x33, 0x33, 0x4e, 0xa7, 0x07, 0x8e, 0x25, 0x1e, 0xbb, 0xc3, 0xa1,
	0x41, 0x31, 0x01, 0x06, 0xef, 0x54, 0x4b, 0x6a, 0xed, 0xa5, 0x33, 0x4d,
	0x94, 0x55, 0xb0, 0xd9, 0xb4, 0x8d, 0xf4, 0xb3, 0x41, 0x32, 0x01, 0x06,
	0xb6, 0xeb, 0xf1, 0x74, 0x7a, 0x92, 0x7d, 0x4c, 0x95, 0xdf, 0x69, 0x8e,
	0x21, 0xe8, 0x0e, 0xb5, 0x41, 0x33, 0x01, 0x06, 0xff, 0x04, 0xef, 0x7e,
	0x28, 0x43, 0x7c, 0x44, 0xb5, 0xbb, 0xd4, 0x49, 0x92, 0x5d, 0x53, 0x8d,
	0x41, 0x34, 0x01, 0x06, 0x9e, 0x15, 0xdb, 0x8a, 0x32, 0x1e, 0x5c, 0x45,
	0xbc, 0x93, 0x30, 0x8a, 0x7e, 0xd9, 0x82, 0x46, 0x41, 0x35, 0x01, 0x01,
	0xfd, 0xd9, 0x51, 0x26, 0x1c, 0x91, 0x69, 0x4b, 0xb9, 0x4e, 0xd0, 0xde,
	0xd5, 0x96, 0x3b, 0xd7, 0x41, 0x36, 0x01, 0x06, 0x21, 0x12, 0x90, 0x05,
	0x66, 0xd5, 0xd1, 0x11, 0xb2, 0xf0, 0x00, 0xa0, 0xc9, 0x06, 0x29, 0x10,
	0x42, 0x41, 0x01, 0x00
            })
            Name(RETN, Package(0x5)
            {
                "Success",
                "Not Supported",
                "Invalid Parameter",
                "Access Denied",
                "System Busy"
            })
            Name(ITEM, Package(0x66)
            {
                Package(0x2)
                {
                    0x1e,
                    "WakeOnLAN"
                },
                Package(0x2)
                {
                    0x0,
                    "FlashOverLAN"
                },
                Package(0x2)
                {
                    0x1,
                    "EthernetLANOptionROM"
                },
                Package(0x2)
                {
                    0x0,
                    "HarddriveDMA"
                },
                Package(0x2)
                {
                    0x4,
                    "WirelessLANandWiMAXRadios"
                },
                Package(0x2)
                {
                    0x18,
                    "SerialPort"
                },
                Package(0x2)
                {
                    0x10,
                    "SerialPortIO"
                },
                Package(0x2)
                {
                    0x6,
                    "SerialPortIRQ"
                },
                Package(0x2)
                {
                    0x18,
                    "ParallelPort"
                },
                Package(0x2)
                {
                    0x9,
                    "ParallelPortMode"
                },
                Package(0x2)
                {
                    0x11,
                    "ParallelPortIO"
                },
                Package(0x2)
                {
                    0x7,
                    "ParallelPortIRQ"
                },
                Package(0x2)
                {
                    0x8,
                    "ParallelPortDMA"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTA"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTB"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTC"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTD"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTE"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTF"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTG"
                },
                Package(0x2)
                {
                    0x12,
                    "PCIINTH"
                },
                Package(0x2)
                {
                    0x0,
                    "USBBIOSSupport"
                },
                Package(0x2)
                {
                    0x0,
                    "AlwaysOnUSB"
                },
                Package(0x2)
                {
                    0x19,
                    "TrackPoint"
                },
                Package(0x2)
                {
                    0x2,
                    "TouchPad"
                },
                Package(0x2)
                {
                    0x0,
                    "FnKeyLock"
                },
                Package(0x2)
                {
                    0xa,
                    "ThinkPadNumLock"
                },
                Package(0x2)
                {
                    0x17,
                    "PowerOnNumLock"
                },
                Package(0x2)
                {
                    0xb,
                    "PrimaryVideo"
                },
                Package(0x2)
                {
                    0xc,
                    "BootDisplayDevice"
                },
                Package(0x2)
                {
                    0x5,
                    "LCDBrightness"
                },
                Package(0x2)
                {
                    0x0,
                    "TimerWakeWithBattery"
                },
                Package(0x2)
                {
                    0x0,
                    "SpeedStep"
                },
                Package(0x2)
                {
                    0x13,
                    "SpeedStepModeAC"
                },
                Package(0x2)
                {
                    0x13,
                    "SpeedStepModeBattery"
                },
                Package(0x2)
                {
                    0x14,
                    "AdaptiveThermalManagementAC"
                },
                Package(0x2)
                {
                    0x14,
                    "AdaptiveThermalManagementBattery"
                },
                Package(0x2)
                {
                    0xd,
                    "CDROMSpeed"
                },
                Package(0x2)
                {
                    0x2,
                    "CPUPowerManagement"
                },
                Package(0x2)
                {
                    0x2,
                    "PCIPowerManagement"
                },
                Package(0x2)
                {
                    0x0,
                    "PowerControlBeep"
                },
                Package(0x2)
                {
                    0x0,
                    "LowBatteryAlarm"
                },
                Package(0x2)
                {
                    0x0,
                    "PasswordBeep"
                },
                Package(0x2)
                {
                    0x0,
                    "KeyboardBeep"
                },
                Package(0x2)
                {
                    0x1,
                    "ExtendedMemoryTest"
                },
                Package(0x2)
                {
                    0xe,
                    "SATAControllerMode"
                },
                Package(0x2)
                {
                    0x1d,
                    "CoreMultiProcessing"
                },
                Package(0x2)
                {
                    0x0,
                    "VirtualizationTechnology"
                },
                Package(0x2)
                {
                    0x1,
                    "LegacyDevicesOnMiniDock"
                },
                Package(0x2)
                {
                    0x0,
                    "Passphrase"
                },
                Package(0x2)
                {
                    0x0,
                    "LockBIOSSetting"
                },
                Package(0x2)
                {
                    0x16,
                    "MinimumPasswordLength"
                },
                Package(0x2)
                {
                    0x0,
                    "BIOSPasswordAtUnattendedBoot"
                },
                Package(0x2)
                {
                    0x0,
                    "PasswordResetService"
                },
                Package(0x2)
                {
                    0x0,
                    "FingerprintPredesktopAuthentication"
                },
                Package(0x2)
                {
                    0xf,
                    "FingerprintReaderPriority"
                },
                Package(0x2)
                {
                    0x5,
                    "FingerprintSecurityMode"
                },
                Package(0x2)
                {
                    0x3,
                    "SecurityChip"
                },
                Package(0x2)
                {
                    0x0,
                    "BIOSUpdateByEndUsers"
                },
                Package(0x2)
                {
                    0x1,
                    "DataExecutionPrevention"
                },
                Package(0x2)
                {
                    0x1,
                    "EthernetLANAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "WirelessLANAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "WirelessWANAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "BluetoothAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "WirelessUSBAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "ModemAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "USBPortAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "IEEE1394Access"
                },
                Package(0x2)
                {
                    0x1,
                    "SerialPortAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "ParallelPortAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "CardBusSlotAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "ExpressCardAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "PCIExpressSlotAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "UltrabayAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "MemoryCardSlotAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "SmartCardSlotAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "IntegratedCameraAccess"
                },
                Package(0x2)
                {
                    0x1,
                    "MicrophoneAccess"
                },
                Package(0x2)
                {
                    0x15,
                    "BootMode"
                },
                Package(0x2)
                {
                    0x1,
                    "StartupOptionKeys"
                },
                Package(0x2)
                {
                    0x1,
                    "BootDeviceListF12Option"
                },
                Package(0x2)
                {
                    0x64,
                    "BootOrder"
                },
                Package(0x2)
                {
                    0x64,
                    "NetworkBootOrder"
                },
                Package(0x2)
                {
                    0x1,
                    "WiMAXAccess"
                },
                Package(0x2)
                {
                    0x1a,
                    "GraphicsDevice"
                },
                Package(0x2)
                {
                    0x0,
                    "TXTFeature"
                },
                Package(0x2)
                {
                    0x0,
                    "VTdFeature"
                },
                Package(0x2)
                {
                    0x0,
                    "AMTControl"
                },
                Package(0x2)
                {
                    0x0,
                    "FingerprintPasswordAuthentication"
                },
                Package(0x2)
                {
                    0x1,
                    "FingerprintReaderAccess"
                },
                Package(0x2)
                {
                    0x0,
                    "OsDetectionForSwitchableGraphics"
                },
                Package(0x2)
                {
                    0x1,
                    "ComputraceModuleActivation"
                },
                Package(0x2)
                {
                    0x1b,
                    "PCIExpressPowerManagement"
                },
                Package(0x2)
                {
                    0x0,
                    "ATpModuleActivation"
                },
                Package(0x2)
                {
                    0x0,
                    "FullTheftProtectionModuleActivation"
                },
                Package(0x2)
                {
                    0x0,
                    "RemoteDisableModuleActivation"
                },
                Package(0x2)
                {
                    0x1,
                    "eSATAPortAccess"
                },
                Package(0x2)
                {
                    0x0,
                    "HardwarePasswordManager"
                },
                Package(0x2)
                {
                    0x1c,
                    "AlwaysOnUSBMode"
                },
                Package(0x2)
                {
                    0x0,
                    "HyperThreadingTechnology"
                },
                Package(0x2)
                {
                    0x0,
                    "FnCtrlKeySwap"
                },
                Package(0x2)
                {
                    0x0,
                    "OnByAcAttach"
                }
            })
            Name(VSEL, Package(0x1f)
            {
                Package(0x2)
                {
                    "Disable",
                    "Enable"
                },
                Package(0x2)
                {
                    "Enable",
                    "Disable"
                },
                Package(0x2)
                {
                    "Disable",
                    "Automatic"
                },
                Package(0x4)
                {
                    "Inactive",
                    "Disable",
                    "Active",
                    ""
                },
                Package(0x4)
                {
                    "On",
                    "",
                    "Off",
                    ""
                },
                Package(0x2)
                {
                    "Normal",
                    "High"
                },
                Package(0x4)
                {
                    "3",
                    "4",
                    "5",
                    "7"
                },
                Package(0x2)
                {
                    "5",
                    "7"
                },
                Package(0x3)
                {
                    "0",
                    "1",
                    "3"
                },
                Package(0x3)
                {
                    "OutputOnly",
                    "BiDirectional",
                    "ECP"
                },
                Package(0x2)
                {
                    "Independent",
                    "Synchronized"
                },
                Package(0x2)
                {
                    "PCIExpress",
                    "Internal"
                },
                Package(0x10)
                {
                    "LCD",
                    "VGA",
                    "Digital",
                    "Digital1onDock",
                    "Digital2onDock",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                },
                Package(0x4)
                {
                    "",
                    "High",
                    "Normal",
                    "Silent"
                },
                Package(0x2)
                {
                    "Compatibility",
                    "AHCI"
                },
                Package(0x2)
                {
                    "External",
                    "InternalOnly"
                },
                Package(0x4)
                {
                    "3F8",
                    "2F8",
                    "3E8",
                    "2E8"
                },
                Package(0x3)
                {
                    "378",
                    "278",
                    "3BC"
                },
                Package(0x10)
                {
                    "Disable",
                    "AutoSelect",
                    "",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "",
                    "9",
                    "10",
                    "11",
                    "",
                    "",
                    "",
                    ""
                },
                Package(0x4)
                {
                    "MaximumPerformance",
                    "BatteryOptimized",
                    "MaximumBattery",
                    "Automatic"
                },
                Package(0x4)
                {
                    "",
                    "",
                    "MaximizePerformance",
                    "Balanced"
                },
                Package(0x2)
                {
                    "Quick",
                    "Diagnostics"
                },
                Package(0xd)
                {
                    "Disable",
                    "",
                    "",
                    "",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                    "11",
                    "12"
                },
                Package(0x4)
                {
                    "Auto",
                    "On",
                    "Off",
                    ""
                },
                Package(0x4)
                {
                    "",
                    "Enable",
                    "Disable",
                    ""
                },
                Package(0x4)
                {
                    "Disable",
                    "",
                    "Automatic",
                    ""
                },
                Package(0x4)
                {
                    "",
                    "IntegratedGfx",
                    "DiscreteGfx",
                    "SwitchableGfx"
                },
                Package(0x2)
                {
                    "Automatic",
                    "Disable"
                },
                Package(0x4)
                {
                    "Automatic",
                    "BlackBerry",
                    "iPodiPhone",
                    "OtherUSBDevices"
                },
                Package(0x4)
                {
                    "Disable",
                    "",
                    "",
                    "Enable"
                },
                Package(0x4)
                {
                    "Disable",
                    "Enable",
                    "ACOnly",
                    "ACandBattery"
                }
            })
            Name(VLST, Package(0xf)
            {
                "HDD0",
                "HDD1",
                "HDD2",
                "HDD3",
                "PCILAN",
                "ATAPICD0",
                "ATAPICD1",
                "",
                "USBFDD",
                "USBCD",
                "USBHDD",
                "",
                "",
                "",
                "NODEV"
            })
            Name(PENC, Package(0x2)
            {
                "ascii",
                "scancode"
            })
            Name(PKBD, Package(0x3)
            {
                "us",
                "fr",
                "gr"
            })
            Name(PTYP, Package(0x8)
            {
                "pap",
                "pop",
                "uhdp1",
                "mhdp1",
                "uhdp2",
                "mhdp2",
                "uhdp3",
                "mhdp3"
            })
            Mutex(MWMI, 0x7)
            Name(PCFG, Buffer(0x18)
            {
            })
            Name(IBUF, Buffer(0x100)
            {
            })
            Name(ILEN, 0x0)
            Name(PSTR, Buffer(0x81)
            {
            })
            Method(WQA0, 0x1, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                If(LNot(LEqual(\WMIS(0x0, Arg0), 0x0)))
                {
                    Release(MWMI)
                    Return("")
                }
                Store(DerefOf(Index(ITEM, \WITM, )), Local0)
                Store(DerefOf(Index(Local0, 0x0, )), Local1)
                Store(DerefOf(Index(Local0, 0x1, )), Local2)
                If(LLess(Local1, 0x64))
                {
                    Concatenate(Local2, ",", Local6)
                    Store(DerefOf(Index(VSEL, Local1, )), Local3)
                    Concatenate(Local6, DerefOf(Index(Local3, \WSEL, )), Local7)
                }
                Else
                {
                    Store(SizeOf(VLST), Local3)
                    If(LNot(LGreater(\WLS0, Local3)))
                    {
                        Concatenate(Local2, ",", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS0, )), Local2)
                    }
                    If(LNot(LGreater(\WLS1, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS1, )), Local2)
                    }
                    If(LNot(LGreater(\WLS2, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS2, )), Local2)
                    }
                    If(LNot(LGreater(\WLS3, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS3, )), Local2)
                    }
                    If(LNot(LGreater(\WLS4, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS4, )), Local2)
                    }
                    If(LNot(LGreater(\WLS5, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS5, )), Local2)
                    }
                    If(LNot(LGreater(\WLS6, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS6, )), Local2)
                    }
                    If(LNot(LGreater(\WLS7, Local3)))
                    {
                        Concatenate(Local2, ":", Local7)
                        Concatenate(Local7, DerefOf(Index(VLST, \WLS7, )), Local2)
                    }
                    Store(Local2, Local7)
                }
                Release(MWMI)
                Return(Local7)
            }
            Method(WMA1, 0x3, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                If(LEqual(SizeOf(Arg2), 0x0))
                {
                    Store(0x2, Local0)
                }
                Else
                {
                    Store(CARG(Arg2), Local0)
                    If(LEqual(Local0, 0x0))
                    {
                        Store(WSET(), Local0)
                    }
                }
                Release(MWMI)
                Return(DerefOf(Index(RETN, Local0, )))
            }
            Method(WMA2, 0x3, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                Store(CARG(Arg2), Local0)
                If(LEqual(Local0, 0x0))
                {
                    If(LNot(LEqual(ILEN, 0x0)))
                    {
                        Store(CPAS(IBUF, 0x0), Local0)
                    }
                    If(LEqual(Local0, 0x0))
                    {
                        Store(\WMIS(0x2, 0x0), Local0)
                    }
                }
                Release(MWMI)
                Return(DerefOf(Index(RETN, Local0, )))
            }
            Method(WMA3, 0x3, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                Store(CARG(Arg2), Local0)
                If(LEqual(Local0, 0x0))
                {
                    If(LNot(LEqual(ILEN, 0x0)))
                    {
                        Store(CPAS(IBUF, 0x0), Local0)
                    }
                    If(LEqual(Local0, 0x0))
                    {
                        Store(\WMIS(0x3, 0x0), Local0)
                    }
                }
                Release(MWMI)
                Return(DerefOf(Index(RETN, Local0, )))
            }
            Method(WMA4, 0x3, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                Store(CARG(Arg2), Local0)
                If(LEqual(Local0, 0x0))
                {
                    If(LNot(LEqual(ILEN, 0x0)))
                    {
                        Store(CPAS(IBUF, 0x0), Local0)
                    }
                    If(LEqual(Local0, 0x0))
                    {
                        Store(\WMIS(0x4, 0x0), Local0)
                    }
                }
                Release(MWMI)
                Return(DerefOf(Index(RETN, Local0, )))
            }
            Method(WQA5, 0x1, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                Store(\WMIS(0x5, 0x0), Local0)
                Store(\WSPM, Index(PCFG, 0x0, ))
                Store(\WSPS, Index(PCFG, 0x4, ))
                Store(\WSMN, Index(PCFG, 0x8, ))
                Store(\WSMX, Index(PCFG, 0xc, ))
                Store(\WSEN, Index(PCFG, 0x10, ))
                Store(\WSKB, Index(PCFG, 0x14, ))
                Release(MWMI)
                Return(PCFG)
            }
            Method(WMA6, 0x3, NotSerialized)
            {
                Acquire(MWMI, 0xffff)
                If(LEqual(SizeOf(Arg2), 0x0))
                {
                    Store(0x2, Local0)
                }
                Else
                {
                    Store(CARG(Arg2), Local0)
                    If(LEqual(Local0, 0x0))
                    {
                        If(LNot(LEqual(ILEN, 0x0)))
                        {
                            Store(SPAS(IBUF), Local0)
                        }
                        If(LEqual(Local0, 0x0))
                        {
                            Store(\WMIS(0x6, 0x0), Local0)
                        }
                    }
                }
                Release(MWMI)
                Return(DerefOf(Index(RETN, Local0, )))
            }
            Method(CARG, 0x1, NotSerialized)
            {
                Store(SizeOf(Arg0), Local0)
                If(LEqual(Local0, 0x0))
                {
                    Store(0x0, IBUF)
                    Store(0x0, ILEN)
                    Return(0x0)
                }
                If(LNot(LEqual(ObjectType(Arg0), 0x2)))
                {
                    Return(0x2)
                }
                If(LNot(LLess(Local0, 0xff)))
                {
                    Return(0x2)
                }
                Store(Arg0, IBUF)
                Decrement(Local0)
                Store(DerefOf(Index(IBUF, Local0, )), Local1)
                If(LOr(LEqual(Local1, 0x3b), LEqual(Local1, 0x2a)))
                {
                    Store(0x0, Index(IBUF, Local0, ))
                    Store(Local0, ILEN)
                }
                Else
                {
                    Store(SizeOf(Arg0), ILEN)
                }
                Return(0x0)
            }
            Method(SCMP, 0x3, NotSerialized)
            {
                Store(SizeOf(Arg0), Local0)
                If(LEqual(Local0, 0x0))
                {
                    Return(0x0)
                }
                Increment(Local0)
                Name(STR1, Buffer(Local0)
                {
                })
                Store(Arg0, STR1)
                Decrement(Local0)
                Store(0x0, Local1)
                Store(Arg2, Local2)
                While(LLess(Local1, Local0))
                {
                    Store(DerefOf(Index(STR1, Local1, )), Local3)
                    Store(DerefOf(Index(Arg1, Local2, )), Local4)
                    If(LNot(LEqual(Local3, Local4)))
                    {
                        Return(0x0)
                    }
                    Increment(Local1)
                    Increment(Local2)
                }
                Store(DerefOf(Index(Arg1, Local2, )), Local4)
                If(LEqual(Local4, 0x0))
                {
                    Return(0x1)
                }
                If(LOr(LEqual(Local4, 0x2c), LEqual(Local4, 0x3a)))
                {
                    Return(0x1)
                }
                Return(0x0)
            }
            Method(GITM, 0x1, NotSerialized)
            {
                Store(0x0, Local0)
                Store(SizeOf(ITEM), Local1)
                While(LLess(Local0, Local1))
                {
                    Store(DerefOf(Index(DerefOf(Index(ITEM, Local0, )), 0x1, )), Local3)
                    If(SCMP(Local3, Arg0, 0x0))
                    {
                        Return(Local0)
                    }
                    Increment(Local0)
                }
                Return(Ones)
            }
            Method(GSEL, 0x3, NotSerialized)
            {
                Store(0x0, Local0)
                Store(SizeOf(Arg0), Local1)
                While(LLess(Local0, Local1))
                {
                    Store(DerefOf(Index(Arg0, Local0, )), Local2)
                    If(SCMP(Local2, Arg1, Arg2))
                    {
                        Return(Local0)
                    }
                    Increment(Local0)
                }
                Return(Ones)
            }
            Method(SLEN, 0x2, NotSerialized)
            {
                Store(DerefOf(Index(Arg0, Arg1, )), Local0)
                Return(SizeOf(Local0))
            }
            Method(CLRP, 0x0, NotSerialized)
            {
                Store(0x0, \WPAS)
                Store(0x0, \WPNW)
            }
            Method(GPAS, 0x2, NotSerialized)
            {
                Store(Arg1, Local0)
                Store(0x0, Local1)
                While(LNot(LGreater(Local1, 0x80)))
                {
                    Store(DerefOf(Index(Arg0, Local0, )), Local2)
                    If(LOr(LEqual(Local2, 0x2c), LEqual(Local2, 0x0)))
                    {
                        Store(0x0, Index(PSTR, Local1, ))
                        Return(Local1)
                    }
                    Store(Local2, Index(PSTR, Local1, ))
                    Increment(Local0)
                    Increment(Local1)
                }
                Store(0x0, Index(PSTR, Local1, ))
                Return(Ones)
            }
            Method(CPAS, 0x2, NotSerialized)
            {
                CLRP()
                Store(Arg1, Local0)
                Store(GPAS(Arg0, Local0), Local1)
                If(LEqual(Local1, Ones))
                {
                    Return(0x2)
                }
                If(LEqual(Local1, 0x0))
                {
                    Return(0x2)
                }
                Store(PSTR, \WPAS)
                Add(Local0, Local1, Local0)
                Increment(Local0)
                Store(GSEL(PENC, Arg0, Local0), Local6)
                If(LEqual(Local6, Ones))
                {
                    Return(0x2)
                }
                Store(Local6, \WENC)
                If(LEqual(Local6, 0x0))
                {
                    Add(Local0, SLEN(PENC, 0x0), Local0)
                    If(LNot(LEqual(DerefOf(Index(Arg0, Local0, )), 0x2c)))
                    {
                        Return(0x2)
                    }
                    Increment(Local0)
                    Store(GSEL(PKBD, Arg0, Local0), Local6)
                    If(LEqual(Local6, Ones))
                    {
                        Return(0x2)
                    }
                    Store(Local6, \WKBD)
                }
                Return(0x0)
            }
            Method(SPAS, 0x1, NotSerialized)
            {
                CLRP()
                Store(GSEL(PTYP, Arg0, 0x0), Local6)
                If(LEqual(Local6, Ones))
                {
                    Return(0x2)
                }
                Store(Local6, \WPTY)
                Store(SLEN(PTYP, Local6), Local0)
                If(LNot(LEqual(DerefOf(Index(Arg0, Local0, )), 0x2c)))
                {
                    Return(0x2)
                }
                Increment(Local0)
                Store(GPAS(Arg0, Local0), Local1)
                If(LOr(LEqual(Local1, Ones), LEqual(Local1, 0x0)))
                {
                    Return(0x2)
                }
                Store(PSTR, \WPAS)
                Add(Local0, Local1, Local0)
                If(LNot(LEqual(DerefOf(Index(Arg0, Local0, )), 0x2c)))
                {
                    Return(0x2)
                }
                Increment(Local0)
                Store(GPAS(Arg0, Local0), Local1)
                If(LEqual(Local1, Ones))
                {
                    Return(0x2)
                }
                If(LEqual(Local1, 0x0))
                {
                    Store(0x0, PSTR)
                }
                Store(PSTR, \WPNW)
                Add(Local0, Local1, Local0)
                Increment(Local0)
                Store(GSEL(PENC, Arg0, Local0), Local6)
                If(LEqual(Local6, Ones))
                {
                    Return(0x2)
                }
                Store(Local6, \WENC)
                If(LEqual(Local6, 0x0))
                {
                    Add(Local0, SLEN(PENC, 0x0), Local0)
                    If(LNot(LEqual(DerefOf(Index(Arg0, Local0, )), 0x2c)))
                    {
                        Return(0x2)
                    }
                    Increment(Local0)
                    Store(GSEL(PKBD, Arg0, Local0), Local6)
                    If(LEqual(Local6, Ones))
                    {
                        Return(0x2)
                    }
                    Store(Local6, \WKBD)
                }
                Return(0x0)
            }
            Method(WSET, 0x0, NotSerialized)
            {
                Store(ILEN, Local0)
                Increment(Local0)
                Store(GITM(IBUF), Local1)
                If(LEqual(Local1, Ones))
                {
                    Return(0x2)
                }
                Store(Local1, \WITM)
                Store(DerefOf(Index(ITEM, Local1, )), Local3)
                Store(DerefOf(Index(Local3, 0x1, )), Local4)
                Store(SizeOf(Local4), Local2)
                Increment(Local2)
                Store(DerefOf(Index(Local3, 0x0, )), Local4)
                If(LLess(Local4, 0x64))
                {
                    Store(DerefOf(Index(VSEL, Local4, )), Local5)
                    Store(GSEL(Local5, IBUF, Local2), Local6)
                    If(LEqual(Local6, Ones))
                    {
                        Return(0x2)
                    }
                    Store(Local6, \WSEL)
                    Add(Local2, SLEN(Local5, Local6), Local2)
                    Store(DerefOf(Index(IBUF, Local2, )), Local4)
                }
                Else
                {
                    Store(0x3f, \WLS0)
                    Store(0x3f, \WLS1)
                    Store(0x3f, \WLS2)
                    Store(0x3f, \WLS3)
                    Store(0x3f, \WLS4)
                    Store(0x3f, \WLS5)
                    Store(0x3f, \WLS6)
                    Store(0x3f, \WLS7)
                    Store(GSEL(VLST, IBUF, Local2), Local6)
                    If(LEqual(Local6, Ones))
                    {
                        Return(0x2)
                    }
                    Store(Local6, \WLS0)
                    Add(Local2, SLEN(VLST, Local6), Local2)
                    Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS1)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS2)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS3)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS4)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS5)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS6)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                    If(LAnd(LLess(Local2, Local0), LEqual(Local4, 0x3a)))
                    {
                        Increment(Local2)
                        Store(GSEL(VLST, IBUF, Local2), Local6)
                        If(LEqual(Local6, Ones))
                        {
                            Return(0x2)
                        }
                        Store(Local6, \WLS7)
                        Add(Local2, SLEN(VLST, Local6), Local2)
                        Store(DerefOf(Index(IBUF, Local2, )), Local4)
                    }
                }
                If(LAnd(LEqual(Local4, 0x2c), LLess(Local2, Local0)))
                {
                    Increment(Local2)
                    Store(CPAS(IBUF, Local2), Local0)
                    If(LNot(LEqual(Local0, 0x0)))
                    {
                        Return(Local0)
                    }
                }
                Return(\WMIS(0x1, 0x0))
            }
            Name(WQBA, Buffer(0x7c1)
            {
	0x46, 0x4f, 0x4d, 0x42, 0x01, 0x00, 0x00, 0x00, 0xb1, 0x07, 0x00, 0x00,
	0x5a, 0x30, 0x00, 0x00, 0x44, 0x53, 0x00, 0x01, 0x1a, 0x7d, 0xda, 0x54,
	0x98, 0x51, 0x97, 0x00, 0x01, 0x06, 0x18, 0x42, 0x10, 0x11, 0x10, 0x0a,
	0x0d, 0x21, 0x02, 0x0b, 0x83, 0x50, 0x4c, 0x18, 0x14, 0xa0, 0x45, 0x41,
	0xc8, 0x05, 0x14, 0x95, 0x02, 0x21, 0xc3, 0x02, 0x14, 0x0b, 0x70, 0x2e,
	0x40, 0xba, 0x00, 0xe5, 0x28, 0x72, 0x0c, 0x22, 0x02, 0xf7, 0xef, 0x0f,
	0x31, 0x0e, 0x88, 0x14, 0x40, 0x48, 0xe6, 0x28, 0x28, 0x81, 0x85, 0xc0,
	0x11, 0x82, 0x7e, 0x05, 0x20, 0x74, 0x88, 0x26, 0x83, 0x02, 0x9c, 0x22,
	0x08, 0xd2, 0x96, 0x05, 0xe8, 0x16, 0xe0, 0x5b, 0x80, 0x76, 0x08, 0xa1,
	0x55, 0x28, 0xc0, 0xa4, 0x00, 0x9f, 0x60, 0xb2, 0x28, 0x40, 0x36, 0x98,
	0x6c, 0xc3, 0x91, 0x61, 0x30, 0x91, 0x63, 0x40, 0x89, 0x19, 0x03, 0x4a,
	0xe7, 0x14, 0x64, 0x13, 0x58, 0xd0, 0x85, 0xa2, 0x68, 0x1a, 0x51, 0x12,
	0x1c, 0xd4, 0x31, 0x44, 0x08, 0x5e, 0xae, 0x00, 0xc9, 0x13, 0x90, 0xe6,
	0x79, 0xc9, 0xfa, 0x20, 0x34, 0x04, 0x36, 0x02, 0x1e, 0x45, 0x02, 0x08,
	0x8b, 0xb1, 0x4c, 0x89, 0x87, 0x41, 0x79, 0x00, 0x91, 0x9c, 0xa1, 0xa2,
	0x80, 0xed, 0x75, 0x22, 0x1a, 0xd6, 0x71, 0x32, 0x49, 0x70, 0xa8, 0x51,
	0x5a, 0xa2, 0x00, 0xf3, 0x23, 0xd3, 0x44, 0x8e, 0xad, 0xe9, 0x11, 0x0b,
	0x92, 0x49, 0x1b, 0x0a, 0x6a, 0xe8, 0x9e, 0xd6, 0x49, 0x79, 0xa2, 0x11,
	0x0f, 0xca, 0x30, 0x09, 0x3c, 0x0a, 0x86, 0xc6, 0x09, 0xca, 0x82, 0x90,
	0x83, 0x81, 0xa2, 0x00, 0x4f, 0xc2, 0x73, 0x2c, 0x5e, 0x80, 0xf0, 0x19,
	0x93, 0xa3, 0x40, 0x8c, 0x04, 0x3e, 0x12, 0x78, 0x34, 0xc7, 0x8c, 0x05,
	0x0a, 0x17, 0xf0, 0x7c, 0x8e, 0x21, 0x72, 0xdc, 0x43, 0x8d, 0x71, 0x14,
	0x91, 0x13, 0xbc, 0x03, 0x44, 0x31, 0x5a, 0x41, 0xf3, 0x16, 0x62, 0xb0,
	0x68, 0x06, 0xeb, 0x19, 0x9c, 0x0c, 0x3a, 0xc1, 0xff, 0xff, 0x08, 0xb8,
	0x0c, 0x08, 0x79, 0x14, 0x60, 0x75, 0x50, 0x9a, 0x86, 0x09, 0xba, 0x17,
	0x60, 0x4d, 0x80, 0x31, 0x01, 0x1a, 0x31, 0xa4, 0x4c, 0x80, 0xb3, 0xfb,
	0x82, 0x66, 0xd4, 0x96, 0x00, 0x73, 0x02, 0xb4, 0x09, 0xf0, 0x86, 0x20,
	0x94, 0xf3, 0x8c, 0x72, 0x2c, 0xa7, 0x18, 0xe5, 0x61, 0x20, 0xe6, 0xcb,
	0x40, 0xd0, 0x28, 0x31, 0x62, 0x9e, 0x4b, 0x5c, 0xc3, 0x46, 0x88, 0x11,
	0xf2, 0x14, 0x02, 0xc5, 0x6d, 0x7f, 0x10, 0x64, 0xd0, 0xb8, 0xd1, 0xfb,
	0xb4, 0x70, 0x56, 0x27, 0x70, 0xf4, 0x4f, 0x0a, 0x26, 0xf0, 0x94, 0x0f,
	0xec, 0xd9, 0xe0, 0x04, 0x8e, 0x35, 0x6a, 0x8c, 0x53, 0x49, 0xe0, 0xd8,
	0x0f, 0x08, 0x69, 0x00, 0x51, 0x24, 0x78, 0xd4, 0x69, 0xc1, 0xe7, 0x02,
	0x0f, 0xed, 0xa0, 0x3d, 0xc7, 0x13, 0x08, 0x72, 0x08, 0x47, 0xf0, 0xc4,
	0xf0, 0x40, 0xe0, 0x31, 0xb0, 0x9b, 0x82, 0x8f, 0x00, 0x3e, 0x21, 0xe0,
	0x5d, 0x03, 0xea, 0x6a, 0xf0, 0x60, 0xc0, 0x06, 0x1d, 0x0e, 0x33, 0x5e,
	0x0f, 0x3f, 0xdc, 0x09, 0x9c, 0xe4, 0x03, 0x06, 0x3f, 0x6c, 0x78, 0x70,
	0xb8, 0x79, 0x9e, 0xcc, 0x91, 0x95, 0x2a, 0xc0, 0xec, 0xe1, 0x40, 0x07,
	0x09, 0x9f, 0x36, 0xd8, 0x19, 0x00, 0x23, 0x7f, 0x10, 0xa8, 0x91, 0x19,
	0xda, 0xe3, 0x7e, 0xe9, 0x30, 0xe4, 0x73, 0xc2, 0x61, 0x31, 0xb1, 0xa7,
	0x0e, 0x3a, 0x1e, 0xf0, 0x5f, 0x46, 0x9e, 0x33, 0x3c, 0x7d, 0xcf, 0xd7,
	0x04, 0xc3, 0x0e, 0x1c, 0x3d, 0x10, 0x43, 0x3f, 0x6c, 0x1c, 0xc6, 0x69,
	0xf8, 0xfe, 0xe1, 0xf3, 0x02, 0x8c, 0x53, 0x80, 0x47, 0xee, 0xff, 0xff,
	0x21, 0xc5, 0xa7, 0x09, 0x7e, 0xb4, 0xf0, 0x69, 0x82, 0x5d, 0x0f, 0x4e,
	0xe3, 0x39, 0xc0, 0xc3, 0x39, 0x2b, 0x1f, 0x26, 0xc0, 0x76, 0x3f, 0x61,
	0x23, 0x7a, 0xb7, 0xf0, 0x68, 0xb0, 0xa7, 0x00, 0xf0, 0x9d, 0x5f, 0xc0,
	0x79, 0xd7, 0x60, 0x83, 0x85, 0x71, 0x7e, 0x01, 0x1e, 0x27, 0x04, 0x0f,
	0x81, 0x1f, 0x24, 0x3c, 0x04, 0x3e, 0x80, 0xe7, 0x8f, 0x33, 0xb4, 0xd2,
	0x79, 0x21, 0x07, 0x06, 0xef, 0x9c, 0x03, 0x63, 0x14, 0x3c, 0xcf, 0x63,
	0xc3, 0x04, 0x0a, 0xf2, 0x1a, 0x50, 0xa8, 0x67, 0x01, 0x85, 0xf1, 0xa9,
	0x06, 0x78, 0xfd, 0xff, 0x4f, 0x35, 0xc0, 0xe5, 0x70, 0x80, 0x3b, 0x39,
	0xc0, 0xbd, 0x17, 0xb0, 0x8b, 0xc3, 0x73, 0x0d, 0x5c, 0xd1, 0xe7, 0x1a,
	0xa8, 0xf7, 0x96, 0xe2, 0xc6, 0xa8, 0x6b, 0x4c, 0x90, 0x47, 0x81, 0x47,
	0x9a, 0x28, 0xcf, 0x33, 0xef, 0x32, 0x11, 0x9e, 0x6d, 0x7c, 0xad, 0xf1,
	0x14, 0xe2, 0xf8, 0x5a, 0x63, 0xc4, 0x97, 0x89, 0x77, 0x1b, 0xe3, 0x1e,
	0xdc, 0x63, 0xcd, 0x43, 0x8e, 0x41, 0x8e, 0x26, 0xc2, 0x8b, 0x41, 0xc0,
	0xc7, 0x1b, 0x1f, 0x6b, 0xc0, 0x2b, 0xe6, 0x85, 0x22, 0x0b, 0xc7, 0x1a,
	0x40, 0xe3, 0xff, 0xff, 0x58, 0x03, 0xdc, 0xb0, 0x1e, 0x50, 0xc0, 0x77,
	0x64, 0x60, 0x37, 0x14, 0x78, 0x27, 0x14, 0xc0, 0x4f, 0xe2, 0x17, 0x80,
	0x8e, 0x1c, 0x4e, 0x0b, 0x22, 0x1b, 0x6f, 0x00, 0x9f, 0x02, 0xa8, 0x1a,
	0x20, 0x4d, 0x13, 0x36, 0xc1, 0xf4, 0xe4, 0x82, 0xf7, 0x91, 0xc0, 0xb9,
	0x49, 0x94, 0x7c, 0x58, 0x14, 0xce, 0x59, 0x0f, 0x22, 0x14, 0xc4, 0x80,
	0x0e, 0x72, 0x9c, 0x40, 0x9f, 0x51, 0x7c, 0x10, 0x39, 0xd1, 0x27, 0x42,
	0x0f, 0xca, 0xc3, 0x78, 0x47, 0x61, 0x27, 0x10, 0x1f, 0x26, 0x3c, 0x76,
	0x1f, 0x13, 0xf8, 0x3f, 0xc6, 0xb3, 0x31, 0xba, 0xd5, 0x60, 0xe8, 0xff,
	0x7f, 0x4e, 0xe1, 0x60, 0x3e, 0x88, 0x70, 0x82, 0x8f, 0x46, 0xdd, 0x24,
	0x40, 0xa5, 0xef, 0xa8, 0x00, 0x0a, 0x20, 0xdf, 0x0b, 0x7c, 0x0e, 0x78,
	0x36, 0x60, 0x63, 0x78, 0x14, 0x30, 0x9a, 0xd1, 0x79, 0xf8, 0xc9, 0xa2,
	0xe2, 0x4e, 0x96, 0x82, 0x78, 0xb2, 0x8e, 0x32, 0x59, 0xf4, 0x4c, 0x7c,
	0xaf, 0xf0, 0x8c, 0xde, 0xb4, 0x3c, 0x47, 0x4f, 0xd8, 0xf7, 0x10, 0x58,
	0x87, 0x81, 0x90, 0x0f, 0x06, 0x9e, 0x86, 0xe1, 0x3c, 0x59, 0x0e, 0xe7,
	0xc9, 0xf2, 0xb1, 0xf8, 0x1a, 0x02, 0x3e, 0x81, 0xb3, 0x05, 0x39, 0x3c,
	0x26, 0xd6, 0xa8, 0xe8, 0x55, 0xc8, 0xc3, 0xe3, 0x97, 0x03, 0xcf, 0xe7,
	0x19, 0xe1, 0x28, 0x9f, 0x24, 0x70, 0x18, 0xcf, 0x24, 0x1e, 0xa2, 0x6f,
	0x45, 0xb0, 0x26, 0x72, 0xd2, 0xbe, 0x2d, 0x9c, 0x6c, 0xd0, 0xd7, 0x33,
	0xcc, 0xad, 0x08, 0xf6, 0xff, 0xff, 0x56, 0x04, 0xe7, 0x82, 0x06, 0x33,
	0xd3, 0xbd, 0x0a, 0x15, 0xeb, 0x5e, 0x05, 0x88, 0x1d, 0xd6, 0x6b, 0x8f,
	0x0f, 0x56, 0x70, 0xef, 0x55, 0x70, 0x2f, 0x55, 0xcf, 0x0a, 0xc7, 0x18,
	0xfe, 0x61, 0x2a, 0xc6, 0x29, 0xbd, 0x76, 0x1a, 0x28, 0x4c, 0x94, 0x78,
	0xef, 0x55, 0x1e, 0xe3, 0x7b, 0x15, 0xbb, 0x42, 0x85, 0x89, 0xf5, 0x72,
	0x65, 0xd4, 0xd7, 0x89, 0x70, 0x81, 0x82, 0x44, 0x7a, 0xb5, 0x8a, 0x12,
	0x39, 0xbe, 0x21, 0xdf, 0xab, 0xc0, 0x2b, 0xe7, 0x5e, 0x05, 0xb2, 0xff,
	0xff, 0xbd, 0x0a, 0x30, 0x8f, 0xf6, 0x5e, 0x05, 0xc6, 0x6b, 0x03, 0xbb,
	0x21, 0xc1, 0x02, 0x7a, 0xb1, 0x02, 0x0c, 0x65, 0xbe, 0x58, 0xd1, 0xbc,
	0x17, 0x2b, 0xc4, 0xff, 0xff, 0x5c, 0xc2, 0xf4, 0x5c, 0xac, 0xc8, 0x3c,
	0xe1, 0xdf, 0xac, 0x00, 0x4e, 0xff, 0xff, 0x6f, 0x56, 0x80, 0xb1, 0x7b,
	0x11, 0xe6, 0x68, 0x05, 0x2f, 0xe5, 0xcd, 0x8a, 0xc6, 0x59, 0x86, 0x02,
	0x2e, 0x88, 0xc2, 0xf8, 0x66, 0x05, 0x38, 0xba, 0xae, 0xe0, 0x86, 0x0c,
	0x17, 0x2c, 0x4a, 0x30, 0x1f, 0x42, 0x3c, 0x9d, 0x23, 0x7e, 0x48, 0x78,
	0x09, 0x78, 0xcc, 0xf1, 0x80, 0x1f, 0x08, 0x7c, 0xb9, 0x02, 0xd3, 0xff,
	0x9f, 0xc0, 0x27, 0xdf, 0xb3, 0x7c, 0x9b, 0x7a, 0xef, 0xe5, 0x07, 0xac,
	0xf7, 0x2a, 0x1f, 0x7e, 0x63, 0xbd, 0x33, 0xbc, 0x5c, 0x79, 0x24, 0x51,
	0x4e, 0x22, 0x94, 0xef, 0x56, 0xef, 0x55, 0x46, 0x89, 0xf8, 0x42, 0xec,
	0x53, 0xb0, 0xa1, 0x8d, 0xf2, 0x54, 0x11, 0xdd, 0x78, 0x2f, 0x57, 0xe0,
	0x95, 0x74, 0xb9, 0x02, 0x68, 0x32, 0xfc, 0x97, 0x2b, 0xf0, 0xdd, 0x1c,
	0xb0, 0xd7, 0x24, 0x38, 0xff, 0xff, 0x6b, 0x12, 0xbf, 0x5e, 0x01, 0x7e,
	0xb2, 0x5f, 0xaf, 0x68, 0xee, 0xeb, 0x15, 0x4a, 0x14, 0x84, 0x14, 0x01,
	0x69, 0xa6, 0xe0, 0xb9, 0x5f, 0x01, 0x9c, 0xf8, 0xff, 0xdf, 0xaf, 0x00,
	0xcb, 0xe1, 0xee, 0x57, 0x40, 0xef, 0x76, 0x04, 0x5e, 0x94, 0xb7, 0x23,
	0xec, 0x15, 0x0b, 0x9f, 0xf1, 0x8a, 0x45, 0xc3, 0xac, 0x44, 0xf1, 0xd6,
	0x44, 0x61, 0x7c, 0xc5, 0x02, 0x26, 0xff, 0xff, 0x2b, 0x16, 0x30, 0x3b,
	0x88, 0xe2, 0x46, 0x0d, 0xf7, 0xe2, 0xe4, 0x5b, 0x8f, 0xe7, 0x1b, 0xd1,
	0x77, 0x18, 0xcc, 0x09, 0x0b, 0xc6, 0x0d, 0x0b, 0xfe, 0x90, 0x1e, 0x86,
	0x7d, 0x92, 0x78, 0xc7, 0xf2, 0xd1, 0xca, 0x20, 0x6f, 0xc0, 0x4f, 0x56,
	0x0f, 0x56, 0x51, 0x8c, 0x10, 0xf0, 0x78, 0xde, 0x85, 0x7d, 0xb4, 0x7a,
	0xd3, 0x32, 0x4a, 0xec, 0x58, 0xbe, 0x50, 0x3d, 0x6b, 0xf9, 0x9a, 0x65,
	0x88, 0xb8, 0x0f, 0xc4, 0xbe, 0x61, 0x01, 0xb6, 0xff, 0xff, 0x37, 0x2c,
	0xc0, 0xd1, 0xc5, 0x81, 0x1f, 0x1c, 0xb0, 0x37, 0x2c, 0xc0, 0xe7, 0x4c,
	0xc1, 0x73, 0xc3, 0x02, 0x36, 0xff, 0xff, 0x1b, 0x16, 0xc0, 0xff, 0xff,
	0xff, 0x0d, 0x0b, 0x38, 0xdc, 0xae, 0xb0, 0xb7, 0x2c, 0xec, 0xed, 0x85,
	0xac, 0x82, 0x86, 0x5a, 0x89, 0x82, 0x7f, 0xaf, 0x0c, 0x43, 0x6f, 0x58,
	0x80, 0xa3, 0x71, 0x7b, 0xd4, 0xe0, 0x38, 0x1b, 0x3c, 0x49, 0x60, 0xce,
	0xd5, 0xb8, 0xd9, 0x1c, 0x5c, 0xe0, 0x08, 0xbd, 0x83, 0x6a, 0xee, 0xec,
	0x92, 0x02, 0xe3, 0x96, 0x05, 0xf7, 0x52, 0xf5, 0xd0, 0x10, 0xe5, 0x20,
	0x5e, 0x85, 0x1f, 0xac, 0x1e, 0xa5, 0x8e, 0xec, 0xf1, 0xea, 0x69, 0xd8,
	0xc7, 0x2c, 0xdf, 0xb2, 0x0c, 0x15, 0xe1, 0x2d, 0x8b, 0x9d, 0x21, 0xe2,
	0xc5, 0x8a, 0x12, 0xe2, 0xbd, 0x22, 0xb4, 0xef, 0x5c, 0x06, 0x7f, 0x34,
	0x36, 0x6a, 0xd0, 0x97, 0xe3, 0xb7, 0x2c, 0x78, 0xff, 0xff, 0x5b, 0x16,
	0x7c, 0x91, 0x7f, 0x15, 0x9d, 0x08, 0x7c, 0xcb, 0x02, 0xf8, 0x11, 0x0c,
	0x42, 0x4e, 0x06, 0x8e, 0x3e, 0x2f, 0xe0, 0x07, 0xf0, 0x30, 0xe2, 0x21,
	0xb1, 0x00, 0x03, 0xa7, 0xf7, 0x25, 0x9f, 0x29, 0xf8, 0x01, 0xc3, 0x67,
	0x0a, 0x76, 0x3d, 0x88, 0xfe, 0x18, 0xe0, 0x73, 0x09, 0x66, 0x70, 0xe0,
	0xbf, 0x56, 0x1c, 0xba, 0x47, 0xf1, 0xfa, 0x60, 0x02, 0x0f, 0x8e, 0xff,
	0xff, 0x07, 0x07, 0xf7, 0xce, 0x70, 0x44, 0xbe, 0xc3, 0x78, 0x70, 0x60,
	0x3b, 0x08, 0x00, 0x87, 0xc1, 0xe1, 0x43, 0x0d, 0x0e, 0x3d, 0x1e, 0x03,
	0x87, 0xf4, 0x79, 0x8c, 0x5d, 0x18, 0x1e, 0x72, 0x3c, 0x34, 0xb0, 0x01,
	0x7a, 0x68, 0xc0, 0x72, 0x12, 0x4f, 0x21, 0x87, 0x06, 0x66, 0x09, 0x43,
	0x03, 0x4a, 0xf1, 0x86, 0x46, 0xff, 0xff, 0x43, 0xe3, 0x43, 0xf2, 0x61,
	0x21, 0xe6, 0x53, 0x4e, 0x84, 0xf7, 0x05, 0x9f, 0xa0, 0x18, 0xfa, 0x6b,
	0x8a, 0x6f, 0x17, 0xbe, 0x09, 0xe2, 0xc6, 0x07, 0xae, 0x4b, 0xa7, 0xc7,
	0x07, 0x7c, 0x8e, 0x5c, 0x1e, 0x1f, 0xee, 0xe8, 0xe4, 0xf1, 0xc1, 0x70,
	0x79, 0x95, 0x21, 0x47, 0x13, 0x1f, 0xad, 0xd8, 0xf0, 0xc0, 0x76, 0xd3,
	0xf3, 0xf0, 0x80, 0xcf, 0x75, 0x13, 0x8c, 0x57, 0x48, 0x7e, 0x2d, 0x81,
	0x71, 0x82, 0xc2, 0x5f, 0x37, 0xc1, 0xfb, 0xff, 0xbf, 0x6e, 0x02, 0xcf,
	0x51, 0x70, 0xad, 0x97, 0x6c, 0x1a, 0xe4, 0x95, 0xa3, 0x58, 0x2f, 0x02,
	0x0a, 0xe3, 0x33, 0x1b, 0xe0, 0x68, 0xac, 0xcf, 0x6c, 0x60, 0xb9, 0x17,
	0xb0, 0x1b, 0x1b, 0xdc, 0xd3, 0x1a, 0xec, 0xbb, 0xc3, 0xc3, 0xd9, 0x63,
	0xda, 0xa3, 0xda, 0x03, 0x9a, 0x8f, 0xd8, 0x31, 0xde, 0xd2, 0x82, 0xc4,
	0x89, 0xf0, 0x3a, 0xf0, 0xb4, 0xe6, 0x4b, 0x46, 0xbc, 0x40, 0x4f, 0x6b,
	0xc6, 0x88, 0xf3, 0xd2, 0x66, 0xc4, 0x57, 0x8a, 0x10, 0x0f, 0x6b, 0x3e,
	0xb9, 0x19, 0xef, 0x61, 0x22, 0x5c, 0x98, 0x17, 0xb6, 0xa7, 0x35, 0x70,
	0xfc, 0xff, 0x4f, 0x6b, 0x70, 0xe4, 0x5c, 0xb1, 0x01, 0x9a, 0x5c, 0xf4,
	0x71, 0x87, 0x14, 0xb0, 0x5c, 0x1b, 0xd8, 0x2d, 0x05, 0xde, 0x05, 0x1b,
	0x38, 0xff, 0xff, 0x8f, 0x28, 0xe0, 0xcb, 0x72, 0xc1, 0xa6, 0x39, 0x2e,
	0xd8, 0x28, 0x0e, 0xab, 0x01, 0xd2, 0x3c, 0xe1, 0x5f, 0xaf, 0xc1, 0x3f,
	0x09, 0x5f, 0xaf, 0x01, 0xdb, 0xb7, 0x58, 0xdc, 0xf5, 0x1a, 0x58, 0xfd,
	0xff, 0xaf, 0xd7, 0xc0, 0x52, 0xf0, 0x48, 0xe9, 0x9d, 0x1a, 0x5c, 0x37,
	0x6d, 0x3c, 0xe8, 0x9b, 0x36, 0x4c, 0x85, 0x36, 0x7d, 0x6a, 0x34, 0x6a,
	0xd5, 0xa0, 0x4c, 0x8d, 0x32, 0x0d, 0x6a, 0xf5, 0xa9, 0xd4, 0x98, 0xb1,
	0xa1, 0x5a, 0xda, 0x5d, 0x82, 0x8a, 0x59, 0x8c, 0x46, 0xe3, 0x28, 0x20,
	0x54, 0xf6, 0x1f, 0x50, 0x20, 0x0e, 0xf9, 0xd1, 0x11, 0xa0, 0x83, 0x7d,
	0xa7, 0x74, 0x0b, 0x27, 0x6b, 0x13, 0x88, 0xe3, 0x9b, 0x80, 0x68, 0x04,
	0x44, 0x5a, 0x54, 0x00, 0xb1, 0xdc, 0x20, 0x02, 0xb2, 0x8a, 0x35, 0x09,
	0xc8, 0x9a, 0xbf, 0x2f, 0x02, 0xb7, 0x4e, 0x1d, 0x40, 0x2c, 0x25, 0x08,
	0x4d, 0xb4, 0x70, 0x81, 0x3a, 0x1e, 0x88, 0x06, 0x43, 0x68, 0x04, 0xe4,
	0x60, 0x14, 0x02, 0xb2, 0x8c, 0xcf, 0x9d, 0xc0, 0x2d, 0xc0, 0x0a, 0x10,
	0x93, 0x0f, 0x42, 0x05, 0x7b, 0x01, 0x65, 0xea, 0x41, 0x04, 0x64, 0xa5,
	0x6b, 0x15, 0x90, 0x75, 0x83, 0x08, 0xc8, 0x59, 0xcd, 0x80, 0xb3, 0x8c,
	0x6e, 0x80, 0x98, 0xc2, 0x87, 0x82, 0x40, 0xac, 0x49, 0x0f, 0x28, 0x13,
	0x08, 0xa2, 0x0b, 0x07, 0xf1, 0x03, 0xc4, 0xa4, 0x81, 0x08, 0xc8, 0x71,
	0x7e, 0x25, 0x02, 0x77, 0x1c, 0x45, 0x80, 0xd4, 0xd1, 0x70, 0x29, 0x08,
	0x15, 0xff, 0x09, 0x13, 0xc8, 0xff, 0xff, 0xfd, 0x44, 0x96, 0xc0, 0x28,
	0x08, 0x8d, 0xa0, 0x09, 0x84, 0xc9, 0xf3, 0x04, 0xc2, 0x42, 0xfd, 0xfd,
	0x34, 0x04, 0x07, 0x51, 0x35, 0x44, 0xea, 0x0a, 0x84, 0x05, 0x7e, 0x18,
	0x68, 0x30, 0x4e, 0x0f, 0x22, 0x20, 0x27, 0x7d, 0x52, 0x05, 0x22, 0xb9,
	0x41, 0x04, 0xe4, 0xff, 0x3f
            })
        }
        Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
        {
            Mutex(BFWM, 0x7)
            Method(MHCF, 0x1, NotSerialized)
            {
                Store(\BFWC(Arg0), Local0)
                Return(Local0)
            }
            Method(MHPF, 0x1, NotSerialized)
            {
                Name(RETB, Buffer(0x25)
                {
                })
                Acquire(BFWM, 0xffff)
                If(LNot(LGreater(SizeOf(Arg0), 0x25)))
                {
                    Store(Arg0, \BFWB)
                    If(\BFWP())
                    {
                        \_SB_.PCI0.LPC_.EC__.CHKS()
                        \BFWL()
                    }
                    Store(\BFWB, RETB)
                }
                Release(BFWM)
                Return(RETB)
            }
            Method(MHIF, 0x1, NotSerialized)
            {
                Name(RETB, Buffer(0xa)
                {
                })
                Acquire(BFWM, 0xffff)
                \BFWG(Arg0)
                Store(\BFWB, RETB)
                Release(BFWM)
                Return(RETB)
            }
            Method(MHDM, 0x1, NotSerialized)
            {
                \BDMC(Arg0)
            }
        }
    }
    Scope(\_SB_.PCI0.EHC2.URTH.URMH.PRTC)
    {
        Name(_EJD, "_SB.GDCK")
    }
    Name(\_S0_, Package(0x4)
    {
        0x0,
        0x0,
        0x0,
        0x0
    })
    Name(\_S3_, Package(0x4)
    {
        0x5,
        0x5,
        0x0,
        0x0
    })
    Name(\_S4_, Package(0x4)
    {
        0x6,
        0x6,
        0x0,
        0x0
    })
    Name(\_S5_, Package(0x4)
    {
        0x7,
        0x7,
        0x0,
        0x0
    })
    Method(\_PTS, 0x1, NotSerialized)
    {
        Store(0x1, Local0)
        If(LEqual(Arg0, \SPS_))
        {
            Store(0x0, Local0)
        }
        If(LOr(LEqual(Arg0, 0x0), LNot(LLess(Arg0, 0x6))))
        {
            Store(0x0, Local0)
        }
        If(Local0)
        {
            Store(Arg0, \SPS_)
            \_SB_.PCI0.LPC_.EC__.HKEY.MHKE(0x0)
            If(\_SB_.PCI0.LPC_.EC__.KBLT)
            {
                \UCMS(0xd)
            }
            If(LEqual(Arg0, 0x1))
            {
                Store(\_SB_.PCI0.LPC_.EC__.HFNI, \FNID)
                Store(0x0, \_SB_.PCI0.LPC_.EC__.HFNI)
                Store(0x0, \_SB_.PCI0.LPC_.EC__.HFSP)
            }
            If(LEqual(Arg0, 0x3))
            {
                \VVPD(0x3)
                \TRAP()
                Store(\_SB_.PCI0.LPC_.EC__.AC__._PSR(), \ACST)
            }
            If(LEqual(Arg0, 0x4))
            {
                And(\_SB_.PCI0.LPC_.GL03, 0xef, Local0)
                Store(Local0, \_SB_.PCI0.LPC_.GL03)
                \_SB_.SLPB._PSW(0x0)
                If(And(\PPMF, 0x1, ))
                {
                    \STEP(0x7)
                }
                \TRAP()
                TPHY(0x2)
                \AWON(0x4)
            }
            If(LEqual(Arg0, 0x5))
            {
                And(\_SB_.PCI0.LPC_.GL03, 0xef, Local0)
                Store(Local0, \_SB_.PCI0.LPC_.GL03)
                \TRAP()
                TPHY(0x2)
                \AWON(0x5)
            }
            \_SB_.PCI0.LPC_.EC__.BPTS(Arg0)
            If(LNot(LLess(Arg0, 0x4)))
            {
                Store(0x0, \_SB_.PCI0.LPC_.EC__.HWLB)
            }
            Else
            {
                Store(0x1, \_SB_.PCI0.LPC_.EC__.HWLB)
            }
            If(LNot(LEqual(Arg0, 0x5)))
            {
                Store(0x1, \_SB_.PCI0.LPC_.EC__.HCMU)
                \_SB_.GDCK.GPTS(Arg0)
                Store(\_SB_.PCI0.EXP4.PDS_, \_SB_.PCI0.EXP4.PDSF)
            }
            \_SB_.PCI0.LPC_.EC__.HKEY.WGPS(Arg0)
        }
    }
    Name(WAKI, Package(0x2)
    {
        0x0,
        0x0
    })
    Method(\_WAK, 0x1, NotSerialized)
    {
        If(LOr(LEqual(Arg0, 0x0), LNot(LLess(Arg0, 0x5))))
        {
            Return(WAKI)
        }
        Store(0x0, \SPS_)
        Store(0x0, \_SB_.PCI0.LPC_.EC__.HCMU)
        \_SB_.PCI0.LPC_.EC__.EVNT(0x1)
        \_SB_.PCI0.LPC_.EC__.HKEY.MHKE(0x1)
        \_SB_.PCI0.LPC_.EC__.FNST()
        \UCMS(0xd)
        Store(0x0, \LIDB)
        If(LEqual(Arg0, 0x1))
        {
            Store(\_SB_.PCI0.LPC_.EC__.HFNI, \FNID)
        }
        If(LEqual(Arg0, 0x3))
        {
            Store(\_SB_.PCI0.LPC_.EC__.AC__._PSR(), \PWRS)
            If(\OSC4)
            {
                \PNTF(0x81)
            }
            If(LNot(LEqual(\ACST, \_SB_.PCI0.LPC_.EC__.AC__._PSR())))
            {
                \_SB_.PCI0.LPC_.EC__.ATMC()
            }
            If(\VIGD)
            {
                \_SB_.PCI0.VID_.GLIS(\_SB_.LID_._LID())
                Store(\_SB_.GDCK.GGID(), Local0)
                If(LOr(LEqual(Local0, 0x0), LEqual(Local0, 0x1)))
                {
                    \_SB_.PCI0.VID_.GDCS(0x1)
                }
                Else
                {
                    \_SB_.PCI0.VID_.GDCS(0x0)
                }
                If(\WVIS)
                {
                    \VBTD()
                }
            }
            \AWON(0x0)
            If(\CMPR)
            {
                Notify(\_SB_.SLPB, 0x2)
                Store(0x0, \CMPR)
            }
        }
        If(LEqual(Arg0, 0x4))
        {
            \NVSS(0x0)
            Store(0x0, \_SB_.PCI0.LPC_.EC__.HSPA)
            Store(\_SB_.PCI0.LPC_.EC__.AC__._PSR(), \PWRS)
            If(\OSC4)
            {
                \PNTF(0x81)
            }
            If(And(\PPMF, 0x1, ))
            {
                \STEP(0x8)
            }
            \_SB_.PCI0.LPC_.EC__.ATMC()
        }
        If(XOr(\_SB_.PCI0.EXP4.PDS_, \_SB_.PCI0.EXP4.PDSF, ))
        {
            Store(\_SB_.PCI0.EXP4.PDS_, \_SB_.PCI0.EXP4.PDSF)
            Notify(\_SB_.PCI0.EXP4, 0x0)
        }
        \_SB_.GDCK.GWAK(Arg0)
        \_SB_.PCI0.LPC_.EC__.BWAK(Arg0)
        \_SB_.PCI0.LPC_.EC__.HKEY.WGWK(Arg0)
        Notify(\_TZ_.THM0, 0x80)
        \VSLD(\_SB_.LID_._LID())
        If(\VIGD)
        {
            \_SB_.PCI0.VID_.GLIS(\_SB_.LID_._LID())
        }
        If(LLess(Arg0, 0x4))
        {
            If(And(\RRBF, 0x2, ))
            {
                ShiftLeft(Arg0, 0x8, Local0)
                Store(Or(0x2013, Local0, ), Local0)
                \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(Local0)
            }
        }
        If(LEqual(Arg0, 0x4))
        {
            Store(0x0, Local0)
            Store(\CSUM(0x0), Local1)
            If(LNot(LEqual(Local1, \CHKC)))
            {
                Store(0x1, Local0)
                Store(Local1, \CHKC)
            }
            Store(\CSUM(0x1), Local1)
            If(LNot(LEqual(Local1, \CHKE)))
            {
                Store(0x1, Local0)
                Store(Local1, \CHKE)
            }
            If(Local0)
            {
                Notify(\_SB_, 0x0)
            }
        }
        Store(Zero, \RRBF)
        Return(WAKI)
    }
    Scope(\_SI_)
    {
        Method(_SST, 0x1, NotSerialized)
        {
            If(LEqual(Arg0, 0x0))
            {
                \_SB_.PCI0.LPC_.EC__.LED_(0x0, 0x0)
                \_SB_.PCI0.LPC_.EC__.LED_(0x7, 0x0)
            }
            If(LEqual(Arg0, 0x1))
            {
                If(LOr(\SPS_, \WNTF))
                {
                    \_SB_.PCI0.LPC_.EC__.BEEP(0x5)
                }
                \_SB_.PCI0.LPC_.EC__.LED_(0x0, 0x80)
                \_SB_.PCI0.LPC_.EC__.LED_(0x7, 0x0)
            }
            If(LEqual(Arg0, 0x2))
            {
                \_SB_.PCI0.LPC_.EC__.LED_(0x0, 0x80)
                \_SB_.PCI0.LPC_.EC__.LED_(0x7, 0xc0)
            }
            If(LEqual(Arg0, 0x3))
            {
                If(LGreater(\SPS_, 0x3))
                {
                    \_SB_.PCI0.LPC_.EC__.BEEP(0x7)
                }
                Else
                {
                    If(LEqual(\SPS_, 0x3))
                    {
                        \_SB_.PCI0.LPC_.EC__.BEEP(0x3)
                        \_SB_.GDCK.PEJ3()
                    }
                    Else
                    {
                        \_SB_.PCI0.LPC_.EC__.BEEP(0x4)
                    }
                }
                If(LEqual(\SPS_, 0x3))
                {
                }
                Else
                {
                    \_SB_.PCI0.LPC_.EC__.LED_(0x0, 0x80)
                }
                \_SB_.PCI0.LPC_.EC__.LED_(0x7, 0xc0)
            }
            If(LEqual(Arg0, 0x4))
            {
                If(LEqual(\PJID, 0x0))
                {
                    \_SB_.PCI0.LPC_.EC__.HKEY.SVWG()
                }
                \_SB_.PCI0.LPC_.EC__.BEEP(0x3)
                \_SB_.PCI0.LPC_.EC__.LED_(0x7, 0xc0)
            }
        }
    }
    Scope(\_GPE)
    {
        Method(_L1D, 0x0, NotSerialized)
        {
            Store(\_SB_.PCI0.LPC_.EC__.HWAK, Local0)
            Store(Local0, \RRBF)
            Sleep(0xa)
            If(And(Local0, 0x2, ))
            {
            }
            If(And(Local0, 0x4, ))
            {
                Notify(\_SB_.LID_, 0x2)
            }
            If(And(Local0, 0x8, ))
            {
                \_SB_.GDCK.GGPE()
                Notify(\_SB_.SLPB, 0x2)
            }
            If(And(Local0, 0x10, ))
            {
                Notify(\_SB_.SLPB, 0x2)
            }
            If(And(Local0, 0x40, ))
            {
            }
            If(And(Local0, 0x80, ))
            {
                Notify(\_SB_.SLPB, 0x2)
            }
            If(LEqual(\PJID, 0x0))
            {
                If(And(Local0, 0x300, ))
                {
                    Notify(\_SB_.SLPB, 0x2)
                }
            }
        }
        Method(_L09, 0x0, NotSerialized)
        {
            If(\_SB_.PCI0.EXP1.PS__)
            {
                Store(0x1, \_SB_.PCI0.EXP1.PS__)
                Store(0x1, \_SB_.PCI0.EXP1.PMCS)
                Notify(\_SB_.PCI0.EXP1, 0x2)
            }
            If(\_SB_.PCI0.EXP2.PS__)
            {
                Store(0x1, \_SB_.PCI0.EXP2.PS__)
                Store(0x1, \_SB_.PCI0.EXP2.PMCS)
                Notify(\_SB_.PCI0.EXP2, 0x2)
            }
            If(\_SB_.PCI0.EXP3.PS__)
            {
                Store(0x1, \_SB_.PCI0.EXP3.PS__)
                Store(0x1, \_SB_.PCI0.EXP3.PMCS)
                Notify(\_SB_.PCI0.EXP3, 0x2)
            }
            If(\_SB_.PCI0.EXP4.PS__)
            {
                Store(0x1, \_SB_.PCI0.EXP4.PS__)
                Store(0x1, \_SB_.PCI0.EXP4.PMCS)
                Notify(\_SB_.PCI0.EXP4, 0x2)
            }
            If(\_SB_.PCI0.EXP5.PS__)
            {
                Store(0x1, \_SB_.PCI0.EXP5.PS__)
                Store(0x1, \_SB_.PCI0.EXP5.PMCS)
                Notify(\_SB_.PCI0.EXP5, 0x2)
            }
        }
        Method(_L01, 0x0, NotSerialized)
        {
            If(\_SB_.PCI0.EXP4.HPCS)
            {
                Sleep(0x64)
                Store(0x1, \_SB_.PCI0.EXP4.HPCS)
                If(\_SB_.PCI0.EXP4.PDC_)
                {
                    Store(0x1, \_SB_.PCI0.EXP4.PDC_)
                    Store(\_SB_.PCI0.EXP4.PDS_, \_SB_.PCI0.EXP4.PDSF)
                    Notify(\_SB_.PCI0.EXP4, 0x0)
                }
            }
        }
        Method(_L02, 0x0, NotSerialized)
        {
            Store(0x0, \_SB_.PCI0.LPC_.SWGE)
            If(LAnd(\CWUE, And(\SWGP, 0x2, )))
            {
                And(\SWGP, Not(0x2, ), \SWGP)
                If(\OSC4)
                {
                    \PNTF(0x81)
                }
            }
        }
        Method(_L06, 0x0, NotSerialized)
        {
            If(\_SB_.PCI0.VID_.GSSE)
            {
                \_SB_.PCI0.VID_.GSCI()
            }
            Else
            {
                Store(0x1, \_SB_.PCI0.LPC_.SCIS)
            }
        }
    }
    Scope(\_SB_.PCI0.LPC_.EC__.HKEY)
    {
        Method(MHQT, 0x1, NotSerialized)
        {
            If(LAnd(\WNTF, \TATC))
            {
                If(LEqual(Arg0, 0x0))
                {
                    Store(\TATC, Local0)
                    Return(Local0)
                }
                Else
                {
                    If(LEqual(Arg0, 0x1))
                    {
                        Store(\TDFA, Local0)
                        Add(Local0, ShiftLeft(\TDTA, 0x4, ), Local0)
                        Add(Local0, ShiftLeft(\TDFD, 0x8, ), Local0)
                        Add(Local0, ShiftLeft(\TDTD, 0xc, ), Local0)
                        Add(Local0, ShiftLeft(\TNFT, 0x10, ), Local0)
                        Add(Local0, ShiftLeft(\TNTT, 0x14, ), Local0)
                        Return(Local0)
                    }
                    Else
                    {
                        If(LEqual(Arg0, 0x2))
                        {
                            Store(\TCFA, Local0)
                            Add(Local0, ShiftLeft(\TCTA, 0x4, ), Local0)
                            Add(Local0, ShiftLeft(\TCFD, 0x8, ), Local0)
                            Add(Local0, ShiftLeft(\TCTD, 0xc, ), Local0)
                            Return(Local0)
                        }
                        Else
                        {
                            If(LEqual(Arg0, 0x3))
                            {
                            }
                            Else
                            {
                                If(LEqual(Arg0, 0x4))
                                {
                                    Store(\TATW, Local0)
                                    Return(Local0)
                                }
                                Else
                                {
                                    Noop
                                }
                            }
                        }
                    }
                }
            }
            Return(0x0)
        }
        Method(MHAT, 0x1, NotSerialized)
        {
            If(LAnd(\WNTF, \TATC))
            {
                Store(And(Arg0, 0xff, ), Local0)
                If(LNot(ATMV(Local0)))
                {
                    Return(0x0)
                }
                Store(And(ShiftRight(Arg0, 0x8, ), 0xff, ), Local0)
                If(LNot(ATMV(Local0)))
                {
                    Return(0x0)
                }
                Store(And(Arg0, 0xf, ), \TCFA)
                Store(And(ShiftRight(Arg0, 0x4, ), 0xf, ), \TCTA)
                Store(And(ShiftRight(Arg0, 0x8, ), 0xf, ), \TCFD)
                Store(And(ShiftRight(Arg0, 0xc, ), 0xf, ), \TCTD)
                ATMC()
                Return(0x1)
            }
            Return(0x0)
        }
        Method(MHGT, 0x1, NotSerialized)
        {
            If(LAnd(\WNTF, \TATC))
            {
                Store(0x1000000, Local0)
                If(\FTPS)
                {
                    Or(Local0, 0x2000000, Local0)
                }
                Add(Local0, ShiftLeft(\TSFT, 0x10, ), Local0)
                Add(Local0, ShiftLeft(\TSTT, 0x14, ), Local0)
                Store(And(Arg0, 0xff, ), Local1)
                If(LNot(ATMV(Local1)))
                {
                    Or(Local0, 0xffff, Local0)
                    Return(Local0)
                }
                Store(And(Arg0, 0xf, ), Local1)
                If(LEqual(Local1, 0x0))
                {
                    Add(Local0, \TIF0, Local0)
                }
                Else
                {
                    If(LEqual(Local1, 0x1))
                    {
                        Add(Local0, \TIF1, Local0)
                    }
                    Else
                    {
                        If(LEqual(Local1, 0x2))
                        {
                            Add(Local0, \TIF2, Local0)
                        }
                        Else
                        {
                            Add(Local0, 0xff, Local0)
                        }
                    }
                }
                Store(And(ShiftRight(Arg0, 0x4, ), 0xf, ), Local1)
                If(LEqual(Local1, 0x0))
                {
                    Add(Local0, ShiftLeft(\TIT0, 0x8, ), Local0)
                }
                Else
                {
                    If(LEqual(Local1, 0x1))
                    {
                        Add(Local0, ShiftLeft(\TIT1, 0x8, ), Local0)
                    }
                    Else
                    {
                        If(LEqual(Local1, 0x2))
                        {
                            Add(Local0, ShiftLeft(\TIT2, 0x8, ), Local0)
                        }
                        Else
                        {
                            Add(Local0, ShiftLeft(0xff, 0x8, ), Local0)
                        }
                    }
                }
                Return(Local0)
            }
            Return(0x0)
        }
        Method(ATMV, 0x1, NotSerialized)
        {
            Store(And(Arg0, 0xf, ), Local1)
            Store(\TNFT, Local0)
            If(LNot(LLess(Local1, Local0)))
            {
                Return(0x0)
            }
            Store(And(ShiftRight(Arg0, 0x4, ), 0xf, ), Local2)
            Store(\TNTT, Local0)
            If(LNot(LLess(Local2, Local0)))
            {
                Return(0x0)
            }
            If(\TATL)
            {
                If(XOr(Local1, Local2, ))
                {
                    Return(0x0)
                }
            }
            Return(0x1)
        }
        Method(MHST, 0x0, NotSerialized)
        {
            Store(\_SB_.PCI0.LPC_.EC__.HSPD, Local0)
            Return(Local0)
        }
        Method(MHTT, 0x0, NotSerialized)
        {
            Acquire(BFWM, 0xffff)
            \GCTP()
            CreateByteField(\BFWB, 0x3, TMP3)
            CreateByteField(\BFWB, 0xb, TMPB)
            If(\VIGD)
            {
                Store(0x0, Local0)
            }
            Else
            {
                Store(TMP3, Local0)
                ShiftLeft(Local0, 0x8, Local0)
            }
            Or(Local0, TMPB, Local0)
            ShiftLeft(Local0, 0x8, Local0)
            Or(Local0, \_SB_.PCI0.LPC_.EC__.TMP0, Local0)
            Release(BFWM)
            Return(Local0)
        }
        Method(MHBT, 0x0, NotSerialized)
        {
            Name(ABUF, Buffer(0x10)
            {
            })
            ATMS(0x0)
            Store(\ATMB, ABUF)
            Return(ABUF)
        }
        Method(MHFT, 0x1, NotSerialized)
        {
            FSCT(Arg0)
        }
        Method(MHCT, 0x1, NotSerialized)
        {
            Store(0x0, Local0)
            If(\SPEN)
            {
                Store(\LWST, Local0)
                Increment(Local0)
                ShiftLeft(Local0, 0x8, Local0)
            }
            Store(0x8, Local1)
            ShiftLeft(Local1, 0x8, Local1)
            If(LEqual(Arg0, 0xffffffff))
            {
                Or(Local1, \TPCR, Local1)
                If(\SPEN)
                {
                    Or(Local0, \PPCR, Local0)
                    If(LNot(LAnd(\PPMF, 0x2000000)))
                    {
                        Or(Local1, 0x80, Local1)
                    }
                    If(LNot(LAnd(\PPMF, 0x8000000)))
                    {
                        Or(Local1, 0x40, Local1)
                    }
                }
                Else
                {
                    Or(Local1, 0xc0, Local1)
                }
            }
            Else
            {
                If(LAnd(\OSPX, \SPEN))
                {
                    And(Arg0, 0xff0000, Local2)
                    ShiftRight(Local2, 0x10, Local2)
                    Or(Local0, Local2, Local0)
                    If(XOr(Local2, \PPCR, ))
                    {
                        Store(Local2, \PPCA)
                        \PNTF(0x80)
                    }
                }
                If(\WVIS)
                {
                    And(Arg0, 0x1f, Local2)
                    Or(Local1, Local2, Local1)
                    If(XOr(Local2, \TPCR, ))
                    {
                        Store(Local2, \TPCA)
                        \PNTF(0x82)
                    }
                }
            }
            ShiftLeft(Local0, 0x10, Local0)
            Or(Local0, Local1, Local0)
            Return(Local0)
        }
    }
    Scope(\_SB_.PCI0.LPC_.EC__)
    {
        Method(ATMC, 0x0, NotSerialized)
        {
            If(LAnd(\WNTF, \TATC))
            {
                If(HPAC)
                {
                    Store(\TCFA, Local0)
                    Store(\TCTA, Local1)
                    Store(Or(ShiftLeft(Local1, 0x4, ), Local0, ), Local2)
                    XOr(Local2, ATMX, Local3)
                    Store(Local2, ATMX)
                    If(LEqual(\TCTA, 0x0))
                    {
                        Store(\TCR0, \TCRT)
                        Store(\TPS0, \TPSV)
                    }
                    Else
                    {
                        If(LEqual(\TCTA, 0x1))
                        {
                            Store(\TCR1, \TCRT)
                            Store(\TPS1, \TPSV)
                        }
                        Else
                        {
                        }
                    }
                }
                Else
                {
                    Store(\TCFD, Local0)
                    Store(\TCTD, Local1)
                    Store(Or(ShiftLeft(Local1, 0x4, ), Local0, ), Local2)
                    XOr(Local2, ATMX, Local3)
                    Store(Local2, ATMX)
                    If(LEqual(\TCTD, 0x0))
                    {
                        Store(\TCR0, \TCRT)
                        Store(\TPS0, \TPSV)
                    }
                    Else
                    {
                        If(LEqual(\TCTD, 0x1))
                        {
                            Store(\TCR1, \TCRT)
                            Store(\TPS1, \TPSV)
                        }
                        Else
                        {
                        }
                    }
                }
                If(LNot(LEqual(\FID_, 0x2)))
                {
                    If(And(\PPMF, 0x2000000, ))
                    {
                        Store(\FTPS, Local4)
                        If(Local2)
                        {
                            Store(0x0, \FTPS)
                        }
                        Else
                        {
                            Store(0x1, \FTPS)
                        }
                        If(XOr(\FTPS, Local4, ))
                        {
                            If(\OSPX)
                            {
                                \PNTF(0x80)
                            }
                        }
                    }
                }
                If(Local3)
                {
                    If(\_SB_.PCI0.LPC_.EC__.HKEY.DHKC)
                    {
                        \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x6030)
                    }
                }
                Notify(\_TZ_.THM0, 0x81)
            }
        }
    }
    Scope(\_TZ_)
    {
        ThermalZone(THM0)
        {
            Method(_CRT, 0x0, NotSerialized)
            {
                Return(\TCRT)
            }
            Method(_PSV, 0x0, NotSerialized)
            {
                Return(\TPSV)
            }
            Method(_TC1, 0x0, NotSerialized)
            {
                Return(\TTC1)
            }
            Method(_TC2, 0x0, NotSerialized)
            {
                Return(\TTC2)
            }
            Method(_TSP, 0x0, NotSerialized)
            {
                Return(\TTSP)
            }
            Method(_PSL, 0x0, NotSerialized)
            {
                If(And(\PPMF, 0x1000000, ))
                {
                    If(LEqual(\PNHM, 0x106e0))
                    {
                        Return(Package(0x8)
                        {
                            \_PR_.CPU0,
                            \_PR_.CPU1,
                            \_PR_.CPU2,
                            \_PR_.CPU3,
                            \_PR_.CPU4,
                            \_PR_.CPU5,
                            \_PR_.CPU6,
                            \_PR_.CPU7
                        })
                    }
                    Else
                    {
                        Return(Package(0x4)
                        {
                            \_PR_.CPU0,
                            \_PR_.CPU1,
                            \_PR_.CPU2,
                            \_PR_.CPU3
                        })
                    }
                }
                Return(Package(0x1)
                {
                    \_PR_.CPU0
                })
            }
            Method(_TMP, 0x0, NotSerialized)
            {
                If(\H8DR)
                {
                    Store(\_SB_.PCI0.LPC_.EC__.TMP0, Local0)
                    Store(\_SB_.PCI0.LPC_.EC__.TSL2, Local1)
                    Store(\_SB_.PCI0.LPC_.EC__.TSL3, Local2)
                }
                Else
                {
                    Store(\RBEC(0x78), Local0)
                    Store(And(\RBEC(0x8a), 0x7f, ), Local1)
                    Store(And(\RBEC(0x8b), 0x7f, ), Local2)
                }
                If(LEqual(Local0, 0x80))
                {
                    Store(0x30, Local0)
                }
                If(Local2)
                {
                    Return(C2K_(0x80))
                }
                If(LNot(\_SB_.PCI0.LPC_.EC__.HKEY.DHKC))
                {
                    If(Local1)
                    {
                        Return(C2K_(0x80))
                    }
                }
                Return(C2K_(Local0))
            }
        }
        Method(C2K_, 0x1, NotSerialized)
        {
            Add(Multiply(Arg0, 0xa, ), 0xaac, Local0)
            If(LNot(LGreater(Local0, 0xaac)))
            {
                Store(0xbb8, Local0)
            }
            If(LGreater(Local0, 0xfac))
            {
                Store(0xbb8, Local0)
            }
            Return(Local0)
        }
    }
    Scope(\_SB_.PCI0.LPC_.EC__)
    {
        Method(_Q40, 0x0, NotSerialized)
        {
            Notify(\_TZ_.THM0, 0x80)
            If(\H8DR)
            {
                Store(\_SB_.PCI0.LPC_.EC__.TSL2, Local1)
            }
            Else
            {
                Store(And(\RBEC(0x8a), 0x7f, ), Local1)
            }
            If(\_SB_.PCI0.LPC_.EC__.HKEY.DHKC)
            {
                If(Local1)
                {
                    \_SB_.PCI0.LPC_.EC__.HKEY.MHKQ(0x6022)
                }
            }
            If(VIGD)
            {
                Noop
            }
            Else
            {
                \VTHR()
            }
            If(And(\PPMF, 0x1, ))
            {
                If(\OSPX)
                {
                    \PNTF(0x80)
                }
                Else
                {
                    Store(And(\_SB_.PCI0.LPC_.EC__.TSL0, 0x77, ), Local2)
                    If(Local2)
                    {
                        \STEP(0x9)
                    }
                    Else
                    {
                        \STEP(0xa)
                    }
                }
            }
        }
    }
    Name(GPIC, 0x0)
    Method(_PIC, 0x1, NotSerialized)
    {
        Store(Arg0, \GPIC)
    }
    OperationRegion(SMI0, SystemIO, 0xb2, 0x1)
    Field(SMI0, ByteAcc, NoLock, Preserve)
    {
        APMC, 8
    }
    Field(MNVS, AnyAcc, NoLock, Preserve)
    {
        Offset(0xfc0),
        CMD_, 8,
        ERR_, 32,
        PAR0, 32,
        PAR1, 32,
        PAR2, 32,
        PAR3, 32
    }
    Mutex(MSMI, 0x7)
    Method(SMI_, 0x5, NotSerialized)
    {
        Acquire(MSMI, 0xffff)
        Store(Arg0, CMD_)
        Store(Arg1, PAR0)
        Store(Arg2, PAR1)
        Store(Arg3, PAR2)
        Store(Arg4, PAR3)
        Store(0xf5, APMC)
        While(LEqual(ERR_, 0x1))
        {
            Sleep(0x64)
            Store(0xf5, APMC)
        }
        Store(PAR0, Local0)
        Release(MSMI)
        Return(Local0)
    }
    Method(RPCI, 0x1, NotSerialized)
    {
        Return(SMI_(0x0, 0x0, Arg0, 0x0, 0x0))
    }
    Method(WPCI, 0x2, NotSerialized)
    {
        SMI_(0x0, 0x1, Arg0, Arg1, 0x0)
    }
    Method(MPCI, 0x3, NotSerialized)
    {
        SMI_(0x0, 0x2, Arg0, Arg1, Arg2)
    }
    Method(RBEC, 0x1, NotSerialized)
    {
        Return(SMI_(0x0, 0x3, Arg0, 0x0, 0x0))
    }
    Method(WBEC, 0x2, NotSerialized)
    {
        SMI_(0x0, 0x4, Arg0, Arg1, 0x0)
    }
    Method(MBEC, 0x3, NotSerialized)
    {
        SMI_(0x0, 0x5, Arg0, Arg1, Arg2)
    }
    Method(RISA, 0x1, NotSerialized)
    {
        Return(SMI_(0x0, 0x6, Arg0, 0x0, 0x0))
    }
    Method(WISA, 0x2, NotSerialized)
    {
        SMI_(0x0, 0x7, Arg0, Arg1, 0x0)
    }
    Method(MISA, 0x3, NotSerialized)
    {
        SMI_(0x0, 0x8, Arg0, Arg1, Arg2)
    }
    Method(VEXP, 0x0, NotSerialized)
    {
        SMI_(0x1, 0x0, 0x0, 0x0, 0x0)
    }
    Method(VUPS, 0x1, NotSerialized)
    {
        SMI_(0x1, 0x1, Arg0, 0x0, 0x0)
    }
    Method(VSDS, 0x2, NotSerialized)
    {
        SMI_(0x1, 0x2, Arg0, Arg1, 0x0)
    }
    Method(VDDC, 0x0, NotSerialized)
    {
        SMI_(0x1, 0x3, 0x0, 0x0, 0x0)
    }
    Method(VVPD, 0x1, NotSerialized)
    {
        SMI_(0x1, 0x4, Arg0, 0x0, 0x0)
    }
    Method(VNRS, 0x1, NotSerialized)
    {
        SMI_(0x1, 0x5, Arg0, 0x0, 0x0)
    }
    Method(GLPW, 0x0, NotSerialized)
    {
        Return(SMI_(0x1, 0x6, 0x0, 0x0, 0x0))
    }
    Method(VSLD, 0x1, NotSerialized)
    {
        SMI_(0x1, 0x7, Arg0, 0x0, 0x0)
    }
    Method(VEVT, 0x1, NotSerialized)
    {
        Return(SMI_(0x1, 0x8, Arg0, 0x0, 0x0))
    }
    Method(VTHR, 0x0, NotSerialized)
    {
        Return(SMI_(0x1, 0x9, 0x0, 0x0, 0x0))
    }
    Method(VBRC, 0x1, NotSerialized)
    {
        SMI_(0x1, 0xa, Arg0, 0x0, 0x0)
    }
    Method(VBRG, 0x0, NotSerialized)
    {
        Return(SMI_(0x1, 0xe, 0x0, 0x0, 0x0))
    }
    Method(VBTD, 0x0, NotSerialized)
    {
        Return(SMI_(0x1, 0xf, 0x0, 0x0, 0x0))
    }
    Method(VDYN, 0x2, NotSerialized)
    {
        Return(SMI_(0x1, 0x11, Arg0, Arg1, 0x0))
    }
    Method(UCMS, 0x1, NotSerialized)
    {
        Return(SMI_(0x2, Arg0, 0x0, 0x0, 0x0))
    }
    Method(BHDP, 0x2, NotSerialized)
    {
        Return(SMI_(0x3, 0x0, Arg0, Arg1, 0x0))
    }
    Method(STEP, 0x1, NotSerialized)
    {
        SMI_(0x4, Arg0, 0x0, 0x0, 0x0)
    }
    Method(TRAP, 0x0, NotSerialized)
    {
        SMI_(0x5, 0x0, 0x0, 0x0, 0x0)
    }
    Method(CBRI, 0x0, NotSerialized)
    {
        SMI_(0x5, 0x1, 0x0, 0x0, 0x0)
    }
    Method(ASSI, 0x1, NotSerialized)
    {
        SMI_(0x5, 0x6, Arg0, 0x0, 0x0)
    }
    Method(BCHK, 0x0, NotSerialized)
    {
        Return(SMI_(0x5, 0x4, 0x0, 0x0, 0x0))
    }
    Method(BYRS, 0x0, NotSerialized)
    {
        SMI_(0x5, 0x5, 0x0, 0x0, 0x0)
    }
    Method(BLTH, 0x1, NotSerialized)
    {
        Return(SMI_(0x6, Arg0, 0x0, 0x0, 0x0))
    }
    Method(FISP, 0x0, NotSerialized)
    {
        SMI_(0x7, 0x0, 0x0, 0x0, 0x0)
    }
    Method(ATCC, 0x1, NotSerialized)
    {
        SMI_(0x8, Arg0, 0x0, 0x0, 0x0)
    }
    Method(WGSV, 0x1, NotSerialized)
    {
        Return(SMI_(0x9, Arg0, 0x0, 0x0, 0x0))
    }
    Method(ATMS, 0x1, NotSerialized)
    {
        Return(SMI_(0xa, 0x3, 0x0, 0x0, 0x0))
    }
    Method(FSCT, 0x1, NotSerialized)
    {
        Return(SMI_(0xa, 0x4, Arg0, 0x0, 0x0))
    }
    Method(GCTP, 0x0, NotSerialized)
    {
        SMI_(0xa, 0x6, 0x0, 0x0, 0x0)
    }
    Method(PPMS, 0x1, NotSerialized)
    {
        Return(SMI_(0xb, Arg0, 0x0, 0x0, 0x0))
    }
    Method(TRAZ, 0x2, NotSerialized)
    {
        Store(Arg1, SMIF)
        If(LEqual(Arg0, 0x1))
        {
            Store(0x0, \_SB_.PCI0.LPC_.TRPI)
        }
        Return(SMIF)
    }
    Method(TPHY, 0x1, NotSerialized)
    {
        SMI_(0xc, Arg0, 0x0, 0x0, 0x0)
    }
    Method(CSUM, 0x1, NotSerialized)
    {
        Return(SMI_(0xe, Arg0, 0x0, 0x0, 0x0))
    }
    Method(NVSS, 0x1, NotSerialized)
    {
        Return(SMI_(0xf, Arg0, 0x0, 0x0, 0x0))
    }
    Method(WMIS, 0x2, NotSerialized)
    {
        Return(SMI_(0x10, Arg0, Arg1, 0x0, 0x0))
    }
    Method(AWON, 0x1, NotSerialized)
    {
        Return(SMI_(0x12, Arg0, 0x0, 0x0, 0x0))
    }
    Method(PMON, 0x2, NotSerialized)
    {
        Store(SizeOf(Arg0), Local0)
        Name(TSTR, Buffer(Local0)
        {
        })
        Store(Arg0, TSTR)
        Store(TSTR, \DBGS)
        SMI_(0x11, Arg1, 0x0, 0x0, 0x0)
    }
    Method(UAWS, 0x1, NotSerialized)
    {
        Return(SMI_(0x13, Arg0, 0x0, 0x0, 0x0))
    }
    Method(BFWC, 0x1, NotSerialized)
    {
        Return(SMI_(0x14, 0x0, Arg0, 0x0, 0x0))
    }
    Method(BFWP, 0x0, NotSerialized)
    {
        Return(SMI_(0x14, 0x1, 0x0, 0x0, 0x0))
    }
    Method(BFWL, 0x0, NotSerialized)
    {
        SMI_(0x14, 0x2, 0x0, 0x0, 0x0)
    }
    Method(BFWG, 0x1, NotSerialized)
    {
        SMI_(0x14, 0x3, Arg0, 0x0, 0x0)
    }
    Method(BDMC, 0x1, NotSerialized)
    {
        SMI_(0x14, 0x4, Arg0, 0x0, 0x0)
    }
    Method(DPIO, 0x2, NotSerialized)
    {
        If(LNot(Arg0))
        {
            Return(0x0)
        }
        If(LGreater(Arg0, 0xf0))
        {
            Return(0x0)
        }
        If(LGreater(Arg0, 0xb4))
        {
            If(Arg1)
            {
                Return(0x2)
            }
            Else
            {
                Return(0x1)
            }
        }
        If(LGreater(Arg0, 0x78))
        {
            Return(0x3)
        }
        Return(0x4)
    }
    Method(DUDM, 0x2, NotSerialized)
    {
        If(LNot(Arg1))
        {
            Return(0xff)
        }
        If(LGreater(Arg0, 0x5a))
        {
            Return(0x0)
        }
        If(LGreater(Arg0, 0x3c))
        {
            Return(0x1)
        }
        If(LGreater(Arg0, 0x2d))
        {
            Return(0x2)
        }
        If(LGreater(Arg0, 0x1e))
        {
            Return(0x3)
        }
        If(LGreater(Arg0, 0x14))
        {
            Return(0x4)
        }
        Return(0x5)
    }
    Method(DMDM, 0x2, NotSerialized)
    {
        If(Arg1)
        {
            Return(0x0)
        }
        If(LNot(Arg0))
        {
            Return(0x0)
        }
        If(LGreater(Arg0, 0x96))
        {
            Return(0x1)
        }
        If(LGreater(Arg0, 0x78))
        {
            Return(0x2)
        }
        Return(0x3)
    }
    Method(UUDM, 0x2, NotSerialized)
    {
        If(LNot(And(Arg0, 0x4, )))
        {
            Return(0x0)
        }
        If(And(Arg1, 0x20, ))
        {
            Return(0x14)
        }
        If(And(Arg1, 0x10, ))
        {
            Return(0x1e)
        }
        If(And(Arg1, 0x8, ))
        {
            Return(0x2d)
        }
        If(And(Arg1, 0x4, ))
        {
            Return(0x3c)
        }
        If(And(Arg1, 0x2, ))
        {
            Return(0x5a)
        }
        If(And(Arg1, 0x1, ))
        {
            Return(0x78)
        }
        Return(0x0)
    }
    Method(UMDM, 0x4, NotSerialized)
    {
        If(LNot(And(Arg0, 0x2, )))
        {
            Return(0x0)
        }
        If(And(Arg1, 0x4, ))
        {
            Return(Arg3)
        }
        If(And(Arg1, 0x2, ))
        {
            If(LNot(LGreater(Arg3, 0x78)))
            {
                Return(0xb4)
            }
            Else
            {
                Return(Arg3)
            }
        }
        If(And(Arg2, 0x4, ))
        {
            If(LNot(LGreater(Arg3, 0xb4)))
            {
                Return(0xf0)
            }
            Else
            {
                Return(Arg3)
            }
        }
        Return(0x0)
    }
    Method(UPIO, 0x4, NotSerialized)
    {
        If(LNot(And(Arg0, 0x2, )))
        {
            If(LEqual(Arg2, 0x2))
            {
                Return(0xf0)
            }
            Else
            {
                Return(0x384)
            }
        }
        If(And(Arg1, 0x2, ))
        {
            Return(Arg3)
        }
        If(And(Arg1, 0x1, ))
        {
            If(LNot(LGreater(Arg3, 0x78)))
            {
                Return(0xb4)
            }
            Else
            {
                Return(Arg3)
            }
        }
        If(LEqual(Arg2, 0x2))
        {
            Return(0xf0)
        }
        Else
        {
            Return(0x384)
        }
    }
    Method(FDMA, 0x2, NotSerialized)
    {
        If(LNot(LEqual(Arg1, 0xff)))
        {
            Return(Or(Arg1, 0x40, ))
        }
        If(LNot(LLess(Arg0, 0x3)))
        {
            Return(Or(Subtract(Arg0, 0x2, ), 0x20, ))
        }
        If(Arg0)
        {
            Return(0x12)
        }
        Return(0x0)
    }
    Method(FPIO, 0x1, NotSerialized)
    {
        If(LNot(LLess(Arg0, 0x3)))
        {
            Return(Or(Arg0, 0x8, ))
        }
        If(LEqual(Arg0, 0x1))
        {
            Return(0x1)
        }
        Return(0x0)
    }
    Method(SCMP, 0x2, NotSerialized)
    {
        Store(SizeOf(Arg0), Local0)
        If(LNot(LEqual(Local0, SizeOf(Arg1))))
        {
            Return(One)
        }
        Increment(Local0)
        Name(STR1, Buffer(Local0)
        {
        })
        Name(STR2, Buffer(Local0)
        {
        })
        Store(Arg0, STR1)
        Store(Arg1, STR2)
        Store(Zero, Local1)
        While(LLess(Local1, Local0))
        {
            Store(DerefOf(Index(STR1, Local1, )), Local2)
            Store(DerefOf(Index(STR2, Local1, )), Local3)
            If(LNot(LEqual(Local2, Local3)))
            {
                Return(One)
            }
            Increment(Local1)
        }
        Return(Zero)
    }
    Name(SPS_, 0x0)
    Name(OSIF, 0x0)
    Name(W98F, 0x0)
    Name(WNTF, 0x0)
    Name(WXPF, 0x0)
    Name(WVIS, 0x0)
    Name(WSPV, 0x0)
    Name(LNUX, 0x0)
    Name(H8DR, 0x0)
    Name(MEMX, 0x0)
    Name(ACST, 0x0)
    Name(FNID, 0x0)
    Name(RRBF, 0x0)
    Name(NBCF, 0x0)
}
